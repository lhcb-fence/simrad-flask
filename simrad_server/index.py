from flask import Blueprint, g, make_response, session, request, jsonify, redirect, url_for, Response
from .resources.scripts import plot, diff_plot, spectra, dict_spectra, dict_regular, plot_box
from io import TextIOWrapper
from .resources.scripts.plots.usrbin_plot import UsrbinPlot
from .resources.scripts.utils.helpers.fluka_helpers import is_file_scoring, get_scoring_type
from .resources.scripts.parsers.scoring_parsers.usrtrack_parser import UsrtrackParser
from .resources.scripts.parsers.scoring_parsers.usrbin_parser import UsrbinParser
import os
import copy
from werkzeug.utils import secure_filename
from urllib.request import unquote
import numpy as np
import time
bp = Blueprint("index", __name__)

def require_login():
    """Shared authentication logic."""
    if "user" not in session:
        return redirect(url_for("auth.login", _scheme="https"))

@bp.before_request
def authenticate():
    """Apply authentication to the index blueprint."""
    require_login()

@bp.route("/hello", methods=["GET"])
def hello():
    return jsonify({"message": "Hello, World!"})

def get_file_info(file, filetype):
    file_name = secure_filename(file.filename)
    backend_dir = os.getenv("CACHE_DIR", "temporary_backend/")
    file_path = os.path.join(backend_dir, file_name)
    file.save(file_path)
    scoring_type = get_scoring_type(file_path)
    if (scoring_type != filetype):
        error_msg: str = ("The provided file is not a {} file.".format(filetype))

        raise Exception(error_msg)

    if (filetype == 'usrbin'):
        bins = UsrbinParser.get_bins(file_path, data=False)
        dict_bins = {"fileMetadata": [{bin.pop("bin_name"): bin} for bin in bins]}

        return dict_bins
    else:
        tracks = UsrtrackParser.get_tracks(file_path, data=False)
        dict_tracks = {"fileMetadata": [{track.pop("track_name"): track} for track in tracks]}

        return dict_tracks

@bp.route("/usrbin_info", methods=["POST"])
def usrbin_info():
    if ('file' not in request.files):
        error_msg: str = ("No file was provided in the POST request.")

        return jsonify({'error': error_msg}), 400

    file = request.files['file']
    try:
        bin_info = get_file_info(file, 'usrbin')

    except Exception as e:

        return jsonify({'error': str(e)}), 400

    return jsonify(bin_info), 200

@bp.route("/usrtrack_info", methods=["POST"])
def usrtrack_info():
    if ('file' not in request.files):
        error_msg: str = ("No file was provided in the POST request.")

        return jsonify({'error': error_msg}), 400

    file = request.files['file']
    try:
        track_info = get_file_info(file, 'usrtrack')

    except Exception as e:

        return jsonify({'error': str(e)}), 400

    return jsonify(track_info), 200

@bp.route("/plot", methods=["GET"])
def plot():
    DATA_DIR = os.getenv("PLOTS_DIR", "")
    # append all 'dir' arguments to a list
    dir_keys = []
    dir_values= []
    for item in request.args.items():
        if (item[0].startswith('dir')):
            dir_keys.append(item[0])
            dir_values.append(item[1])
    if (not dir_values):
        error_msg: str = ("A list one scoring must be provided.")

        return jsonify({'error': error_msg}), 400
    # check if the directory are valid
    for i in range(len(dir_values)):
        dir_values[i] = os.path.join(DATA_DIR, dir_values[i])
        if (not os.path.isdir(dir_values[i])):
            error_msg: str = ("Provided directory "
                              + "'{}' not found.".format(dir_values[i]))

            return jsonify({'error': error_msg}), 400
    # check if the directories contained valid fluka scoring files
    scoring_files: list[str] = []
    scoring_types: list[str] = []
    for dir_path in dir_values:
        listdir = os.listdir(dir_path)
        file_paths = []
        for elem in listdir:
            path = os.path.join(dir_path, elem)
            if (os.path.isfile(path)):
                file_paths.append(path)
        for file_path in file_paths:
            if (not is_file_scoring):
                error_msg: str = ("Provided directory "
                                  + "'{}' contains .".format(dir_path)
                                  + "files not recognized as fluka files.")

                return jsonify({'error': error_msg}), 406
            else:
                scoring_type = get_scoring_type(file_path)
                if (scoring_type is not None):
                    scoring_files.append(file_path)
                    scoring_types.append(scoring_type)
                else:
                    error_msg: str = ("Provided directory "
                                      + "'{}' contains ".format(dir_path)
                                      + "files recognized as fluka scoring fil"
                                      + "es but the scoring type is unknown.")

                    return jsonify({'error': error_msg}), 406
    # check if there is an available endpoint and redirects
    unique_scorings = list(set(scoring_types))
    query_string: str = ''
    if ((len(unique_scorings) == 1) and (unique_scorings[0]=='usrtrack')):
        type_value = request.args.get('type')
        if (type_value is not None):
            query_string += 'type=' + copy.copy(type_value)
        else:   # assigning user by default
            query_string += 'type=user'
        for i in range(len(dir_values)):
            separator = '&'# if i else ''
            query_string += separator + dir_keys[i] + '=' + dir_values[i]
            vol_value = request.args.get('vol'+dir_keys[i][3:])
            if (vol_value is None):
                pass # should return error
            else:
                query_string += '&vol{}='.format(i+1) + copy.copy(vol_value)
            name_value = request.args.get('name'+dir_keys[i][3:])
            if (name_value is None):
                pass # should return error
            else:
                query_string += '&name{}='.format(i+1) + copy.copy(name_value)
        url = os.getenv("URL_PREFIX", "") + 'usrtrack_plot/?' + query_string
        return redirect(url, code=307)

        #return jsonify({'plot_endpoint': 'usrtrack_plot'})
    elif ((len(unique_scorings) == 1) and (unique_scorings[0]=='usrbin')):
        type_value = request.args.get('type')
        if (type_value is not None):
            query_string += 'type=' + copy.copy(type_value)
        else:   # assigning user by default
            query_string += 'type=user'
        for i in range(len(dir_values)):
            separator = '&'# if i else ''
            query_string += separator + dir_keys[i] + '=' + dir_values[i]
            name_value = request.args.get('name'+dir_keys[i][3:])
            if (name_value is None):
                pass # should return error
            else:
                query_string += '&name{}='.format(i+1) + copy.copy(name_value)

        url = os.getenv("URL_PREFIX", "") + 'usrbin_plot/?' + query_string
        return redirect(url, code=307)
    else:
        str_scorings: str = ''
        for scoring in unique_scorings:
            str_scorings += scoring.capitalize() + ', '
        str_scorings = str_scorings[:-2]
        error_msg: str = ("The provided scorings in the request ("
                          + str_scorings + ") can not currently be plotted "
                          + "simultaneously.")

        return jsonify({'error': error_msg}), 501
