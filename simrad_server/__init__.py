""".. moduleauthor:: Carlos Brito"""

from flask import Flask
from dotenv import load_dotenv
from flask_cors import CORS
import os
from dash_extensions.enrich import ServersideOutputTransform, FileSystemBackend
from dash_extensions.enrich import DashProxy

def create_app(test_config=None):
    load_dotenv(".env.local")

    server = Flask(__name__, instance_relative_config=True)
    server.config.from_mapping(
        SECRET_KEY=os.getenv("SECRET_KEY"),
    )
    if test_config != None:
        server.config.update(test_config)

    server.config['SESSION_COOKIE_SAMESITE'] = 'None'
    server.config['SESSION_COOKIE_SECURE'] = True
    CORS(server)

    from .auth import oauth
    oauth.init_app(server)

    #if not app.testing:
    #   from . import logger
    #   logger.setup_logging(app)

    from . import auth
    from . import index

    server.register_blueprint(auth.bp)
    server.register_blueprint(index.bp)

    # DISCLAIMER: programmer responsability to make sure that all dash ids
    # and callbacks across different dash apps are different, otherwise failure
    # Import Dash application
    from .resources.dashboards.usrtrack_dashboard import init_dashboard as init_dash_1
    from .resources.dashboards.usrbin_dashboard import init_dashboard as init_dash_2

    ############################################################################
    #TODO: change the transforms passing argument
    # Create a backend server side cache
    # only pass it once to enrich dash-extensions, will be shared among all
    # instances of DashProxy
    backend_dir = os.getenv("CACHE_DIR", "temporary_backend/")
    backend = FileSystemBackend(backend_dir)
    transforms = [ServersideOutputTransform(backends=[backend])]

    app_1 = init_dash_1(server, transforms)
    app_2 = init_dash_2(server, [])
    # make url_for('index') == url_for('blog.index')#
    # in another app, you might define a separate main index here with
    # app.route, while giving the blog blueprint a url_prefix, but for
    # the tutorial the blog will be the main index
    #app.add_url_rule("/", endpoint="index")

    return server
