""".. moduleauthor:: Carlos Brito"""

from flask import url_for, session, Blueprint, redirect, request
from flask import redirect
from authlib.integrations.flask_client import OAuth
import os

bp = Blueprint("auth", __name__)

CLIENT_ID = os.getenv("CLIENT_ID")
CLIENT_SECRET = os.getenv("CLIENT_SECRET")
CERN_SSO = "https://auth.cern.ch/auth/realms/cern/.well-known/openid-configuration"

oauth = OAuth()

oauth.register(
    name="cern",
    server_metadata_url=CERN_SSO,
    client_id=CLIENT_ID,
    client_secret=CLIENT_SECRET,
    client_kwargs={"scope": "openid email profile"}
)

@bp.before_request
def require_login():
    if "user" not in session:
        if "_dash-" in request.path:
            return

        if request.endpoint and not request.endpoint.startswith("auth"):
            return redirect(url_for("auth.login", _scheme="https"))

@bp.route("/login")
def login():
    redirect_uri = url_for("auth.auth", _external=True, _scheme="https")
    return oauth.cern.authorize_redirect(redirect_uri=redirect_uri, prompt="none")

@bp.route("/auth")
def auth():
    token = oauth.cern.authorize_access_token()
    session["user"] = token.get("userinfo")
    return redirect("/")

@bp.route("/logout")
def logout():
    session.pop("user", None)
    return redirect("/")
