""".. moduleauthor:: Carlos Brito"""

import logging
from flask import jsonify, request, session
import json
import logging

def setup_logging(app):
    log = logging.getLogger('werkzeug')
    log.disabled = True

    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.INFO)

    formatter = logging.Formatter(
        fmt="[%(asctime)s] simrad-flask.%(levelname)s: %(message)s",
        datefmt="%d/%b/%Y %H:%M:%S"
    )
    console_handler.setFormatter(formatter)
    app.logger.handlers.clear()
    app.logger.addHandler(console_handler)
    app.logger.setLevel(logging.INFO)

    @app.before_request
    def log_request():
        if '_dash-' in request.path:
            return

        app.logger.info("=" * 56)
        app.logger.info("%s %s", request.method, request.path)

        if request.method == "POST":
                if request.content_type == 'application/json':
                    app.logger.info("Request data: %s", json.dumps(request.json, indent=4))
                else:
                    app.logger.info("Request body type: %s", request.content_type)

        if request.method == "GET" and request.args:
            query_params = request.args.to_dict(flat=False)
            app.logger.info("Query parameters: %s", json.dumps(query_params, indent=4))

    @app.errorhandler(Exception)
    def handle_general_exception(e):
        app.logger.error("Unexpected error: %s", str(e))
