""".. moduleauthor:: Sacha E.R. Medaer"""

blue_roundbutton = {
    "border": None,
    "border-radius": "50%",
    "padding": 0,
    "backgroundColor": "blue",
    "color": "white",
    "textAlign": "center",
    "display": "block",
    "fontSize": 12,
    "height": 18,
    "width": 18,
    "margin": 2,
}
