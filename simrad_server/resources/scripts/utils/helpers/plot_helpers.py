""".. moduleauthor:: Sacha E.R. Medaer"""

import copy
import sys
import re

from dash import html
import dash_bootstrap_components as dbc

from simrad_server.resources.scripts.utils.constants.color_scales import \
                                         BUILTIN_COLORSCALES


def create_colorscale_from_colorlist(color_list: list[str]):
    """Create a numeric color scale from a color list assuming a uniform
    distribution of the scale.
    """
    nbr_colors = len(color_list)
    custom_colorscale = []
    for i in range(nbr_colors):
        custom_colorscale.append([(1.0/nbr_colors)*i, color_list[i]])
        custom_colorscale.append([(1.0/nbr_colors)*(i+1), color_list[i]])

    return custom_colorscale


def get_saturated_colorscale(colorscale: str, lower_saturation_color='#ffffff',
                             upper_saturation_color='#ffffff'):
    """Adding saturation values to color scale"""
    # adding a small epsilon on both extremities of the color scale
    eps = sys.float_info.epsilon
    colorscale_ = ([[0.0, lower_saturation_color],
                    [eps, colorscale[0][1]]]
                   + list(colorscale)[1:-1]
                   + [[(1.0-eps), colorscale[-1][1]],
                      [1.0, upper_saturation_color]])

    return colorscale_


def str_tag_html_to_dash(string):
    """For now, just replace <sup>, but should replace all html tags."""
    children = []
    if (('<sup>' in string) and ('</sup>' in string)):
        split_1 = string.split('<sup>')
        split_2 = split_1[1].split('</sup>')
        children.extend([split_1[0], html.Sup(split_2[0]), split_2[1]])
    else:
        children.extend([string])

    return children


def str_tag_html_to_unicode(string):
    """Replace <sup> and <sub> html tag in unicode."""
    unicode_superscripts = ["\u2070", "\u00B9", "\u00B2", "\u00B3", "\u2074",
                            "\u2075", "\u2076", "\u2077", "\u2078", "\u2070"]
    unicode_subscripts = ["\u2080", "\u2081", "\u2082", "\u2083", "\u2084",
                          "\u2085", "\u2086", "\u2087", "\u2088", "\u2089"]
    unicode_super_minus = "\u207B"
    unicode_super_plus = "\u207A"
    unicode_sub_minus = "\u208B"
    unicode_sub_plus = "\u208A"

    sup_res = ''
    sup_split = string.split("<sup>")
    if (len(sup_split) > 1):
        for elem in sup_split:
            sec_split = elem.split('</sup>')
            if (len(sec_split) > 1):    # ['content', ...], at least ['content', '']
                for char in sec_split[0]:
                    if (char == '-'):
                        sup_res += unicode_super_minus
                    elif (char == '+'):
                        sup_res += unicode_super_plus
                    else:
                        try:
                            ind = int(char)
                            sup_res += unicode_superscripts[ind]
                        except:
                            sup_res += '?'
                for i in range(1, len(sec_split)):
                    sup_res += sec_split[i]
            else:  # <sup> without </sup>, this should not be possible
                sup_res += sec_split[0]   # leaving the single <sup>
    else:       # the entire string
        sup_res = copy.copy(string)

    sub_res = ''
    sub_split = sup_res.split("<sub>")
    if (len(sub_split) > 1):
        for elem in sub_split:
            sec_split = elem.split('</sub>')
            if (len(sec_split) > 1):    # ['content', ...], at least ['content', '']
                for char in sec_split[0]:
                    if (char == '-'):
                        sup_res += unicode_sub_minus
                    elif (char == '+'):
                        sup_res += unicode_sub_plus
                    else:
                        try:
                            ind = int(char)
                            sub_res += unicode_subscripts[ind]
                        except:
                            sub_res += '?'
                for i in range(1, len(sec_split)):
                    sub_res += sec_split[i]
            else:  # <sup> without </sup>, this should not be possible
                sub_res += sec_split[0]   # leaving the single <sup>
    else:       # the entire string
        sub_res = copy.copy(sup_res)

    return sub_res


if __name__ == "__main__":

    print(str_tag_html_to_unicode('Gy'))
    print(str_tag_html_to_unicode('cm<sup>-2</sup>'))
    print(str_tag_html_to_unicode('cm<sup>-2</sup>/Gy'))
    print(str_tag_html_to_unicode('cm<sub>-2</sub>/Gy'))
