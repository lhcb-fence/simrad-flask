""".. moduleauthor:: Sacha E.R. Medaer"""

import mmap

from ..constants.fluka_code import FLUKA_WHAT_LENGTH as arg_len
from ..constants.fluka_code import FLUKA_NBR_WHATS as nbr_whats
from ..constants.fluka_scorings_rx import *


#*...+....1....+....2....+....3....+....4....+....5....+....6....+....7....+...
def get_fluka_command_line(keyword: str = '', WHATS: list[str] = [],
                           SDUM: str = ''):
    """This function returns the command line of fluka with the proper spacing.
    This function assumes that the limit of 10 char is respected. See
    'Input Alignement' in FLUKA manual for more details.
    """
    line = ''
    line += keyword
    # Possibility of 6 WHATS, can throw warning if len(WHATS) > 6
    i = 0
    offset = 2 if (len(keyword)) else 1 # align on 1 if now keyword
    while (i < nbr_whats) and (i < len(WHATS)):
        nbr_of_spaces = ((i+offset) * arg_len) - (len(line) + len(WHATS[i]))
        line += " " * nbr_of_spaces
        line += WHATS[i]
        i += 1
    # The SDUM can be left aligned
    nbr_of_spaces = ((nbr_whats+1) * arg_len) - len(line)
    line += " " * nbr_of_spaces
    line += SDUM

    return line



if __name__ == "__main__":

    from ..constants.fluka_code import FLUKA_SCALE_COMMAND_LINE

    print(FLUKA_SCALE_COMMAND_LINE)
    print(get_fluka_command_line('BEAM', ['1.0', '2.0', '-3.0'], 'PROTON'))
    print(get_fluka_command_line('PLOTGEOM',
                                 ['1.0', '1.0', '1.0', '1.0', '1.0', '1.0'],
                                 'FORMAT'))
    print(get_fluka_command_line('', ['1.0', '1.0', '0.0', '0.0']))


def get_scoring_type(file_path: str):
    """Return the type of scoring the provided file is, otherwise None."""
    with open(file_path, 'rb', 0) as file, \
            mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ) as s:
        if (USRYIELD_RX.search(s)):

            return 'usryield'
        elif (USRTRACK_RX.search(s) or USRTRACK_LIS_RX.search(s)):

            return 'usrtrack'
        elif (USRCOLL_RX.search(s)):

            return 'usrcoll'
        elif (USRBDX_RX.search(s)):

            return 'usrbdx'
        elif (USRBIN_RX.search(s)):

            return 'usrbin'
        #elif (s.find(b'example') != -1):
        #
        #    return 'another_scoring'
        else:

            return None


def is_file_scoring(file_path: str):
    """Return true if the provided file is detected as a FLUKA scoring."""

    return (get_scoring_type(file_path) is not None)


if __name__ == "__main__":
    usrbin_file = './test_data/fluka_scorings/example_score001_fort_40.bin'
    usrbin_bbn_file = '/mnt/c/Users/samedaer/cernbox/Documents/FLUKA/test_simu/output_dir/lhcb_cavern_2024001_fort41.txt'
    usrbdx_file = './test_data/fluka_scorings/example_score001_fort_50.bdx'
    usrtrk_file = './test_data/fluka_scorings/example_score001_fort_55.trk'
    usrtrk_lis_file = '/mnt/c/Users/samedaer/Downloads/run/example_track_lin_enegy_bin_with_low_en_neutrons.lis'
    usrcol_file = './test_data/fluka_scorings/example_score001_fort_56.col'
    usryld_file = './test_data/fluka_scorings/example_score001_fort_59.yld'
    random_file = './test_data/fluka_scorings/file.random'
    scoring_files = {#'USRBIN': usrbin_file,
                     'USRBDX': usrbdx_file,
                     #'USRTRACK': usrtrk_file, 'USRCOLL': usrcol_file,
                     'USRYIELD': usryld_file, #'RANDOM': random_file,
                     'USRTRACK': usrtrk_lis_file,
                     'USRBIN': usrbin_bbn_file}
    for key in scoring_files.keys():
        is_scoring = is_file_scoring(scoring_files[key])
        print('---------------------- is scoring: {}'.format(is_scoring))
        calc_type = get_scoring_type(scoring_files[key])
        print('The file {} should be of type {}.'.format(key, calc_type))
