""".. moduleauthor:: Sacha E.R. Medaer"""

import plotly.graph_objects as go


# See available color scales of plotly at:
# https://plotly.com/python/builtin-colorscales/

BUILTIN_COLORSCALES = ['aggrnyl',     'agsunset',    'blackbody',   'bluered',
                       'blues',       'blugrn',      'bluyl',       'brwnyl',
                       'bugn',        'bupu',        'burg',        'burgyl',
                       'cividis',     'darkmint',    'electric',    'emrld',
                       'gnbu',        'greens',      'greys',       'hot',
                       'inferno',     'jet',         'magenta',     'magma',
                       'mint',        'orrd',        'oranges',     'oryel',
                       'peach',       'pinkyl',      'plasma',      'plotly3',
                       'pubu',        'pubugn',      'purd',        'purp',
                       'purples',     'purpor',      'rainbow',     'rdbu',
                       'rdpu',        'redor',       'reds',        'sunset',
                       'sunsetdark',  'teal',        'tealgrn',     'turbo',
                       'viridis',     'ylgn',        'ylgnbu',      'ylorbr',
                       'ylorrd',      'algae',       'amp',         'deep',
                       'dense',       'gray',        'haline',      'ice',
                       'matter',      'solar',       'speed',       'tempo',
                       'thermal',     'turbid',      'armyrose',    'brbg',
                       'earth',       'fall',        'geyser',      'prgn',
                       'piyg',        'picnic',      'portland',    'puor',
                       'rdgy',        'rdylbu',      'rdylgn',      'spectral',
                       'tealrose',    'temps',       'tropic',      'balance',
                       'curl',        'delta',       'oxy',         'edge',
                       'hsv',         'icefire',     'phase',       'twilight',
                       'mrybm',       'mygbm']

BUILTIN_COLORSCALES = sorted(BUILTIN_COLORSCALES)


class PlotlyColorscalesError(Exception):
    pass


class PlotlyColorscales(object):
    """This class allows to access the numerical version of the built-in
    color scales in Plotly.
    """
    def __getitem__(self, attr: str) -> tuple[tuple[float, str]]:
        attr_: str = attr.lower()
        if (attr_ in BUILTIN_COLORSCALES):
            img = dict()

            return go.Figure(data=go.Heatmap(img, colorscale=attr_)
                             ).data[0].colorscale

        else:
            error_message: str = ("The requested color scale '{}' ".format(attr)
                                  + "does not exist, please choose among the "
                                  + "following scales "
                                  + "{}.".format(BUILTIN_COLORSCALES))
            raise PlotlyColorscalesError(error_message)


PLOTLY_COLORSCALES = PlotlyColorscales()


if __name__ == "__main__":
    """
    Small code snippet to extract all numeric colorscale numeric.
    """
    for scale in BUILTIN_COLORSCALES:
        print(PLOTLY_COLORSCALES[scale])
