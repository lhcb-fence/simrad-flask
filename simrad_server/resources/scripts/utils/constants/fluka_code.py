""".. moduleauthor:: Sacha E.R. Medaer"""


FLUKA_COMMENT_CHAR: str = '*'

FLUKA_WHAT_LENGTH: int = 10

FLUKA_NBR_WHATS: int = 6

FLUKA_SCALE_COMMAND_LINE: str = ("*...+....1....+....2....+....3....+....4...."
                                 + "+....5....+....6....+....7....+...")
