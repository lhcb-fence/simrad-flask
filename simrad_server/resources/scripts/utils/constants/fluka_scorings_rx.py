""".. moduleauthor:: Sacha E.R. Medaer"""

import re

SCORING_RX = re.compile(br'\*\*\*\*\*  FLUKA Course Exercise \s*\*\*\*\*\*[\s\S]*DATE: (\d{2}/\d{2}/\d{2}),  TIME: (\d{2}:\d{2}:\d{2})[\s\S]*Total number of particles followed\s*(\d+), for a total weight of\s*([\d.E+]+)')
USRTRACK_RX = re.compile(br'Track n\.\s+(\d+)\s+"(\w+)\s+"\s*,\s*generalized particle n\.\s+(\d+),\s*region n\.\s+(\d+)')
USRTRACK_LIS_RX = re.compile(br'\**.trk\s*\*')
USRCOLL_RX = re.compile(br'Coll  n\.\s+(\d+)\s+"(\w+)\s+"\s*,\s*generalized particle n\.\s+(\d+),\s*region n\.\s+(\d+)')
USRYIELD_RX = re.compile(br'Yield n\.\s+(\d+)\s+"(\w+)\s+"\s*,\s*generalized particle n\.\s+(\d+),\s*from region n\.\s+(\d+)\s*to region n\.\s+(\d+)')
USRBDX_RX = re.compile(br'Bdrx n\.\s+(\d+)\s+"(\w+)\s+"\s*,\s*generalized particle n\.\s+(\d+),\s*from region n\.\s+(\d+)\s*to region n\.\s+(\d+)')
USRBIN_RX = re.compile(br'binning n\.\s+(\d+)\s+"(\w+)\s+"\s*,\s*generalized particle n\.\s+(\d+)')
