""".. moduleauthor:: Sacha E.R. Medaer"""


DELIMITER_CHAR_OPTIONS: list[dict[str, str]]
DELIMITER_CHAR_OPTIONS = [{'label': 'Tab (\t)', 'value': '\t'},
                          {'label': 'Comma (,)', 'value': ','},
                          {'label': 'Space ( )', 'value': ' '},
                          {'label': 'Tilde (~)', 'value': '~'},
                          {'label': 'Colon (:)', 'value': ':'},
                          {'label': 'Semi-colon (;)', 'value': ';'},
                          {'label': 'Pipe (|)', 'value': '|'},]

END_LINE_CHAR_OPTIONS: list[dict[str, str]]
END_LINE_CHAR_OPTIONS = [{'label': 'Unix/MacOS (LF)', 'value': '\n'},
                         {'label': 'Windows/DOS (CR + LF)', 'value': '\r\n'},
                         {'label': 'Classic MacOS (CR)', 'value': '\r'}]
