


# source : https://lbgroups.cern.ch/online/OperationsPlots/index.htm
# click on year and then "Integrated Lumi over LHC Fill Number"


DELIVERED_LUMI_ORIGIN = {2010: [(42.15, 3.5)],     #[1/pb, Tev], p-p
                          2011: [(1.2195, 3.5)],    #[1/fb, Tev], p-p
                          2012: [(2208.75, 4.0)],   #[1/pb, Tev], p-p
                          2013: [(2137.72, 4.0)],   #[1/ub, TeV], p-Pb
                          2015: [(370.80, None),    #[1/pb, Tev], p-p
                                 (6.10, None)],     #[1/ub, Tev], p-Pb
                          2016: [(1906.01, None),   #[1/pb, Tev], p-p
                                 (34.09, None)],    #[1/nb, TeV], p-Pb
                          2017: [(1875.91, 6.5),    #[1/pb, TeV], p-p
                                 (111.23, 2.51)],   #[1/pb, TeV], p-p
                          2018: [(2461.70, 6.5),    #[1/pb, TeV], p-p
                                 (236.92, None)],   #[1/ub, TeV], p-Pb
                          2022: [(1058.63, 6.8)],   #[1/pb, TeV], p-p
                          2023: [(536.14, 6.8),     #[1/pb, TeV], p-p
                                 (249.29, None)],   #[1/ub, TeV], p-Pb
                          2024: [(10.90, 6.8),      #[1/fb, TeV], p-p
                                 (246.06, 2.68),    #[1/pb, TeV], p-p ref run
                                 (505.31, 6.8)],    #[1/ub, TeV], p-Pb
                         }

# All in [1/fb, TeV (cofm), collision_type]
# energy cofm = 2*energy
# taking the cross section at the closest available energy
DELIVERED_LUMI = {2010: [(42.15e-3, 7.0, 'pp')],     #[1/fb, Tev], p-p
                  2011: [(1.2195e-0, 7.0, 'pp')],    #[1/fb, Tev], p-p
                  2012: [(2208.75e-3, 8.0, 'pp')],   #[1/fb, Tev], p-p
                  2013: [(2137.72e-9, 8.0, 'pPb')],   #[1/fb, p-Pb
                  2015: [(370.80e-3, 14.0, 'pp'),    #[1/fb, Tev], p-p
                         (6.10e-9, 14.0, 'pp')],     #[1/fb, Tev], p-Pb
                  2016: [(1906.01e-3, 14.0, 'pp'),   #[1/fb, Tev], p-p
                         (34.09e-6, None, 'pPb')],    #[1/fb, TeV], p-Pb
                  2017: [(1875.91e-3, 14.0, 'pp'),    #[1/fb, TeV], p-p
                         (111.23e-3, 7.0, 'pp')],   #[1/fb, TeV], p-p
                  2018: [(2461.70e-3, 14.0, 'pp'),    #[1/fb, TeV], p-p
                         (236.92e-9, None, 'pPb')],   #[1/fb, TeV], p-Pb
                  2022: [(1058.63e-3, 14.0, 'pp')],   #[1/fb, TeV], p-p
                  2023: [(536.14e-3, 14.0, 'pp'),     #[1/fb, TeV], p-p
                         (249.29e-9, None, 'pPb')],   #[1/fb, TeV], p-Pb
                  2024: [(10.90e-0, 14.0, 'pp'),      #[1/fb, TeV], p-p
                         (246.06e-3, 7.0, 'pp'),    #[1/fb, TeV], p-p ref run
                         (505.31e-9, 14.0, 'pPb')],    #[1/fb, TeV], p-Pb
                  # Below is expected, see TDR Upgrade II or
                  # https://lhcb.web.cern.ch/lhcb_page/lhcb_documents/progress/Source/RRB/October_2022/LHCb_upgrade2_RRB_october2022.pdf
                  2025: [(10e-0, 14.0, 'pp')],  #[1/fb, TeV], p-p
                  2026: [(10e-0, 14.0, 'pp')],  #[1/fb, TeV], p-p
                  2029: [(10e-0, 14.0, 'pp')],  #[1/fb, TeV], p-p
                  2030: [(10e-0, 14.0, 'pp')],  #[1/fb, TeV], p-p
                  2031: [(10e-0, 14.0, 'pp')],  #[1/fb, TeV], p-p
                  2032: [(10e-0, 14.0, 'pp')],  #[1/fb, TeV], p-p
                  2033: [(10e-0, 14.0, 'pp')],  #[1/fb, TeV], p-p
                  2035: [(50e-0, 14.0, 'pp')],  #[1/fb, TeV], p-p
                  2036: [(50e-0, 14.0, 'pp')],  #[1/fb, TeV], p-p
                  2037: [(50e-0, 14.0, 'pp')],  #[1/fb, TeV], p-p
                  2038: [(50e-0, 14.0, 'pp')],  #[1/fb, TeV], p-p
                  2039: [(50e-0, 14.0, 'pp')],  #[1/fb, TeV], p-p
                 }

# given by Matthias, should find a proper ref for this
# All in TeV: mb
PP_CROSS_SECTIONS = {7.0: 72.0,
                     8.0: 74.0,
                     14.0: 84.0}

RUN_DEF = {'RUN 1 (~7/8 TeV): 2010-2013': [2010, 2011, 2012, 2013],
           'RUN 2 (~14 TeV): 2015-2018': [2015, 2016, 2017, 2018],
           'RUN 3 (~14 TeV): 2022-2024': [2022, 2023, 2024],
           'Nominal RUN 3 (~14 TeV): 2022-2026': [2022, 2023, 2024, 2025, 2026],
           'Nominal RUN 4 (~14 TeV): 2029-2033': [2029, 2030, 2031, 2032, 2033],
           'Nominal RUN 5 (~14 TeV): 2035-2039': [2035, 2036, 2037, 2038, 2039],
           }
