import plotly.express as px


PLOT_2D_COLORS: list[str]
PLOT_2D_COLORS = ['#7F00FF', '#5500ff', '#330099','#001f3f', '#004080',
                  '#0070bf', '#0080ff', '#0099FF', '#00CCFF', '#009900',
                  '#00CC33', '#66CC33', '#93F600', '#ffff00', '#ff8000',
                  '#FF0000', '#CC0000', '#990033'
                 ]

PLOTLY_COLOR_PALETTES_STR = ['Plotly', 'D3', 'G10', 'T10', 'Alphabet',
                             'Dark24', 'Light24', 'Set1', 'Pastel1', 'Dark2',
                             'Set2', 'Pastel2', 'Set3', 'Antique', 'Bold',
                             'Pastel', 'Prism', 'Safe', 'Vivid']
PLOTLY_COLOR_PALETTES_DICT = {}
for palette_str in PLOTLY_COLOR_PALETTES_STR:
    colors = getattr(px.colors.qualitative, palette_str)
    palette_name = palette_str + ' (' + str(len(colors)) + ' colors)'
    PLOTLY_COLOR_PALETTES_DICT[palette_name] = colors
