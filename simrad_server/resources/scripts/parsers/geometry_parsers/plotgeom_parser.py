""".. moduleauthor:: Sacha E.R. Medaer"""

import os
import re

import numpy as np


EXP_NUM_RX =r"[-+]?[0-9]*\.?[0-9]+(?:[eE][-+]?[0-9]+)?"


class PlotgeomParserFileError(FileNotFoundError):
    pass


class PlotgeomParser(object):
    """This class parses the content of a STORE extension file
    produced with the PLOTGEOM card from FLUKA.
    """

    def __init__(self, file_path: str) -> None:

        self._file_path: str = ''
        self.file_path = file_path

        return None

    @property
    def file_path(self) -> str:

        return self._file_path

    @file_path.setter
    def file_path(self, new_file_path: str) -> None:
        if (os.path.isfile(new_file_path)):
            self._file_path = new_file_path
        else:
            error_message: str = ("The provided .STORE file from PLOTGEOM "
                                  "was not found.")

            raise PlotgeomParserFileError(error_message)

    def get_segments_as_line(self, x_offset: float = 0., y_offset: float = 0.
                             ) -> tuple[list[float], list[float]]:
        """This function returns all the segments in two lists. The first list
        corresponds to the x-values, and the second list corresponds to the
        y-values. The segment are separated by Nan values.
        Optional x- or y-offsets can be applied to all values.
        """
        counter: int = 0
        length: int = 0
        x: list[float] = []
        y: list[float] = []
        with open(self.file_path, 'r') as f:
            for raw_line in f.readlines():
                line = raw_line.replace(" ", "")
                if (counter < length):
                    temp_list = re.findall(EXP_NUM_RX, raw_line)
                    x.append(float(temp_list[0])+x_offset)
                    y.append(float(temp_list[1])+y_offset)
                    counter += 1

                if (line.startswith("Wormno")): # Worm found
                    # append Nan to separate segments
                    x.append(float('nan'))
                    y.append(float('nan'))
                    length = int(line.split('length')[1])
                    counter = 0

        return x, y


if __name__ == "__main__":

    import argparse

    import plotly.graph_objects as go

    parser = argparse.ArgumentParser(description=" ")
    parser.add_argument("--file", help="Path to STORE file from PLOTGEOM card.")
    args = parser.parse_args()

    x, y = PlotgeomParser(args.file).get_segments_as_line()
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=x, y=y, mode='lines', line_color='black',
                             showlegend=False),)
    fig.show()
