""".. moduleauthor:: Sacha E.R. Medaer"""

import argparse
import re
import sys
import datetime

import numpy as np

from ...utils.constants.fluka_low_neutrons_en_bins import LOW_NEUTRON_BINS
from ...scoring_codes import ScoringCodes
get_info_from_id = ScoringCodes.get_info_from_id
get_name_from_id = ScoringCodes.get_name_from_id

DATETIME_FORMAT = "%m/%d/%y%H:%M:%S"

CONVERT_UNIT = {"GeV/g": "Gy", "pSv": "mSv"}

DATE_TIME_RE = re.compile(r'DATE: (\d{2}/\d{2}/\d{2}),  TIME: (\d{2}:\d{2}:\d{2})')
PARTICLE_INFO_RE = re.compile(r'Total number of particles followed\s*(\d+), for a total weight of\s*([\d.E+]+)')
NBR_PRIMARIES_RE = re.compile(r'Total primaries run:\s*(\d+)\s*')
TOTAL_WEIGHT_RE = re.compile(r'Total weight of the primaries run:\s*(\d+)')

SUM_TRACK_INFO_RE = re.compile(r'Detector n:\s*(\d+)\s*(\w+)\s*')
SUM_VOLUME_RE = re.compile(r'\(Volume:\s*(\d+)\s*')
SUM_PARTICLE_RE = re.compile(r'distr. scored:\s*(\d+)\s*,')
SUM_REGION_RE = re.compile(r'from reg.\s*(\d+)\s*\)')
SUM_FLUX_RE = re.compile(r'\*\*\*\* Different\. Fluxes as a function of energy \*\*\*\* \*\*\*\*      \(integrated over solid angle\)        \*\*\*\*\s*')
SUM_CUM_FLUX_RE = re.compile(r'\*\*\*\* Cumulative Fluxes as a function of energy \*\*\*\* \*\*\*\*      \(integrated over solid angle\)        \*\*\*\*\s*')
SUM_ENERGY_BINS_RE = re.compile(r'Energy boundaries \(GeV\):')
SUM_LOW_BIN_RE = re.compile(r'Lowest boundary   \(GeV\):\s*([\d.E+-]+)\s*')
SUM_FLUX_DATA_RE = re.compile(r'Flux \(Part\/GeV\/cmq\/pr\):')
SUM_CUM_FLUX_DATA_RE = re.compile(r'Cumul\. Flux \(Part\/cmq\/pr\):')
TRACK_INFO_RE = re.compile(r'Track n\.\s+(\d+)\s+"(\w+)\s+"\s*,\s*generalized particle n\.\s+(\d+),\s*region n\.\s+(\d+)')
VOLUME_RE = re.compile(r'detector volume:\s*([\d.E+]+)\s*cm')
LOG_ENERGY_BINS_RE = re.compile(r'logar\.?\s*energy\s+binning\s+from\s+([\d.E+-]+)\s+to\s+([\d.E+-]+)\s+GeV,\s+(\d+)\s+bins\s+\(ratio\s+:\s+([\d.E+-]+)\)')
LIN_ENERGY_BINS_RE = re.compile(r'linear\s+energy\s+binning\s+from\s+([\d.E+-]+)\s+to\s+([\d.E+-]+)\s+GeV,\s+(\d+)\s+bins\s*\(\s*([\d.E+-]+)\s*GeV\s+wide\)')
LOW_EN_RE = re.compile(r'Low\s+energy\s+neutron\s+data\s+from\s+group\s+(\d+)\s+to\s+group\s+(\d+)\s+follow\s+in\s+a\s+vector\s+A\(ig\),\s+format\s+\((\d+\(\d+x,\d+p,\d+\(\d+x,e\d{2}\.\d\)\))\)')
EXP_NUM_RE = re.compile(r'[-+]?\d*\.\d+[eE][-+]?\d+')
ANY_NUM_RE = re.compile(r'[-+]?\d*\.\d+(?:[eE][-+]?\d+)?')
# in DATA_NUM_RE: (?:\.?) needed because float can end with a \., such as 25.
DATA_NUM_RE = re.compile(r'([-+]?\d*\.(?:\d+)?(?:[eE][-+]?\d+)?)\s*\+\/\-\s*([-+]?\d*\.(?:\d+)(?:[eE][-+]?\d+)?)\s*\%\s*')

USRTRACK_FILE_ERROR_MSG = ("The provided file is not recognized as a USRTRACK "
                           "file from FLUKA.")


class UsrtrackParserError(Exception):
    pass


class UsrtrackParser(object):
    """This class parses the content of a USRTRACK scoring from FLUKA.
    """

    def __init__(self) -> None:

        return None

    @staticmethod
    def get_tracks(file_path: str, err_file_path: str= '', data: bool = True,
                   rel_err: bool = True) -> list[dict]:
        r"""This function extracts the tracks from the USRTRACK file.

        Parameters
        ----------
        file_path :
            The full path of the USRTRACK scoring file.

        Returns
        -------
        :
            Return a list of tracks. Each track is a dictionnary.

        """
        tracks = []
        file_type, crt_data, crt_flux = '', '', ''
        nbr_particles, total_weight, datetime = None, None, None
        with open(file_path, 'r') as f:
            for line in f.readlines():
                # Search for file header
                if (line.strip()):
                    particle_info_re_search = PARTICLE_INFO_RE.search(line)
                    nbr_primaries_re_search = NBR_PRIMARIES_RE.search(line)
                    total_weight_re_search = TOTAL_WEIGHT_RE.search(line)
                    date_time_re_search = DATE_TIME_RE.search(line)
                    if (particle_info_re_search is not None):
                        file_type = '.trk'
                        re_groups = particle_info_re_search.groups()
                        nbr_particles = int(re_groups[0].strip())
                        total_weight = float(re_groups[1].strip())
                    elif (nbr_primaries_re_search is not None):
                        file_type = 'sum.lis'
                        re_groups = nbr_primaries_re_search.groups()
                        nbr_particles = int(re_groups[0].strip())
                    elif (total_weight_re_search is not None):
                        re_groups = total_weight_re_search.groups()
                        total_weight = float(re_groups[0].strip())
                    elif (date_time_re_search is not None):
                        re_groups = date_time_re_search.groups()
                        date, time = re_groups[0], re_groups[1]
                        #datetime_ = datetime.strptime(date+time,
                        #                              DATETIME_FORMAT)
                    else:
                        # Search for tracks and track metada
                        if (file_type == '.trk'):
                            track_re_search = TRACK_INFO_RE.search(line)
                            volume_re_search = VOLUME_RE.search(line)
                            lin_bins_re_search = LIN_ENERGY_BINS_RE.search(line)
                            log_bins_re_search = LOG_ENERGY_BINS_RE.search(line)
                            low_en_re_search = LOW_EN_RE.search(line)
                            if (track_re_search is not None):
                                # New track detected
                                tracks.append({})
                                re_groups = track_re_search.groups()
                                particle_id = int(re_groups[2])
                                name = get_info_from_id(particle_id)
                                gen_name = get_name_from_id(particle_id)
                                track_nbr = int(re_groups[0])
                                tracks[-1]['nbr_particles'] = nbr_particles
                                tracks[-1]['total_weight'] = total_weight
                                tracks[-1]['datetime'] = datetime
                                tracks[-1]['track_nbr'] = track_nbr
                                tracks[-1]['track_name'] = re_groups[1]
                                tracks[-1]['particle_nbr'] = particle_id
                                tracks[-1]['particle_name'] = name
                                tracks[-1]['particle_gen_name'] = gen_name
                                tracks[-1]['region_nbr'] = int(re_groups[3])
                                if (data):
                                    tracks[-1]['data'] = []
                                    tracks[-1]['err_data'] = []
                                    tracks[-1]['bins'] = []
                                    tracks[-1]['mid_bins'] = []
                            elif (volume_re_search is not None):
                                re_groups = volume_re_search.groups()
                                tracks[-1]['volume'] = float(re_groups[0])
                            elif (lin_bins_re_search is not None):
                                re_groups = lin_bins_re_search.groups()
                                tracks[-1]['bin_start'] = float(re_groups[0])
                                tracks[-1]['bin_end'] = float(re_groups[1])
                                tracks[-1]['nbr_bins'] = int(re_groups[2])
                                tracks[-1]['bin_width'] = float(re_groups[3])
                                if (data):
                                    bins = np.linspace(tracks[-1]['bin_start'],
                                                       tracks[-1]['bin_end'],
                                                       tracks[-1]['nbr_bins']+1,
                                                       True)
                                    tracks[-1]['bins'] = bins
                                    mid_bins = (bins[:-1]+bins[1:])/2
                                    tracks[-1]['mid_bins'] = mid_bins
                            elif (log_bins_re_search is not None):
                                re_groups = log_bins_re_search.groups()
                                tracks[-1]['bin_start'] = float(re_groups[0])
                                tracks[-1]['bin_end'] = float(re_groups[1])
                                tracks[-1]['nbr_bins'] = int(re_groups[2])
                                tracks[-1]['bin_ratio'] = float(re_groups[3])
                                if (data):
                                    bins = np.logspace(
                                        np.log10(tracks[-1]['bin_start']),
                                        np.log10(tracks[-1]['bin_end']),
                                        tracks[-1]['nbr_bins']+1, True)
                                    tracks[-1]['bins'] = bins
                                    mid_bins = (bins[:-1]+bins[1:])/2
                                    tracks[-1]['mid_bins'] = mid_bins
                            elif (low_en_re_search is not None):
                                re_groups = low_en_re_search.groups()
                                en_first_group = int(re_groups[0])
                                if (en_first_group != 1):
                                    # Should always be 1 for continuity
                                    raise UsrtrackParserError()
                                en_last_group = int(re_groups[1])
                                low_bins = LOW_NEUTRON_BINS[:en_last_group+1]
                                low_bins = np.array(low_bins)
                                if (np.array(tracks[-1]['bins']).any()):
                                    print('from ', tracks[-1]['bin_start'], tracks[-1]['bin_end'])
                                    print('--------------', tracks[-1]['bins'][0], low_bins[0])
                                    if (tracks[-1]['bins'][0] != low_bins[0]):
                                        # first group should be first bin
                                        raise UsrtrackParserError()
                                    else:
                                        bins_ = np.hstack((tracks[-1]['bins'],
                                                           low_bins[1:]))
                                        tracks[-1]['bins'] = bins_
                                        ########################################
                                        # Should have proper way of having mid-bin here
                                        ########################################
                                        mid_bins = (low_bins[:-1]+low_bins[1:])/2
                                        mid_bins_ = np.hstack((tracks[-1]['mid_bins'],
                                                               mid_bins))
                                        tracks[-1]['mid_bins'] = mid_bins_
                                else:
                                    tracks[-1]['bins'] = low_bins
                                    ########################################
                                    # Should have proper way of having mid-bin here
                                    ########################################
                                    mid_bins = (low_bins[:-1]+low_bins[1:])/2
                                    tracks[-1]['mid_bins'] = mid_bins
                            else:
                                if (data):
                                    # Search for Data
                                    str_data = EXP_NUM_RE.findall(line)
                                    sub_nbr = EXP_NUM_RE.sub("", line).strip()
                                    if (not len(sub_nbr)):
                                        float_data =  [float(elem) for elem in
                                                       str_data]
                                        tracks[-1]['data'].extend(float_data)
                        elif (file_type == 'sum.lis'):
                            track_re_search = SUM_TRACK_INFO_RE.search(line)
                            volume_re_search = SUM_VOLUME_RE.search(line)
                            particle_re_search = SUM_PARTICLE_RE.search(line)
                            region_re_search = SUM_REGION_RE.search(line)
                            flux_re_search = SUM_FLUX_RE.search(line)
                            cum_flux_re_search = SUM_CUM_FLUX_RE.search(line)
                            bins_re_search = SUM_ENERGY_BINS_RE.search(line)
                            low_bin_re_search = SUM_LOW_BIN_RE.search(line)
                            flux_data_re_search = SUM_FLUX_DATA_RE.search(line)
                            cum_flux_data_re_search = SUM_CUM_FLUX_DATA_RE.search(line)
                            if (track_re_search is not None):
                                # New track detected
                                tracks.append({})
                                re_groups = track_re_search.groups()
                                tracks[-1]['nbr_particles'] = nbr_particles
                                tracks[-1]['total_weight'] = total_weight
                                tracks[-1]['datetime'] = datetime
                                tracks[-1]['track_nbr'] = int(re_groups[0])
                                tracks[-1]['track_name'] = re_groups[1]
                                if (data):
                                    tracks[-1]['data'] = []
                                    tracks[-1]['err_data'] = []
                                    tracks[-1]['bins'] = []
                                    tracks[-1]['mid_bins'] = []
                            elif (volume_re_search is not None):
                                re_groups = volume_re_search.groups()
                                tracks[-1]['volume'] = float(re_groups[0])
                            elif (particle_re_search is not None):
                                re_groups = particle_re_search.groups()
                                particle_id = int(re_groups[0])
                                name = get_info_from_id(particle_id)
                                gen_name = get_name_from_id(particle_id)
                                tracks[-1]['particle_name'] = name
                                tracks[-1]['particle_gen_name'] = gen_name
                            elif (region_re_search is not None):
                                re_groups = region_re_search.groups()
                                tracks[-1]['region_nbr'] = int(re_groups[0])
                            elif (flux_re_search is not None):
                                crt_flux = 'flux'
                            elif (cum_flux_re_search is not None):
                                crt_flux = 'cum_flux'
                            elif (bins_re_search is not None):
                                crt_data = 'bins'
                            elif (flux_data_re_search is not None):
                                crt_data = 'flux'
                            elif (cum_flux_data_re_search is not None):
                                crt_data = 'cum_flux'
                            elif ((low_bin_re_search is not None) and data
                                    and (crt_flux == 'flux')
                                    and (crt_data == 'bins')):
                                re_groups = low_bin_re_search.groups()
                                # Can not have data without bins
                                if (not tracks[-1]['bins']
                                        and tracks[-1]['data']):

                                        raise UsrtrackParserError()
                                # Can have empty data structure
                                # But _sum.lis has bin values written to file
                                if (tracks[-1]['data']):
                                    bins = tracks[-1]['bins']
                                    tracks[-1]['bins'].append(float(re_groups[0]))
                                    bins_ = np.flip(np.array(tracks[-1]['bins']))
                                    tracks[-1]['bin_start'] = bins_[0]
                                    tracks[-1]['bin_end'] = bins_[-1]
                                    tracks[-1]['nbr_bins'] = len(bins_)-1
                                if (data):
                                    ########################################
                                    # Should have proper way of having mid-bin here
                                    ########################################
                                    bins = np.array(tracks[-1]['bins'])
                                    mid_bins = (bins[:-1]+bins[1:])/2
                                    tracks[-1]['mid_bins'] = mid_bins
                            else:
                                if (data and (crt_flux == 'flux')
                                        and (crt_data == 'flux')):
                                    # Search for Data
                                    str_data = DATA_NUM_RE.findall(line)
                                    sub_nbr = DATA_NUM_RE.sub("", line).strip()
                                    if (not len(sub_nbr)):
                                        values = []
                                        errors = []
                                        for elem in str_data:
                                            values.append(float(elem[0]))
                                            errors.append(float(elem[1])/100.0)
                                        tracks[-1]['data'].extend(values)
                                        tracks[-1]['err_data'].extend(errors)
                                if (data and (crt_flux == 'flux')
                                        and (crt_data == 'bins')):
                                    str_data = ANY_NUM_RE.findall(line)
                                    sub_nbr = ANY_NUM_RE.sub("", line).strip()
                                    if (not len(sub_nbr)):
                                        float_data =  [float(elem) for elem in
                                                       str_data]
                                        # This happens if 2 set of data in a row
                                        # (normal and then low energy neutron)
                                        bins = tracks[-1]['bins']
                                        if ((not bins)  # stops at first eval.
                                                or (bins[-1] != float_data[0])):
                                            bins.extend(float_data)
                                        else:
                                            bins.extend(float_data[1:])
                                        tracks[-1]['bins'] = bins
                        else:
                            if (crt_data or crt_flux):

                                raise UsrtrackParserError()

        if (data):  # sort data arrays depending on bins
            for track in tracks:
                mid_bins = np.array(track['mid_bins'])
                ind_sort = mid_bins.argsort()
                track['data'] = np.array(track['data'])[ind_sort]
                if (np.array(track['err_data']).any()):
                    track['err_data'] = np.array(track['err_data'])[ind_sort]
                track['mid_bins'] = mid_bins[ind_sort]
                bins = np.array(track['bins'])
                track['bins'] = bins[bins.argsort()]

        if (err_file_path and data):
            # assumed relative error -> no unit conversion
            err_tracks = UsrtrackParser.get_tracks(err_file_path,
                                                   err_file_path='',
                                                   data=data,
                                                   #unit_conversion=False,
                                                   rel_err=rel_err)
            track_names = [track['track_name'] for track in tracks]
            err_track_names = [err_track['track_name']
                               for err_track in err_tracks]
            for i, err_name in enumerate(err_track_names):
                if (err_name in track_names):
                    ind = track_names.index(err_name)
                    if (rel_err):
                        tracks[ind]['err_data'] = err_tracks[i]['data']
                    else:
                        tracks[ind]['err_data'] = (err_tracks[i]['data']
                                                   /tracks[ind]['data'])

        return tracks


if __name__ == "__main__":
    print('-------------------------------')
    parser = argparse.ArgumentParser(description=" ")
    parser.add_argument("--file", help="relative path to original file")
    parser.add_argument("--errfile", help='err file', default='')
    args = parser.parse_args()
    parser = UsrtrackParser()
    tracks = parser.get_tracks(args.file, data=False)#, args.errfile)
    dict_tracks = {}
    for track in tracks:
        track_name = track.pop("track_name")
        dict_tracks[track_name] = track
    print(dict_tracks)
    #print(tracks)
    #data = parser.get_data(args.file, metadata)
