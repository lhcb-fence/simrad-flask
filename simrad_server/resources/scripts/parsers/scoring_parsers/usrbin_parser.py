""".. moduleauthor:: Sacha E.R. Medaer"""

import argparse
import copy
import math
import re
import sys
import threading
from datetime import datetime

import numpy as np

from ...scoring_codes import ScoringCodes

DATETIME_FORMAT = "%m/%d/%y%H:%M:%S"

CONVERT_UNIT = {"GeV/g": "Gy", "pSv": "mSv"}


# Example of header for bin values:
#
#1
#   Cartesian binning n.   1  "flallcon  " , generalized particle n.  201
#      X coordinate: from -7.0000E+02 to  7.0000E+02 cm,    70 bins ( 2.0000E+01 cm wide)
#      Y coordinate: from -6.0000E+02 to  6.0000E+02 cm,    60 bins ( 2.0000E+01 cm wide)
#      Z coordinate: from -9.0000E+02 to  2.2000E+03 cm,   155 bins ( 2.0000E+01 cm wide)
#      Data follow in a matrix A(ix,iy,iz), format (1(5x,1p,10(1x,e11.4)))
#
#      accurate deposition along the tracks requested
#      this is a track-length binning
#       0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00
#       0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00
#       ...
#
# Example of header for bin errors:
#
#       Percentage errors follow in a matrix A(ix,iy,iz), format (1(5x,1p,10(1x,e11.4)))
#

DATE_TIME_RX = re.compile(r'DATE: (\d{2}/\d{2}/\d{2}),  TIME: (\d{2}:\d{2}:\d{2})')
PARTICLE_INFO_RX = re.compile(r'Total number of particles followed\s*(\d+), for a total weight of\s*([\d.E+]+)')
DIM_COORD_RX = re.compile(r"(\w+) coordinate\: from ([ 0-9eE.+\-]+) to ([ 0-9eE.+\-]+) cm, ([ 0-9]+)")
AXIS_COORD_RX = re.compile(r"axis coordinates\: X = ([ 0-9eE.+\-]+), Y =  ([ 0-9eE.+\-]+) cm, ([ 0-9]+)")
MESH_RX = re.compile(r"(\w+)\s*binning n\.\s+(\d+)\s+\"\s*(\w+)\s+\"\s*,\s*generalized particle n\.\s+(\d+)")
ERROR_RX = re.compile(r"\s*Percentage errors follow in a matrix\s*")
EXP_NUM_STR = r"[-+]?\d*\.\d+[eE][-+]?\d+"
EXP_NUM_RX = re.compile(EXP_NUM_STR)
DATA_LINE_RX = re.compile(r"(\s+?{})+".format(EXP_NUM_STR))

USRBIN_FILE_ERROR_MSG = ("The provided file is not recognized as a USRBIN file"
                         "from FLUKA.")


class UsrbinParserError(Exception):
    pass


class UsrbinParser(object):
    """This class parses the content of a USRBIN scoring from FLUKA.
    """

    def __init__(self) -> None:

        return None


    @staticmethod
    def extract_data(data_lines, data_key, bin, rel_err, unit_conversion):
        # extract data from data_lines containing the raw data lines
        list_data = []
        for i, line in enumerate(data_lines):
            str_data = line.split(' ')
            for elem in str_data:
                elem_ = elem.strip('\n').strip(' ')
                if (elem_):
                    list_data.append(float(elem_))
        bin[data_key] = np.array(list_data)
        # process extracted data
        if (bin['mesh_type'].lower() == 'cartesian'):
            nbr_bins: tuple[str, str, str]
            nbr_bins = (bin['z']['nbr_bins'], bin['y']['nbr_bins'],
                        bin['x']['nbr_bins'])
            # Verify length of the raw data list
            np_data = bin[data_key]
            total_size = nbr_bins[0] * nbr_bins[1] * nbr_bins[2]
            if (total_size != np_data.size):

                raise UsrbinParserError(USRBIN_FILE_ERROR_MSG)
            # Create matrix of size (nbr_bins_x, nbr_bins_y, nbr_bins_z)
            np_data = np_data.reshape(*nbr_bins)
            np_data = np.transpose(np_data, (2, 1, 0))
            bin[data_key] = np_data
        else:

            raise NotImplementedError("Only cartesian is implemented for"
                                      "now.")
        if ((data_key == 'err_data') and rel_err):
            bin[data_key] /= 100.0
        else:   # data or abs. data
            if (unit_conversion):
                orig_unit, unit = bin['orig_unit'], bin['unit']
                bin[data_key] *= ScoringCodes.get_conv_factor(orig_unit, unit)
        # no need to keep data_lines in memory
        del data_lines

    @staticmethod
    def get_bins(file_path: str, err_file_path: str= '',
                 data: bool = True, rel_err: bool = True,
                 unit_conversion: bool = True) -> list[dict]:
        r"""This function extracts the bins from the USRBIN file. The regex
        is made by probability of occurence. The leading line by frequency is
        a data line, followed by a bin value or error header, followed by
        an optional file header.

        Parameters
        ----------
        file_path :
            The full path of the USRBIN scoring file.

        Returns
        -------
        :
            Return a list of bins. Each bin is a dictionnary.

        """
        def start_extract_data_thread(threads, data_lines, data_key, bins):
            target = UsrbinParser.extract_data
            args = (copy.copy(data_lines), data_key, bins[-1],
                    rel_err, unit_conversion)
            thread = threading.Thread(target=target, args=args)
            threads.append(thread)
            thread.start()

        bins = []
        threads = []
        data_lines = []
        ERROR_FOUND = False
        skip_to_line_nbr = None
        nbr_particles, total_weight, datetime = None, None, None
        with open(file_path, 'r') as f:
            for i, line in enumerate(f.readlines()):
                if ((skip_to_line_nbr is None) or (i > skip_to_line_nbr)):
                    data_line_re_search = DATA_LINE_RX.match(line)
                    if (data_line_re_search is not None):
                        # there can be add. info which takes a few line
                        x, y, z = (bins[-1].get('x'), bins[-1].get('y'),
                                   bins[-1].get('z'))
                        if ((x is not None) and (y is not None)
                                and (z is not None)):
                            nbr_x = x['nbr_bins']
                            nbr_y = y['nbr_bins']
                            nbr_z = z['nbr_bins']
                            # Matrix in FLUKA divided by lines of 10 values
                            skip_to_line_nbr = (nbr_x*nbr_y*nbr_z) / 10.0
                            skip_to_line_nbr = math.ceil(skip_to_line_nbr) - 1
                            skip_to_line_nbr += i
                            if (data_lines):
                                print('THIS SHOULD NOT HAPPEN  ', len(data_lines))
                            data_lines = [line]

                    else:  # only go a few times here
                        # all search below have same probability of occurence
                        mesh_rx_re_search = MESH_RX.search(line)
                        mesh_rx_re_search = MESH_RX.search(line)
                        dim_coord_rx_re_search = DIM_COORD_RX.search(line)
                        axis_coord_rx_re_search = AXIS_COORD_RX.search(line)
                        if (mesh_rx_re_search is not None):
                            if (bins):  # already a bin before
                                bins[-1]['last_line'] = i-1
                            # New bin detected, reinit all vars
                            ERROR_FOUND = False
                            bins.append({})
                            re_groups = mesh_rx_re_search.groups()
                            unit = ScoringCodes.get_unit_from_id(int(re_groups[3]))
                            name = ScoringCodes.get_info_from_id(int(re_groups[3]))
                            bins[-1]['nbr_particles'] = nbr_particles
                            bins[-1]['total_weight'] = total_weight
                            bins[-1]['datetime'] = datetime
                            bins[-1]['bin_nbr'] = int(re_groups[1])
                            bins[-1]['bin_name'] = re_groups[2].strip()
                            bins[-1]['particle_nbr'] = int(re_groups[3])
                            bins[-1]['particle_name'] = name
                            bins[-1]['orig_unit'] = unit
                            bins[-1]['mesh_type'] = str(re_groups[0])
                            bins[-1]['first_line'] = copy.copy(i)
                            bins[-1]['last_line'] = -1
                            if (data):
                                new_unit = CONVERT_UNIT.get(unit)
                                if (new_unit is not None):
                                    bins[-1]['unit'] = new_unit
                                else:
                                    bins[-1]['unit'] = unit
                                bins[-1]['data'] = np.array([])
                                bins[-1]['err_data'] = np.array([])
                        elif (dim_coord_rx_re_search is not None):
                            re_groups = dim_coord_rx_re_search.groups()
                            crt_axis: str = re_groups[0].lower()
                            bins[-1][crt_axis] = {}
                            bins[-1][crt_axis]['bin_start'] = float(re_groups[1])
                            bins[-1][crt_axis]['bin_end'] = float(re_groups[2])
                            bins[-1][crt_axis]['nbr_bins'] = int(re_groups[3])
                        elif (axis_coord_rx_re_search is not None):
                            re_groups = axis_coord_rx_re_search.groups()
                            bins[-1]['axis_coord'] = (float(re_groups[0]),
                                                       float(re_groups[1]))
                        else:
                            # if error, same freq. probability than above
                            error_rx_search = ERROR_RX.search(line)
                            if (error_rx_search is not None):
                                ERROR_FOUND = True
                                #bins[-1]['data_and_error_in_file'] = True
                            else:   # only happens once or for non identified lines
                                particle_info_re_search = PARTICLE_INFO_RX.search(line)
                                date_time_re_search = DATE_TIME_RX.search(line)
                                if (particle_info_re_search is not None):
                                    re_groups = particle_info_re_search.groups()
                                    nbr_particles = int(re_groups[0].strip())
                                    total_weight = float(re_groups[1].strip())
                                if (date_time_re_search is not None):
                                    re_groups = date_time_re_search.groups()
                                    date, time = re_groups[0], re_groups[1]
                                    # datetime_ = datetime.strptime(date+time,
                                    #                               DATETIME_FORMAT)
                else:
                    if (data):  # first append last line before calling thread
                        data_lines.append(line)
                    if ((skip_to_line_nbr is not None)
                            and (i == skip_to_line_nbr)):
                        skip_to_line_nbr = None
                        # Start data thread if any data in the stack
                        if (data and data_lines):
                            data_key = 'err_data' if ERROR_FOUND else 'data'
                            start_extract_data_thread(threads, data_lines,
                                                      data_key, bins)
                            data_lines = []

        # Join thread
        for thread in threads:
            thread.join()

        if (err_file_path and data):
            # assumed relative error -> no unit conversion
            err_bins = UsrbinParser.get_bins(err_file_path, err_file_path='',
                                             data=data, unit_conversion=False,
                                             rel_err=rel_err)
            bin_names = [bin['bin_name'] for bin in bins]
            err_bin_names = [err_bin['bin_name'] for err_bin in err_bins]
            for i, err_name in enumerate(err_bin_names):
                if (err_name in bin_names):
                    ind = bin_names.index(err_name)
                    if (rel_err):
                        bins[ind]['err_data'] = err_bins[i]['data']
                    else:
                        bins[ind]['err_data'] = (err_bins[i]['data']
                                                 / bins[ind]['data'])

        return bins



if __name__ == "__main__":

    import time
    parser = argparse.ArgumentParser(description=" ")
    parser.add_argument("--file", help="relative path to original file")
    parser.add_argument("--errfile", help="err file", default='')
    args = parser.parse_args()

    parser = UsrbinParser()
    start = time.time()
    bins = parser.get_bins(args.file, data=True)
    duration = time.time() - start
    dict_bins = {}
    for bin in bins:
        bin_name = bin.pop("bin_name")
        dict_bins[bin_name] = bin
    #print(dict_bins)
    #print(data)
    print('-------------------------------------------: ', duration)
