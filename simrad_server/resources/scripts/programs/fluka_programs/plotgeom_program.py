""".. moduleauthor:: Sacha E.R. Medaer"""

import copy
import os
import shutil
import subprocess
import time

from ...utils.constants.fluka_code import FLUKA_COMMENT_CHAR,\
                                           FLUKA_SCALE_COMMAND_LINE
from ...utils.helpers.fluka_helpers import get_fluka_command_line

# run: $which rfluka on local machine
PATH_TO_FLUKA: str = '/usr/local/fluka/'
PLOTGEOM_FILE_SUFFIX: str = '_PLOTGEOM.STORE'


class PlotgeomProgramFileError(FileNotFoundError):
    pass

class PlotgeomProgram(object):
    """This class inserts a PLOTGEOM card in a provided FLUKA file
    and return the created STORE file containing the segements
    (aka. worms). Following FLUKA manual, the PLOTGEOM card is
    inserted right after the GEOEND card.
    """

    def __init__(self, file_path: str) -> None:

        self._file_path: str = ''
        self.file_path = file_path

        return None

    @property
    def file_path(self) -> str:

        return self._file_path

    @file_path.setter
    def file_path(self, new_file_path: str) -> None:
        # Could add additional checks for fluka files here
        if (os.path.isfile(new_file_path)):
            self._file_path = new_file_path
        else:
            error_message: str = ("The provided input file from FLUKA "
                                  "was not found.")

            raise PlotgeomProgramFileError(error_message)

    @staticmethod
    def get_plotgeom_card(WHAT1: float = 0.0, WHAT2: float = 0.0,
                          WHAT3: float = 0.0, WHAT4: float = 0.0,
                          WHAT5: float = 0.0, WHAT6: float = 0.0,
                          SDUM: str = 'FORMAT',
                          title: str = 'My custom PLOTGEOM card',
                          X0: float = 0.0, Y0: float = 0.0, Z0: float = 0.0,
                          X1: float = 1.0, Y1: float = 1.0, Z1: float = 0.0,
                          TYX: float = 1.0, TYY: float = 0.0,
                          TYZ: float = 0.0, TXX: float = 0.0,
                          TXY: float = 1.0, TXZ: float = 0.0,
                          EXPANY: float = 1.0, EXPANX: float = 1.0,
                          PFIMXX: float = 0.0, PFIMXY: float = 0.0) -> str:
        """Create a copy of the original input file with a PLOTGEOM card
        right after the GEOEND card. The path to the new file is returned.
        This function assumed that there is a valid version of FLUKA installed.
        See FLUKA manual for more details about the PLOTGEOM card.
        """
        code: str = ''
        code += FLUKA_COMMENT_CHAR + '\n'
        code += FLUKA_SCALE_COMMAND_LINE + '\n'
        WHATS = [str(WHAT1), str(WHAT2), str(WHAT3), str(WHAT4), str(WHAT5),
                 str(WHAT6)]
        code += get_fluka_command_line('PLOTGEOM', WHATS, SDUM) + '\n'
        code += title + '\n'
        WHATS = [str(X0), str(Y0), str(Z0), str(X1), str(Y1), str(Z1)]
        code += get_fluka_command_line('', WHATS, '') + '\n'
        WHATS = [str(TYX), str(TYY), str(TYZ), str(TXX), str(TXY), str(TXZ)]
        code += get_fluka_command_line('', WHATS, '') + '\n'
        WHATS = [str(EXPANY), str(EXPANX), str(PFIMXX), str(PFIMXY)]
        code += get_fluka_command_line('', WHATS, '') + '\n'
        code += FLUKA_COMMENT_CHAR + '\n'
        code += 'STOP' + '\n'

        return code

    @staticmethod
    def insert_plotgeom_card(file_path: str, plotgeom_card: str) -> None:
        """This function inserts the PLOTGEOM card right after the GEOEND card
        in the provided file.
        """
        CARD_TO_FIND: str = 'GEOEND'     # must be at beginning of line
        card_pos_start: int = -1
        card_pos_end: int = -1
        with open(file_path, 'r') as f:
            str_file = f.read()
            card_pos_start = str_file.find(CARD_TO_FIND)
            if (card_pos_start != -1):
                card_pos_end = str_file[card_pos_start:].find('\n')
                card_pos_end += card_pos_start  # assume that there is a '\n'
        if (card_pos_start != -1):
            with open(file_path, 'w') as f:
                new_str_file = (str_file[:card_pos_end+1] + plotgeom_card
                                + str_file[card_pos_end+1:])
                f.write(new_str_file)

        return card_pos_start

    def run(self, WHAT1: float = 0.0, WHAT2: float = 0.0, WHAT3: float = 0.0,
            WHAT4: float = 0.0, WHAT5: float = 0.0, WHAT6: float = 0.0,
            X0: float = 0.0, Y0: float = 0.0, Z0: float = 0.0,
            X1: float = 1.0, Y1: float = 1.0, Z1: float = 0.0,
            TYX: float = 1.0, TYY: float = 0.0, TYZ: float = 0.0,
            TXX: float = 0.0, TXY: float = 1.0, TXZ: float = 0.0,
            EXPANY: float = 0.0, EXPANX: float = 0.0,
            PFIMXX: float = 0.0, PFIMXY: float = 0.0,
            output_file_path: str = '', fluka_dir: str = PATH_TO_FLUKA,
            working_dir: str = '') -> str:
        """This function implements the program which performs the following:

        a) Copy the provided file in a new directory
        b) Create the PLOTGEOM card
        c) Insert the card in the copied file
        d) Run FLUKA on the copied file and get the ouput
        f) Get the path to the output file (in crt dir if no file path provided)
        g) Delete the created directory

        The output_file_path is returned.
        """
        # ! should insert log here
        CRT_DIR_BACKUP = os.getcwd()
        WRK_DIR: str = working_dir if working_dir else CRT_DIR_BACKUP
        # Create new directory to run fluka
        crt_time: str = str(time.time()).replace(".", "")
        NEW_DIR: str = os.path.join(WRK_DIR + '/temp_fluka_{}/'.format(crt_time))
        os.makedirs(NEW_DIR)
        os.chdir(NEW_DIR)
        # a) copying the provided file in the new directory
        file_dir, file_name  = os.path.split(self.file_path)
        new_file_name: str = 'COPIED_' + file_name
        new_file_path: str = os.path.join(NEW_DIR, new_file_name)
        # Use copy2 to keep metadata and permissions
        shutil.copy2(self.file_path, new_file_path)
        # b) create the plotgeom card with provided arguments
        str_card = self.get_plotgeom_card(WHAT1=WHAT1, WHAT2=WHAT2, WHAT3=WHAT3,
                                          WHAT4=WHAT4, WHAT5=WHAT5, WHAT6=WHAT6,
                                          X0=X0, Y0=Y0, Z0=Z0, X1=X1, Y1=Y1,
                                          Z1=Z1, TYX=TYX, TYY=TYY, TYZ=TYZ,
                                          TXX=TXX, TXY=TXY, TXZ=TXZ,
                                          EXPANY=EXPANY, EXPANX=EXPANX,
                                          PFIMXX=PFIMXX, PFIMXY=PFIMXY)
        print(str_card)
        # c) Insert the card in the copied file
        inserted = self.insert_plotgeom_card(new_file_path, str_card)
        if (inserted == -1):
            print('SHOULD GET AN ERROR HERE')
        # d) Run fluka with the new file
        # Should have here a higher level function to run all fluka command
        fluka_exec: str = os.path.join(fluka_dir, 'bin/rfluka')
        sp_result = subprocess.run([fluka_exec, "-M", "1", new_file_path])
        sp_code = sp_result.returncode
        if ((sp_code != 0) or (sp_code != 12)): # 0 success, 12 not simu ran
            print('SHOULD RETURN ERROR: no fluka successfully run')
        # N.B.: return code of non successful PLOTGEOM is 136
        # f) Return the path to the .STORE file
        new_file_base_name, new_file_ext  = os.path.splitext(new_file_path)
        crt_output_file_path: str = (new_file_base_name + '001'
                                     + PLOTGEOM_FILE_SUFFIX)
        _, crt_output_file_name = os.path.split(crt_output_file_path)
        is_file_moved: bool = False
        if (output_file_path): # rename to the provide output file path
            try:
                final_output_file_path = output_file_path
                shutil.move(crt_output_file_path, final_output_file_path)
                is_file_moved = True
            except OSError as e:
                print('ERROR')
        if (not is_file_moved):   # move in current directory
            try:
                final_output_file_path = os.path.join(WRK_DIR,
                                                      crt_output_file_name)
                shutil.move(crt_output_file_path, final_output_file_path)
                is_file_moved = True
            except OSError as e:
                print('ERROR')
        # g) Come back to previous working dir and delete the created directory
        os.chdir(CRT_DIR_BACKUP)
        try:
            shutil.rmtree(NEW_DIR)
        except OSError as e:
            print('ERROR')
        # Return

        return final_output_file_path


if __name__ == "__main__":

    import argparse

    import plotly.graph_objects as go
    import numpy as np

    from ...parsers.geometry_parsers.plotgeom_parser import PlotgeomParser

    parser = argparse.ArgumentParser(description=" ")
    parser.add_argument("--file", help="Path to STORE file from PLOTGEOM card.")
    args = parser.parse_args()

    inst = PlotgeomProgram(args.file)
    plotgeom_file = inst.run('', # leave blank for auto file handling
                             1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                             40.0, -600.0, -900.0, 40.0, 600.0, 2200.0,
                             0.0, 1.0, 0.0, 0.0, 0.0, 1.0,
                             0.0, 0.0, 0.0, 0.0)

    x, y = PlotgeomParser(plotgeom_file).get_segments_as_line(x_offset=-900,
                                                              y_offset=-600)
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=x, y=y, mode='lines', line_color='black',
                             showlegend=False),)
    fig.show()
