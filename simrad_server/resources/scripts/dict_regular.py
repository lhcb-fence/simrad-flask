#!/bin/env python

import re
import os
import math
import numpy as np
import glob
import argparse
from .scoring_codes import ScoringCodes
import json
import sys

DATA_DIR = os.getenv("PLOTS_DIR", "")

scoringCodes = ScoringCodes()

exp_num_rx = r"[-+]?\d*\.\d+[eE][-+]?\d+"
rx = re.compile(exp_num_rx)

bname_rx = re.compile(r"Cartesian binning n. ([ 0-9]+)(\s+)\"(\w+)([\ \"\,.a-z]+)(\d+)$")
coord_rx = re.compile(r"(\w+) coordinate\: from ([ 0-9eE.+\-]+) to ([ 0-9eE.+\-]+) cm, ([ 0-9]+)")

def hasFileKeyword(filename, keyword):
    f = open(filename, "r")
    for line in f:
        if keyword in line:
           f.close()
           return True
    f.close()
    return False


def print_dict_keys(d, indent=0):
    for key, value in d.items():
        print("  " * indent + str(key))
        if isinstance(value, dict):
            print_dict_keys(value, indent + 1)


def extractParameters(file):
    parameters = dict()
    for line in file:
        found = coord_rx.search(line)
        if found:
            found = found.groups()
            parameters[found[0]] = { 'from' : float(found[1]), 'to' : float(found[2]), 'count' : int(found[3]) }
        title = bname_rx.search(line)
        if title:
            parameters['type'] = { 'number' : int(title.group(1)), 'word' : title.group(3), 'part_nr' : int(title.group(5)) }
        if set(parameters.keys())==set(['X','Y','Z','type']):
            break
    return parameters


def extractNumbers(file):
    all_numbers = []

    for line in file:
        numbers = rx.findall(line)
        if len(rx.sub("", line).strip()) != 0 :
            continue #
        numbers = map(float, numbers)
        all_numbers += numbers

    return all_numbers


def loadFort(file):
    parameters = extractParameters(file)
    file.seek(0)
    numbers    = extractNumbers(file)
    file.close()
    return dict(header=parameters, data=numbers)


def roundMin(x):
    if x<=0: return x
    else:    return pow(10,math.floor(math.log10(x)))

def roundMax(x):
    if x<=0: return x
    else:    return pow(10,math.ceil(math.log10(x)))


def deterUnits(fort,data):
    particle_number = fort['header']['type']['part_nr']
    key = fort['header']['type']['word']
    unit = scoringCodes.get_unit_from_id(particle_number)
    title = scoringCodes.get_info_from_id(particle_number)
    conv_factor = 1.0

    if unit.startswith('GeV/g'):
           conv_factor = scoringCodes.get_conv_factor("GeV/g", "Gy")
           unit  = unit.replace("GeV/g", "Gy")
           title = title.replace("GeV/g", "Gy")
    if unit.startswith('pSv'):
           conv_factor = scoringCodes.get_conv_factor("pSv", "mSv")
           unit  = unit.replace("pSv", "mSv")
           title = title.replace("pSv", "mSv")

    data = data*conv_factor
    return data

def makeDataArray(fort):
    h_name = fort['header']['type']['word']
    #print_dict_keys(fort)

    (a_name, a, a_lo, a_hi) = ('X', fort['header']['X']['count'], fort['header']['X']['from'], fort['header']['X']['to'])
    (b_name, b, b_lo, b_hi) = ('Y', fort['header']['Y']['count'], fort['header']['Y']['from'], fort['header']['Y']['to'])
    (c_name, c, c_lo, c_hi) = ('Z', fort['header']['Z']['count'], fort['header']['Z']['from'], fort['header']['Z']['to'])
    print('Processing...:')
    print("%s, a*b*c(%d), data len (%d)" % (h_name, (a*b*c), len(fort['data'])) )
    if a*b*c != len(fort['data']):
        print("Some problem with data", a*b*c, " , ", len(fort['data']) )
        return None
    data = fort['data']
    data = np.array(data).reshape(c, b, a)
    data = np.transpose(data, (2, 1, 0))
    data = deterUnits(fort,data)
    return(data)


def retBins(fort, proj):
    if proj=='x':
        proj_key = 'X'
    if proj=='y':
        proj_key = 'Y'
    if proj=='z':
        proj_key = 'Z'
    (lo, hi, count) = (fort['header'][proj_key]['from'],fort['header'][proj_key]['to'],fort['header'][proj_key]['count'])
    bins = np.linspace(lo, hi, count + 1)
    return(bins.tolist())


def extendTitle(fort, title, key):
    binx = (fort['header']['X']['to']-fort['header']['X']['from'])/fort['header']['X']['count']
    biny = (fort['header']['Y']['to']-fort['header']['Y']['from'])/fort['header']['Y']['count']
    binz = (fort['header']['Z']['to']-fort['header']['Z']['from'])/fort['header']['Z']['count']
    binning = "(%dx%dx%d cm each bin) " % (binx,biny,binz)
    title = "%s %s" % (title, binning)
    return title


def toJsonFile(fort):
    particle_number = fort['header']['type']['part_nr']
    key   = fort['header']['type']['word']
    unit  = scoringCodes.get_unit_from_id(particle_number)
    title = scoringCodes.get_info_from_id(particle_number)

    (a_name, a, a_lo, a_hi) = ('X', fort['header']['X']['count'], fort['header']['X']['from'], fort['header']['X']['to'])
    (b_name, b, b_lo, b_hi) = ('Y', fort['header']['Y']['count'], fort['header']['Y']['from'], fort['header']['Y']['to'])
    (c_name, c, c_lo, c_hi) = ('Z', fort['header']['Z']['count'], fort['header']['Z']['from'], fort['header']['Z']['to'])
    title = extendTitle(fort, title, key)
    bins_x = retBins(fort,'x')
    bins_y = retBins(fort,'y')
    bins_z = retBins(fort,'z')
    data_dict = {
        "metadata": {
            "keyword": key,
            "unit": unit,
            "part_nr": particle_number,
            "title": title,
            'a_name': a_name,
            'a': a,
            'a_lo': a_lo,
            'a_hi': a_hi,
            'b_name': b_name,
            'b': b,
            'b_lo': b_lo,
            'b_hi': b_hi,
            'c_name': c_name,
            'c': c,
            'c_lo': c_lo,
            'c_hi': c_hi,
            'bins_x' : bins_x,
            'bins_y' : bins_y,
            'bins_z' : bins_z
        }
    }

    return(data_dict)

def main(file):

    """
    The program analyzes .txt file with USRBINs data from FLUKA and returns dictionary
    containing all necessary metadata for generating plots. You should only provide the
    path to the appropriate file. The dictionary is returned to the standard input.
    Not extracting data.

    Example fragment:

    {
    "keyword": "BPscVD",
    "unit": "GeV/g",
    "part_nr": 228,
    "title": "Dose (energy deposited per unit mass) (2x2x5 cm each bin) ",
    "a_name": "X",
    "a": 80,
    "a_lo": -100.0,
    "a_hi": 100.0,
    "b_name": "Y",
    "b": 80,
    "b_lo": -100.0,
    "b_hi": 100.0,
    "c_name": "Z",
    "c": 146,
    "c_lo": -230.0,
    "c_hi": 500.0,
    "bins_x": [
        -100.0,
        -97.5,
        -95.0,
        -92.5,
        -90.0,
        -87.5,
        -85.0,
        -82.5,
        -80.0,
        -77.5,
        ...
    ...
    }
    """

    fort = loadFort(file)
    plot_dict = toJsonFile(fort)
    # the following line is not required for the endpoint:
    # json_output = json.dumps(plot_dict, indent=4)
    return plot_dict




if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="USRBINs metadata extraction")
    parser.add_argument("--file", help = "directory to file from main folder with data. Obtained by user choosing file")

    args = parser.parse_args()
    main(args.file)
