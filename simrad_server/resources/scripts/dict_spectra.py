#!/bin/env python

import re
import os
import json
import sys
import argparse
import numpy as np
from .scoring_codes import ScoringCodes

DATA_DIR = os.getenv("PLOTS_DIR", "")

scoringCodes = ScoringCodes()

exp_num_rx = r"[-+]?\d*\.\d+[eE][-+]?\d+" # pattern, example 3.2465e+10
rx = re.compile(exp_num_rx)
numbers_line_rx = r"/^ (\s* #{$rx} \s*)+ $/x"
line_rx = re.compile(numbers_line_rx)

# Examples of original file elements with patterns (FLUKA 4.4.1 24/07/2024)

'''
 *****  LHCb 19.04.2021 RICH PEshield study                                               *****

           DATE: 10/11/23,  TIME: 17:12:39

          Total number of particles followed           1000, for a total weight of  1.0000E+03

'''
date_time_rx = re.compile(r'DATE: (\d{2}/\d{2}/\d{2}),  TIME: (\d{2}:\d{2}:\d{2})')
particles_info_rx = re.compile(r'Total number of particles followed (\d+), for a total weight of  ([\d.E+]+)')

'''
   Track n.  61  "spNSn25   " , generalized particle n.    8, region n.  1425
      detector volume:  1.0000E+00 cm**3
      logar. energy binning from  2.0000E-02 to  3.0000E+02 GeV,   127 bins (ratio : 1.0787E+00)
'''

track_rx = re.compile(r'Track n\.\s+(\d+)\s+"(\w+)\s+"\s*,\s*generalized particle n\.\s+(\d+),\s*region n\.\s+(\d+)')
energy_bins_rx = re.compile(r'logar\.?\s*energy\s+binning\s+from\s+([\d.E+-]+)\s+to\s+([\d.E+-]+)\s+GeV,\s+(\d+)\s+bins\s+\(ratio\s+:\s+([\d.E+-]+)\)')
low_rx = re.compile(r'Low\s+energy\s+neutron\s+data\s+from\s+group\s+(\d+)\s+to\s+group\s+(\d+)\s+follow\s+in\s+a\s+vector\s+A\(ig\),\s+format\s+\((\d+\(\d+x,\d+p,\d+\(\d+x,e\d{2}\.\d\)\))\)')


def extractParticleFromNumber(particle_number):
    particle = scoringCodes.get_info_from_id(particle_number)
    particle = particle.replace(' ', '-')
    return(particle)


def extractHeader(file):
    headers = []
    #data = []
    header_started = False
    after_header = False

    with file as file:
        current_header = []
        #current_data = np.empty(0)
        for line in file:
            if not header_started:
                if after_header == True:
                    if not line.strip():
                        after_header = False
                else:
                    if track_rx.search(line):
                        header_started = True
                        current_header.append(line)
            if header_started:
                current_header.append(line)
                if not line.strip():
                    header_started = False
                    headers.append(''.join(current_header))
                    current_header = []
                    after_header = True
    return headers


def extractLowEnergyNeutrons(filename):
    ndata = []
    after_neutrons = False
    skip_next_line = False
    with file as file:
        for line in file:
            if skip_next_line:
                skip_next_line = False
                continue
            if after_neutrons:
                if not line.strip():
                    after_neutrons = False
                else:
                    numbers = rx.findall(line)
                    ndata = np.append(ndata, [float(num) for num in numbers])
            else:
                if low_rx.search(line):
                    after_neutrons = True
                    skip_next_line = True
    return ndata

def readGeneralInfo(filename):
    general_info = dict()
    file = open(filename, 'r')
    lines = file.readlines()
    title = lines[0]
    general_info['title'] = title.strip('*').strip()
    date_line = lines[2]
    date_time_match = date_time_rx.search(date_line)
    if date_time_match:
        general_info['date']= date_time_match.group(1)
        general_info['time'] = date_time_match.group(2)
    return(general_info)

def readFromHeader(header):
    parameters = dict()
    track_info = header.split('\n')
    for line in track_info:
        track= track_rx.search(line)
        if track:
            found = track.groups()
            parameters['track_number'] = float(found[0])
            parameters['track_name'] = found[1]
            parameters['particle_number'] = float(found[2])
            parameters['region_number'] = float(found[3])
        energy_bins = energy_bins_rx.search(line)
        if energy_bins:
            found = energy_bins.groups()
            parameters['binning_from'] = float(found[0])
            parameters['binning_to'] = float(found[1])
            parameters['no_bins'] = float(found[2])
            parameters['ratio'] = float(found[3])
    return(parameters)


def prepareRegionDict(file):
    if isinstance(file, str):
        file = open(file, 'r')

    # extracting based on pattern recognition. Be careful with changes from new FLUKA version!

    headers = extractHeader(file)

    params = []
    i = 0

    # extracting params for every heather and keeping them in list

    for header in headers:
        params.append(readFromHeader(header))
        i += 1

    region_dict = {}

    i=0
    for single_params in params:

        # obtaining the name of particle using particle number from FLUKA output and dictionary

        particle_key = extractParticleFromNumber(single_params["particle_number"])

        if particle_key == 0:
            return 0
        else:
            if particle_key not in region_dict:
                region_dict[particle_key] = {}  # Tworzymy nowy słownik dla klucza particle_key

            region_dict[particle_key].update(single_params)  # Dodajemy dane do istniejącego słownika

        i += 1

    return {"metadata": region_dict}

def main(file):

    """
    The program analyzes .txt file with USTRACK data from FLUKA and returns dictionary
    containing all necessary metadata for generating plots. You should only provide the
    path to the appropriate file. The dictionary is returned to the standard input.
    Not extracting data.

    Example fragment:
    {
    "Proton": {

            {
                "track_number": 101.0,
                "track_name": "AW16_p",
                "particle_number": 1.0,
                "region_number": 1426.0,
                "binning_from": 0.003,
                "binning_to": 8000.0,
                "no_bins": 100.0,
                "ratio": 1.1595
            }

    },
    "Neutron": {

            {
                "track_number": 102.0,
                "track_name": "AW16_n",
                "particle_number": 8.0,
                "region_number": 1426.0,
                "binning_from": 1e-14,
                "binning_to": 8000.0,
                "no_bins": 340.0,
                "ratio": 1.1289
            }
        ]
    },
    "Positive-Pion": {

            {
                "track_number": 103.0,
                "track_name": "AW16_pip",
                "particle_number": 13.0,
                "region_number": 1426.0,
                "binning_from": 0.003,
                "binning_to": 8000.0,
                "no_bins": 100.0,
                "ratio": 1.1595
            }

    },
    ...
    }
    """

    region_dict = prepareRegionDict(file)
    # the following line is not required for the endpoint:
    #json_output = json.dumps(region_dict, indent=4)
    return region_dict




if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Lethargy Fluence Spectra dictionary")
    parser.add_argument("--file", help = "directory to .txt file from main folder with data. Obtained by user choosing file")

    args = parser.parse_args()
    main(args.file)
