""".. moduleauthor:: Sacha E.R. Medaer"""

from typing import Optional

from .utils.constants.fluka_particle_code import FLUKA_PARTICLE_CODE

#FLUKA_PARTICLE_CODE = {ID: {'name', 'info', 'unit'}}


class ScoringCodesValueError(ValueError):
    pass


class ScoringCodes(object):
    """This class servers as a wrapper for FLUKA particle code,
    see FLUKA Manual version 4-4.1, p. 47.
    """

    def init(self) -> None:

        return None

    @staticmethod
    def _get_code_dict_content(code_id: int) -> dict:
        code_dict_key: Optional[dict] = FLUKA_PARTICLE_CODE.get(code_id)
        if (code_dict_key is not None):

            return code_dict_key
        else:
            error_message: str = "The requested FLUKA id does not exist."

            return ScoringCodesValueError(error_message)

    @staticmethod
    def _get_code_dict_key(code_id: int, code_id_key: str) -> str:
        code_id_value: Optional[dict]
        code_id_value = ScoringCodes._get_code_dict_content(code_id)[code_id_key]
        if ((code_id_value is not None) and isinstance(code_id_value, str)):

            return code_id_value
        else:
            error_message: str = "The id '{}' is not valid.".format(code_id_key)

            return ScoringCodesValueError(error_message)

    @staticmethod
    def get_info_from_id(code_id: int) -> str:

        return ScoringCodes._get_code_dict_key(code_id, 'info')


    @staticmethod
    def get_unit_from_id(code_id: int) -> str:

        return ScoringCodes._get_code_dict_key(code_id, 'unit')

    @staticmethod
    def get_name_from_id(code_id: int) -> str:

        return ScoringCodes._get_code_dict_key(code_id, 'name')

    @staticmethod
    def get_conv_factor(orig: str, dest: str) -> float:
        if (orig == dest):

            return 1.0

        if (orig.startswith('GeV/g') and dest.startswith("Gy")):

            return 1.602e-7
        elif (orig.startswith('pSv') and dest.startswith("mSv")):

            return 1e-9
        else:

            return 1e0
