""".. moduleauthor:: Sacha E.R. Medaer"""

import argparse
import copy
import math
import os
import sys

import numpy as np
import plotly as pl
import plotly.graph_objs as go
import pandas as pd
from typing import Optional, Union

from ..parsers.geometry_parsers.plotgeom_parser import PlotgeomParser
from ..programs.fluka_programs.plotgeom_program import PlotgeomProgram
from ..size_config import SIZE_CONFIG
from ..utils.constants.plot_colors import PLOT_2D_COLORS as COLORS
from ..utils.constants.color_scales import BUILTIN_COLORSCALES,\
                                           PLOTLY_COLORSCALES
from ..utils.helpers.plot_helpers import create_colorscale_from_colorlist


class UsrtrackPlotValueError(ValueError):
    pass

class UsrtrackPlotPathError(Exception):
    pass


class UsrtrackPlot(object):
    """This class plots the data from a USRTRACK scoring of FLUKA. The python
    library Plotly is used as a plotting tool.
    """
    def __init__(self, tracks: list = []) -> None:
        self._tracks: list[dict] = []
        self.add_track(*tracks)

        return None

    def add_track(self, *tracks) -> None:
        for track in tracks:
            self._tracks.append(track)

    @property
    def tracks(self) -> list[str]:

        return self._tracks

    @property
    def nbr_tracks(self) -> int:

        return len(self._tracks)

    @property
    def track_info_table(self) -> list[dict]:
        """Returns the track informatin, i.e. the tracks without data."""
        track_info_table = []
        keys_to_remove = ['data', 'err_data', 'bins', 'mid_bins']
        for track in self.tracks:
            track_info = dict()
            for key, value in track.items():
                if (key not in keys_to_remove):
                    track_info[key] = value
            if (track_info):
                track_info_table.append(track_info)

        return track_info_table

    @property
    def particle_names(self) -> list[str]:
        names = [track['particle_name'] for track in self.tracks]

        return list(set(names)) # unique element list

    @property
    def track_names(self) -> list[str]:

        return [track['track_name'] for track in self.tracks]

    @property
    def track_numbers(self) -> list[int]:

        return [int(track['track_nbr']) for track in self.tracks]

    @property
    def track_volumes(self) -> list[float]:

        return [float(track['volume']) for track in self.tracks]

    def get_track_volume(self, track_name) -> Optional[str]:
        if (track_name in self.track_names):
            ind = self.track_names.index(track_name)

            return str(self.tracks[ind]['volume'])
        else:

            None

    def get_log_dict(self, abs_min, abs_max):
        exp_min = int(np.floor(np.log10(abs_min)))
        mult_min = int(np.floor(10**(np.log10(abs_min) - exp_min)))
        exp_max = int(np.ceil(np.log10(abs_max)))
        mult_max = int(np.ceil(10**(np.log10(abs_max) - exp_max)))
        tickvals, ticktext = [], []
        for i in range(exp_min, exp_max+1, 1):
            start, end = 1, 9
            if (i == exp_min):
                start = mult_min
            if (i == exp_max):
                end = mult_max
            new_vals = [j*10**i for j in range(start, end+1, 1)]
            tickvals.extend(new_vals)
            new_text = ['10<sup>{}</sup>'.format(i) if (not (np.log10(val)%1)) else '' for val in new_vals]
            ticktext.extend(new_text)

        return dict(showgrid=True, type='log', gridcolor='white',
                    gridwidth=0.5, #range=[exp_min, exp_max],
                    tickmode='array',
                    tickvals=tickvals, ticktext=ticktext)

    def convert_to_lethargy_units(self, bins, y_data):
        """Convert the bins and fluence to lethargy units."""
        x_data = np.sqrt(bins[1:]*bins[:-1])

        return x_data, y_data*x_data

    def get_data_as_string(self, err: bool = False,
                           particle_names: list[str] = [],
                           track_names: list[str] = [],
                           track_volumes: list[float] = [],
                           log_x: bool = True, log_y: bool = True,
                           lethargy_unit: bool = True,
                           delimiter_char='\t', end_line_char='\n'
                           ) -> list[str]:
        """Return one string per track and particle."""
        list_txt_data = []
        list_particles = []
        for i, track_name in enumerate(track_names):
            ind = self.track_names.index(track_name)
            # track['volume'] is the original value used to compute track
            volume_factor = self.tracks[ind]['volume'] / track_volumes[i]
            particle_name = self.tracks[ind]['particle_name']
            bins = self.tracks[ind]['bins']
            unit = 'cm<sup>-2</sup>'
            txt = ('Energy[Gev]' + delimiter_char + 'value[{}]'.format(unit)
                    + delimiter_char + 'error[{}]'.format(unit) + end_line_char)
            if (particle_names and (particle_name in particle_names)):
                y_data = np.array(self.tracks[ind]['data']) * volume_factor
                err_y_data = np.array(self.tracks[ind]['err_data'])
                if (lethargy_unit and log_x):
                    x_data, y_data = self.convert_to_lethargy_units(bins,
                                                                    y_data)
                else:
                    x_data = (bins[:-1] + bins[1:])/2
                # Create error dictionary
                if (np.array(err_y_data).any() and err):
                        err_y_data *= y_data    # * volume_factor * x_data
                for i in range(len(x_data)):
                    txt += (str(x_data[i]) + delimiter_char + str(y_data[i]))
                    if (np.array(err_y_data).any() and err):
                        txt += (delimiter_char + str(err_y_data[i]))
                    txt += end_line_char
                list_txt_data.append(txt)
                list_particles.append(particle_name)

        return list_txt_data, list_particles


    def get_data_as_actiwiz_string(self,
                           track_names: list[str] = [],
                           track_volumes: list[float] = [],
                           delimiter_char='\t', end_line_char='\n'
                           ) -> list[str]:
        """Return one string per track and particle."""
        list_txt_data = []
        list_particles = []
        for i, track_name in enumerate(track_names):
            ind = self.track_names.index(track_name)
            # track['volume'] is the original value used to compute track
            volume_factor = self.tracks[ind]['volume'] / track_volumes[i]
            particle_name = self.tracks[ind]['particle_name']
            bins = self.tracks[ind]['bins']
            txt = '* PARTICLE TYPE: ' + self.tracks[ind]['particle_gen_name']
            txt += end_line_char
            txt += '* ActiWiz input file created with SimRad' + end_line_char
            y_data = np.array(self.tracks[ind]['data']) * volume_factor
            err_y_data = np.array(self.tracks[ind]['err_data'])
            for i in range(len(y_data)):
                txt += (str(bins[i]) + delimiter_char + str(bins[i+1])
                        + delimiter_char + str(y_data[i]))
                if (np.array(err_y_data).any()):
                    txt += (delimiter_char + str(err_y_data[i]))
                txt += end_line_char
            list_txt_data.append(txt)
            list_particles.append(particle_name)

        return list_txt_data, list_particles


    def get_plot_light(self, err: bool = False, particle_names: list[str] = [],
                       track_names: list[str] = [],
                       track_volumes: list[float] = [],
                       log_x: bool = True, log_y: bool = True,
                       lethargy_unit: bool = True, line_names: list[str] = [],
                       line_colors: list[str] = []):
        # Errors assumed to be relative
        # lethargy_unit is ignored if not log_x
        abs_y_min, abs_y_max = np.inf, -1*np.inf
        abs_x_min, abs_x_max = np.inf, -1*np.inf
        fig = go.Figure()
        for i, track_name in enumerate(track_names):
            ind = self.track_names.index(track_name)
            # track['volume'] is the original value used to compute track
            volume_factor = self.tracks[ind]['volume'] / track_volumes[i]
            particle_name = self.tracks[ind]['particle_name']
            if (particle_names and (particle_name in particle_names)):
                # Create bins and data
                bins = self.tracks[ind]['bins']
                y_data = np.array(self.tracks[ind]['data']) * volume_factor
                err_y_data = np.array(self.tracks[ind]['err_data'])
                if (lethargy_unit and log_x):
                    x_data, y_data = self.convert_to_lethargy_units(bins,
                                                                    y_data)
                else:
                    x_data = (bins[:-1] + bins[1:])/2
                # Create error dictionary
                error_y = dict()
                if (np.array(err_y_data).any() and err):
                        err_y_data *= y_data    # * volume_factor * x_data
                        error_y = dict(symmetric=True, array=err_y_data)

                non_zero_y_data = y_data[y_data!=0]
                filtered_y_data = non_zero_y_data[np.isfinite(non_zero_y_data)]
                crt_min = np.min(filtered_y_data)
                crt_max = np.max(filtered_y_data)
                abs_y_max = crt_max if (crt_max > abs_y_max) else abs_y_max
                abs_y_min = crt_min if (crt_min < abs_y_min) else abs_y_min
                x_data = x_data[y_data!=0]
                crt_min, crt_max = np.min(x_data), np.max(x_data)
                abs_x_max = crt_max if (crt_max > abs_x_max) else abs_x_max
                abs_x_min = crt_min if (crt_min < abs_x_min) else abs_x_min
                line_name = '<b>' + particle_name + '</b>'
                if (line_names[i]):
                    line_name += ' (' + line_names[i] + ')'
                line_name += ('<br>[' + '{:.2e}'.format(bins[0]) + ', '
                              + '{:.2e}'.format(bins[-1]) + '] GeV')
                line = go.Scatter(x=x_data, y=non_zero_y_data,
                                  mode='markers',
                                  name=line_name,
                                  error_y=error_y)
                # Adding hovering text
                hovertext = []
                for j, ind in enumerate(np.argwhere(y_data!=0)):
                    text = f'<b>{particle_name}</b>'
                    if (line_names[i]):
                        text += ' (' + line_names[i] + ')'
                    text += ('<br>Energy bin: ' + '{:.4e}'.format(bins[ind[0]])
                             + ' to ' + '{:.4e}'.format(bins[ind[0]+1])
                             + ' GeV<br>')
                    if (lethargy_unit and log_x):
                        text += 'Lethargy fluence: '
                    else:
                        text += 'Fluence: '
                    text += '{:.4e}'.format(non_zero_y_data[j])
                    text += ' cm<sup>-2</sup>'
                    hovertext.append(text)
                line.hoverinfo = 'text'
                line.text = hovertext
                # Add colors if specified
                if (line_colors):
                    line.line['color']=line_colors[i]
                fig.add_trace(line)

        title=  'Lethargy Fluence Spectra for region'
        if (lethargy_unit and log_x):
            yaxis_title='Lethargy fluence (cm<sup>-2</sup>)'
        else:
            yaxis_title='Fluence (cm<sup>-2</sup>)'
        fig.update_layout(xaxis_title='Energy (GeV)',
                          yaxis_title=yaxis_title,
                          )
        if (log_y and track_names):
            y_log_dict = self.get_log_dict(abs_y_min, abs_y_max)
            fig.update_layout(yaxis=y_log_dict)
        if (log_x and track_names):
            x_log_dict = self.get_log_dict(abs_x_min, abs_x_max)
            fig.update_layout(xaxis=x_log_dict)

        return fig


    def get_html_plot(self):

        return pl.io.to_html(self.get_plot(), include_plotlyjs='cdn')



if __name__ == "__main__":
    """
    The program accepts a original file path, and the projection axis (x, y, or z),
    as well as the projection depth (must match the bins, available projections are listed in the dictionary
    under keys 'bins_x', 'bins_y', 'bins_z'). By default, it displays the main plot - the error plot can be viewed
    using an optional argument. Default scaling is set to one collision. The plot can be rescaled using an optional
    argument.

    .ERR files must be stored in exact same location as main file.
    """


    parser = argparse.ArgumentParser(description=" ")
    parser.add_argument("--file", help="Path to the USRTRACK file")

    parser.add_argument(
        '--errfile',
        default='',
        help="aou"
    )

    args = parser.parse_args()
    usrtrack_plot = UsrtrackPlot(args.file, args.errfile)
    fig = usrtrack_plot.get_plot_light(particle_names=['Neutron'])#[])#['Photon'])
    #fig.write_html("usrtrack_html_slice_with_map.html")
    fig.show()
    #usrtrack_plot.get_html_plot()
