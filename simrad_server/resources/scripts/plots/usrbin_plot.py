""".. moduleauthor:: Sacha E.R. Medaer"""

import argparse
import copy
import math
import os
import sys

import numpy as np
import plotly as pl
import plotly.graph_objs as go
from typing import Optional, Union

from ..parsers.geometry_parsers.plotgeom_parser import PlotgeomParser
from ..programs.fluka_programs.plotgeom_program import PlotgeomProgram
from ..size_config import SIZE_CONFIG
from ..utils.constants.plot_colors import PLOT_2D_COLORS as COLORS
from ..utils.constants.color_scales import BUILTIN_COLORSCALES,\
                                           PLOTLY_COLORSCALES
from ..utils.helpers.plot_helpers import create_colorscale_from_colorlist

PROJECTIONS = ['x', 'y', 'z']
PROJ_TO_AXES = {'x': ['z', 'y', 'x'], 'y': ['z', 'x', 'y'],
                'z': ['x', 'y', 'z']}


class UsrbinPlotValueError(ValueError):
    pass

class UsrbinPlotPathError(Exception):
    pass


class UsrbinPlot(object):
    """This class plots the data from a USRBIN scoring of FLUKA. The python
    library Plotly is used as a plotting tool.
    """
    def __init__(self, bin: dict, data_ratio=False) -> None:
        file_error_message: str = ("The specified file path for the "
                                   "USRBIN scorings does not exist.")

        self._usrbin_data = bin['data']
        self._usrbin_err_data = bin['err_data']

        self._usrbin_info = bin

        #if (self._usrbin_data.shape != self._usrbin_err_data.shape):
        #    error_message: str = ("The USRBIN scoring errors do not "
        #                          "possess the same dimension as the "
        #                          "original USRBIN scorings.")
        #
        #    raise UsrbinPlotValueError(error_message)
        self._data_ratio = data_ratio

        self._inp_file = ''


        return None


    def __truediv__(self, operand):
        #print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')
        #print(self.original_shape, operand.original_shape)
        #print(self.unit, operand.unit)
        if ((self.original_shape == operand.original_shape)
                and (self.unit == operand.unit)
                and (self.particle_name == operand.particle_name)):
            new_plot = copy.deepcopy(self)
            # temporary
            new_plot._usrbin_data = self._usrbin_data / operand._usrbin_data
            new_err_data = np.sqrt(np.square(self._usrbin_err_data)
                                   + np.square(operand._usrbin_err_data))
            new_plot._usrbin_err_data = new_err_data
            new_plot._data_ratio = True
            new_plot._usrbin_info['unit'] = self.unit
            new_plot._usrbin_info['particle_name'] = self.particle_name

            return new_plot
        else:

            return None

    @property
    def data_ratio(self) -> str:

        return self._data_ratio

    @property
    def inp_file_path(self) -> str:

        return self._inp_file

    @property
    def original_shape(self):

        return self._usrbin_data.shape

    @property
    def projection(self) -> str:

        return self._projection

    @projection.setter
    def projection(self, proj: str) -> None:
        if (isinstance(proj, str) and (proj.lower() in PROJECTIONS)):
            self._projection: str = proj.lower()
        else:
            error_message: str = "The projection possibilies are {}".format(
                                    str(PROJECTIONS))

            raise UsrbinPlotValueError(error_message)

    @property
    def unit(self) -> str:

        return self._usrbin_info['unit']

    @property
    def bin_info_dict(self) -> list[dict]:
        """Returns the bin information, i.e. the bins without data."""
        keys_to_remove = ['data', 'err_data', 'bins', 'mid_bins']
        bin = self._usrbin_info
        bin_info = dict()
        for key, value in bin.items():
            if (key not in keys_to_remove):
                bin_info[key] = value

        return bin_info

    @property
    def particle_name(self) -> str:

        return self._usrbin_info['particle_name']


    def get_original_bins(self, axis):
        """Return the bins and bin spacing of the specified axis."""
        bins, bin_size = np.linspace(self._usrbin_info[axis]['bin_start'],
                                     self._usrbin_info[axis]['bin_end'],
                                     self._usrbin_info[axis]['nbr_bins']+1,
                                     True, True)
        return bins, bin_size


    @staticmethod
    def reduce_bins(bins: list[float], avg: int = 1):

        return [bins[i] for i in range(0, len(bins), avg)]

    def get_bins(self, axis: str, axis_avg: int = 1):
        """Return the bins and bin spacing of the specified axis."""
        if (axis in PROJECTIONS):
            bins, bin_size = self.get_original_bins(axis)
            if (axis_avg != 1):     # avoid unnecessary operations
                bins = UsrbinPlot.reduce_bins(bins, axis_avg)
                bin_size *= axis_avg

            return bins, bin_size
        else:

            return None

    def _average_matrix(self, data: np.ndarray,
                        axes_avgs: list[int] = [1, 1, 1]):
        # Check if the requested interval over axis is possible and apply
        if (data.size):
            if (axes_avgs[0] != 1):
                data = data.reshape(-1, axes_avgs[0], data.shape[1],
                                    data.shape[2]).mean(axis=1)
            if (axes_avgs[1] != 1):
                data = data.reshape(data.shape[0], -1, axes_avgs[1],
                                    data.shape[2]).mean(axis=2)
            if (axes_avgs[2] != 1):
                data = data.reshape(data.shape[0], data.shape[1], -1,
                                    axes_avgs[2]).mean(axis=3)

        return data


    def get_orig_data(self):

        return self._usrbin_data

    def get_data(self, axes_avgs: list[int] = [1, 1, 1]):

        return self._average_matrix(self._usrbin_data, axes_avgs)

    def get_orig_abs_err_data(self):

        if (self._usrbin_err_data.size):

            return self._usrbin_err_data*self._usrbin_data
        else:

            return self._usrbin_err_data

    def get_abs_err_data(self, axes_avgs: list[int] = [1, 1, 1]):
        if (self._usrbin_err_data.size):

            return self._average_matrix(self._usrbin_err_data*self._usrbin_data,
                                        axes_avgs)
        else:

            return self._usrbin_err_data

    def get_orig_rel_err_data(self):

        return self._usrbin_err_data

    def get_rel_err_data(self, axes_avgs: list[int] = [1, 1, 1]):

        return self._average_matrix(self._usrbin_err_data, axes_avgs)

    def calc_extrema(self, data):
        if (data.size):

            return (np.min(data[np.isfinite(data)]),
                    np.max(data[np.isfinite(data)]))
        else:

            return (0. , 0.)

    def calc_percentile(self, q, data):
        if (data.size):

            return np.percentile(data[np.isfinite(data)], q)
        else:

            return 0.

    def get_extrema(self, axes_avgs: list[int] = [1, 1, 1]):

        return self.calc_extrema(self.get_data(axes_avgs))

    def get_abs_err_extrema(self, axes_avgs: list[int] = [1, 1, 1]):

        return self.calc_extrema(self.get_abs_err_data(axes_avgs))

    def get_rel_err_extrema(self, axes_avgs: list[int] = [1, 1, 1]):

        return self.calc_extrema(self.get_rel_err_data, axes_avgs)

    def get_rel_err_percentile(self, q: int, axes_avgs: list[int] = [1, 1, 1]):
        usrbin_data = self.get_rel_err_data(axes_avgs)

        return self.calc_percentile(q, usrbin_data)

    def get_log10_extrema(self, axes_avgs: list[int] = [1, 1, 1],
                          multiplicator: float = 1.0):

        usrbin_data = self.get_data(axes_avgs)
        with np.errstate(divide='ignore'):
            log10_data = np.log10(multiplicator*usrbin_data)

        return self.calc_extrema(log10_data)

    def get_log10_percentile(self, q: int, axes_avgs: list[int] = [1, 1, 1],
                             multiplicator: float = 1.0):
        usrbin_data = self.get_data(axes_avgs)
        with np.errstate(divide='ignore'):
            log10_data = np.log10(multiplicator*usrbin_data)

        return self.calc_percentile(q, log10_data)

    def get_log10_abs_err_extrema(self, axes_avgs: list[int] = [1, 1, 1],
                                  multiplicator: float = 1.0):
        usrbin_data = self.get_abs_err_data(axes_avgs)
        with np.errstate(divide='ignore'):
            log10_data = np.log10(multiplicator*usrbin_data)

        return self.calc_extrema(log10_data)

    def get_log10_abs_err_percentile(self, q: int,
                                     axes_avgs: list[int] = [1, 1, 1],
                                     multiplicator: float = 1.0):
        usrbin_data = self.get_abs_err_data(axes_avgs)
        with np.errstate(divide='ignore'):
            log10_data = np.log10(multiplicator*usrbin_data)

        return self.calc_percentile(q, log10_data)

    def get_box_statistics(self, x_min, x_max, y_min, y_max, z_min, z_max,
                           multiplicator: float = 1.0):
        # no axis average here, must be the real stat of the data
        box_inds = []
        extr = [(x_min, x_max), (y_min, y_max), (z_min, z_max)]
        for i, axis in enumerate(['x', 'y', 'z']):
            bins, bin_size = self.get_bins(axis)
            box_inds.append((int((extr[i][0]-bins[0])/bin_size),
                             int((extr[i][1]-bins[0])/bin_size)))
        box = self._usrbin_data[box_inds[0][0]:box_inds[0][1]+1,
                                box_inds[1][0]:box_inds[1][1]+1,
                                box_inds[2][0]:box_inds[2][1]+1]
        err_box = self._usrbin_err_data[box_inds[0][0]:box_inds[0][1]+1,
                                        box_inds[1][0]:box_inds[1][1]+1,
                                        box_inds[2][0]:box_inds[2][1]+1]
        if (multiplicator != 1.0):
            box *= multiplicator
        ind_to_keep = np.logical_and(np.isfinite(box), np.isfinite(err_box))
        box, err_box = box[ind_to_keep], err_box[ind_to_keep]

        return (np.mean(box), np.mean(err_box)*np.mean(box),
                np.mean(err_box)*100, np.min(box), np.max(box))

    def get_projected_bins(self, projection, axes_avgs):
        # Extract the bins of each dimension
        # LHCb axes: [x, y, z], Plot axes: [x', y', z']
        # Proj. |   x'  |   y'
        # --------------------
        #   x   |   z   |   y
        #   y   |   z   |   x
        #   z   |   x   |   y
        # Variables:
        #               - usrbin_data: np.ndarray = [x, y, z]
        #               - axes: list[str] = [x', y', proj]
        # Calculate bins and bin labels
        if (projection in PROJECTIONS):
            axes: list[str] = PROJ_TO_AXES[projection]
            bins, bin_sizes = [], []
            for axis in axes:
                axis_avg = axes_avgs[ord(axis)-ord(PROJECTIONS[0])]
                new_bins, bin_size = self.get_bins(axis, axis_avg)
                bins.append(new_bins)
                bin_sizes.append(bin_size)

            return bins, bin_sizes
        else:

            return None


    def get_index_slice(self, depth, bins, bin_sizes):
        # The slice is considered to be taken in the third dimension of the bins

        return int((depth-bins[2][0])/bin_sizes[2])


    def get_slice_data(self, projection: str, depth: float,
                       axes_avgs: list[int] = [1, 1, 1],
                       multiplicator: float = 1.0,
                       percentage_rel_err: bool = True):
        # Get current data
        usrbin_data = self.get_data(axes_avgs)
        usrbin_abs_err_data = self.get_abs_err_data(axes_avgs)
        usrbin_rel_err_data = self.get_rel_err_data(axes_avgs)
        # Select Data
        bins, bin_sizes = self.get_projected_bins(projection, axes_avgs)
        ind = self.get_index_slice(depth, bins, bin_sizes)
        if (projection == 'x'):
            extr_data = copy.copy(usrbin_data[ind, :, :])
            if (usrbin_rel_err_data.size):
                extr_abs_err_data = copy.copy(usrbin_abs_err_data[ind, :, :])
                extr_rel_err_data = copy.copy(usrbin_rel_err_data[ind, :, :])
        elif (projection == 'y'):
            extr_data = copy.copy(usrbin_data[:, ind, :])
            if (usrbin_rel_err_data.size):
                extr_abs_err_data = copy.copy(usrbin_abs_err_data[:, ind, :])
                extr_rel_err_data = copy.copy(usrbin_rel_err_data[:, ind, :])
        else:
            # need here to take .T, because z -> x, y
            extr_data = copy.copy(usrbin_data[:, :, ind].T)
            if (usrbin_rel_err_data.size):
                extr_abs_err_data = copy.copy(usrbin_abs_err_data[:, :, ind].T)
                extr_rel_err_data = copy.copy(usrbin_rel_err_data[:, :, ind].T)

        extr_data *= multiplicator
        if (usrbin_rel_err_data.size):
            extr_abs_err_data *= multiplicator
            if (percentage_rel_err):
                extr_rel_err_data *= 100    # in percentage

        if (usrbin_rel_err_data.size):

            return (bins[0], bins[1], extr_data, extr_abs_err_data,
                    extr_rel_err_data)
        else:

            return (bins[0], bins[1], extr_data, np.array([]), np.array([]))


    def get_slice_data_as_string(self, projection: str, depth: float,
                                 axes_avgs: list[int] = [1, 1, 1],
                                 data_type: str = 'avg',
                                 multiplicator: float = 1.0,
                                 percentage_rel_err: bool = True,
                                 delimiter_char: str = '\t',
                                 end_line_char: str = '\n'):
        # Get current bins
        axes: list[str] = PROJ_TO_AXES[projection]
        bins, bin_sizes = self.get_projected_bins(projection, axes_avgs)
        ind = self.get_index_slice(depth, bins, bin_sizes)
        mid_third_bin = (bins[2][ind+1] + bins[2][ind]) / 2.0
        # Select Data
        slice_data = self.get_slice_data(projection, depth, axes_avgs,
                                         multiplicator, percentage_rel_err)
        x_bins, y_bins, avg_data, abs_err_data, rel_err_data = slice_data

        txt = ('x[cm]' + delimiter_char + 'y[cm]' + delimiter_char
               + 'z[cm]' + delimiter_char + 'value[{}]'.format(self.unit)
               + delimiter_char + 'error[{}]'.format(self.unit) + end_line_char)

        for i in range(avg_data.shape[0]):
            for j in range(avg_data.shape[1]):
                value = avg_data[i][j]
                error = abs_err_data[i][j]
                mid_bins = [(x_bins[j+1] + x_bins[j]) / 2.0,
                            (y_bins[i+1] + y_bins[i]) / 2.0,
                            mid_third_bin]
                ordered_mid_bins = []
                for axis in ['x', 'y', 'z']:
                    crt_ind = axes.index(axis)
                    ordered_mid_bins.append(mid_bins[crt_ind])
                txt += ('{:.2f}'.format(ordered_mid_bins[0]) + delimiter_char
                        + '{:.2f}'.format(ordered_mid_bins[1]) + delimiter_char
                        + '{:.2f}'.format(ordered_mid_bins[2]) + delimiter_char
                        + str(value) + delimiter_char + str(error)
                        + end_line_char)

        return txt


    def get_raw_data_as_string(self, delimiter_char='\t', end_line_char='\n'):
        """Return two stings, the first being the values, and the second the
        errors. In each string, the first line is the X-bins (size M+1), the
        second line is the Y-bins (size N+1), and the third line is the Z-bins
        (size P+1). These three lines are followed by M times a matrix of size
        (N x P)."""

        def get_str_1D_array(array, delimiter_char, end_line_char):
            str_array: str = ''
            for i in range(len(array)):
                str_array += str(array[i])
                if (i < (len(array)-1)):
                    str_array += delimiter_char
                else:
                    str_array += end_line_char

            return str_array

        txt: str = ''
        for axis in ['x', 'y', 'z']:
            txt += get_str_1D_array(self.get_original_bins(axis)[0],
                                    delimiter_char, end_line_char)
        txts = [copy.copy(txt), copy.copy(txt)] # value and errors
        data = [self.get_orig_data(), self.get_orig_abs_err_data()]
        for ind in range(len(txts)):
            for i in range(len(data[ind])): # x
                for j in range(len(data[ind][i])):  # y
                    txts[ind] += get_str_1D_array(data[ind][i][j],
                                                  delimiter_char, end_line_char)

        return txts


    def get_plot(self, projection='x', depth=0., nbr_collisions=1,
                 scale_min_expo_val: Optional[int] = None,
                 scale_max_expo_val: Optional[int] = None,
                 colorscale: Union[str, list] = 'viridis',
                 axes_avgs = [1, 1, 1], abs_err: bool = False,
                 rel_err: bool = False, plot_map: bool = False,
                 fluka_dir: str = '', working_dir: str = '',
                 map_name: str = ''):
        # Load data
        # plot abs_err by default if both abs_err and rel_err are True
        usrbin_info = self._usrbin_info
        # Get current bins
        axes: list[str] = PROJ_TO_AXES[projection]
        bins, bin_sizes = self.get_projected_bins(projection, axes_avgs)
        ind = self.get_index_slice(depth, bins, bin_sizes)
        zis = [ind, ind+1]
        # Select Data
        slice_data = self.get_slice_data(projection, depth, axes_avgs,
                                         multiplicator=nbr_collisions,
                                         percentage_rel_err=True)
        _, _, avg_data, abs_err_data, rel_err_data = slice_data
        if ((abs_err or rel_err) and (not rel_err_data.size)):
            # returning empty graph, there is no error

            return go.Figure()

        unit = usrbin_info['unit']
        quantity_name = usrbin_info['particle_name']
        colorbar = {'title_side': 'right'}
        # looking for ratio plot with values between 0 and 1
        if (False):#self._data_ratio and np.all(avg_data < 1)):
            if (abs_err or (not rel_err)):
                crt_data = abs_err_data if abs_err else avg_data
            else:
                crt_data = rel_err_data
            if (scale_min_expo_val is None):
                scale_min_expo_val = 0.0
            if (scale_max_expo_val is None):
                scale_max_expo_val = 1.0
        else:
            # No logarithmic scale available, plot log10 and change scale
            with np.errstate(divide='ignore'):
                avg_data = np.log10(avg_data)
                if (abs_err_data.size):
                    abs_err_data = np.log10(abs_err_data)
            # Create variables -------------------------------------------------
            if (abs_err or (not rel_err)):
                crt_data = abs_err_data if abs_err else avg_data
                if (scale_min_expo_val is None):
                    scale_min_val = np.min(avg_data[np.isfinite(avg_data)])
                    scale_min_expo_val = int(math.floor(scale_min_val))
                if (scale_max_expo_val is None):
                    scale_max_val = np.max(avg_data[np.isfinite(avg_data)])
                    scale_max_expo_val = int(math.ceil(scale_max_val))
                tickvals =  [exp for exp in range(scale_min_expo_val,
                                                  scale_max_expo_val+1)]
                ticktext = ["1e{}".format(str(elem)) for elem in tickvals]
                colorbar['tickvals'] = tickvals
                colorbar['ticktext'] = ticktext
                if (self._data_ratio):
                    colorbar['title'] = 'Ratio'# of {}'.format(quantity_name)
                else:
                    colorbar['title'] = '{} ({})'.format(quantity_name, unit)
            else:
                crt_data = rel_err_data
                colorbar['title'] = 'Percentage %'
        # Create Heatmaps and Figure -------------------------------------------
        fig = go.Figure()
        # Create heatmap
        heatmap_data = go.Heatmap(z=crt_data,
                                  x=bins[0], y=bins[1],
                                  zmin=scale_min_expo_val,
                                  zmax=scale_max_expo_val,
                                  colorbar=colorbar)
        heatmap_data.colorscale = colorscale
        heatmap_data.visible = True
        heatmap_data.name = ''
        # Create text for hovering
        hovertext = []  # first index is y-value, second is the x-value
        for yi in range(len(bins[1])-1):
            hovertext.append([])
            for xi in range(len(bins[0])-1):
                text = '<b>Voxel Information</b>:<br>'
                text += '{}: [{}, {}] cm<br>'.format(axes[0], bins[0][xi],
                                                     bins[0][xi+1])
                text += '{}: [{}, {}] cm<br>'.format(axes[1], bins[1][yi],
                                                     bins[1][yi+1])
                text += '{}: [{}, {}] cm<br>'.format(axes[2], bins[2][zis[0]],
                                                     bins[2][zis[0]+1])
                text += '<b>Voxel Values</b>:<br>'
                text += ('Mean Value: {:.4e}'.format(10**avg_data[yi][xi])
                         + ' {}<br>'.format(unit))
                if (rel_err_data.size):
                    text += ('Absolute Error: '
                             + '{:.4e}'.format(10**abs_err_data[yi][xi])
                             + ' {}<br>'.format(unit))
                    text += ('Relative Error: '
                             + '{:.4f}'.format(rel_err_data[yi][xi])
                             + ' %<br>')
                hovertext[-1].append(text)
        heatmap_data.hoverinfo = 'text'
        heatmap_data.text = hovertext
        fig.add_trace(heatmap_data)

        # Plot 2D map ----------------------------------------------------------
        if (plot_map and self._inp_file):
            # to put somewhere else
            direction_cosines_ri = [1.0, 0.0, 0.0]  # right (x)
            direction_cosines_up = [0.0, 1.0, 0.0]  # up    (y)
            direction_cosines_in = [0.0, 0.0, 1.0]  # in    (z)
            direction_cosines_ou = [1.0, 0.0, 0.0]  # out

            if (self.projection == 'x'):    # z -> x, and y -> y
                direction_cosines = direction_cosines_up + direction_cosines_in
                plane_points = [bins[2][zis[0]], bins[1][0], bins[0][0],
                                bins[2][zis[0]], bins[1][-1], bins[0][-1]]
            elif (self.projection == 'y'):
                direction_cosines = direction_cosines_ou + direction_cosines_up
                plane_points = [bins[1][0], bins[2][zis[0]], bins[0][0],
                                bins[1][-1], bins[2][zis[0]], bins[0][-1]]
            else:
                direction_cosines = direction_cosines_up + direction_cosines_ri
                plane_points = [bins[0][0], bins[1][0], bins[2][zis[0]],
                                bins[0][-1], bins[1][-1], bins[2][zis[0]]]

            # Temporary: getting cross-sections  -------------------------------
            inst = PlotgeomProgram(self._inp_file)
            working_dir = os.path.dirname(os.path.realpath(self._inp_file))
            plotgeom_file = inst.run(1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                                     *plane_points,
                                     *direction_cosines,
                                     0.0, 0.0, 0.0, 0.0,
                                     working_dir=working_dir)
            x_p, y_p = PlotgeomParser(plotgeom_file).get_segments_as_line(
                                    x_offset=bins[0][0], y_offset=bins[1][0])
            fig.add_trace(go.Scatter(x=x_p, y=y_p, mode='lines',
                                     line_color='black',
                                     showlegend=False,
                                     visible=True, name=map_name,
                                     hoverinfo='none',
                                     opacity=0.5),)
        # Plot Parameters ------------------------------------------------------
        title: str = '{} ({})'.format(quantity_name, unit)
        title += '<br>in [{:.2f}, {:.2f}] cm '.format(bins[2][ind],
                                                      bins[2][ind+1])
        title += 'along {}-axis'.format(projection.upper())
        fig.update_xaxes(title=axes[0].capitalize()+' (cm)')#, showgrid=False,
                         #constrain='domain')
        fig.update_yaxes(title=axes[1].capitalize()+' (cm)')#, showgrid=False,
                         #constrain='domain')
        fig.update_layout(title_text=title, title_x=0.5)

        return fig


    def get_html_plot(self):

        return pl.io.to_html(self.get_plot(), include_plotlyjs='cdn')



if __name__ == "__main__":
    """
    The program accepts a original file path, and the projection axis (x, y, or z),
    as well as the projection depth (must match the bins, available projections are listed in the dictionary
    under keys 'bins_x', 'bins_y', 'bins_z'). By default, it displays the main plot - the error plot can be viewed
    using an optional argument. Default scaling is set to one collision. The plot can be rescaled using an optional
    argument.

    .ERR files must be stored in exact same location as main file.
    """


    parser = argparse.ArgumentParser(description=" ")
    parser.add_argument("--file", help="Path to the USRBIN file")
    parser.add_argument("--proj", help="projection (x, y or z)")
    parser.add_argument("--depth", help="Depth in the projected direction [cm]")

    parser.add_argument(
        '--coli',
        nargs='?',
        default=1,
        help="Number of collisions. Default = 1."
    )
    parser.add_argument(
        '--size',
        nargs='?',
        default='medium',
        choices=['small', 'medium', 'large'],
        help="Size of the plot (for webpage purposes). Default = medium."
    )

    parser.add_argument(
        '--inpfile',
        nargs='?',
        default='',
        help="Path to the input file used to generate the USRBIN."
    )

    args = parser.parse_args()
    usrbin_plot = UsrbinPlot(args.file, args.proj, float(args.depth),
                             inp_file=args.inpfile)
    fig = usrbin_plot.get_plot()
    #fig.write_html("usrbin_html_slice_with_map.html")
    fig.show()
    #usrbin_plot.get_html_plot()
