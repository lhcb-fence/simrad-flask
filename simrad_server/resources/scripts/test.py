import plotly.graph_objects as go
import numpy as np
import re

regex=r"[-+]?[0-9]*\.?[0-9]+(?:[eE][-+]?[0-9]+)?"

file_path = 'simrad_server/resources/scripts/worm_test.txt'
file_path = 'simrad_server/resources/scripts/worm_lhcb_test.txt'
#file_path = 'C:\Users\samedaer\cernbox\Documents\FLUKA\test_4\20240820_lhcb_14TeV_fullCavern001_PLOTGEOM.STORE'

x_offset = -600
y_offset = -900

counter = 0
length = 0
x = []
y = []
with open(file_path, 'r') as f:
    for raw_line in f.readlines():
        line = raw_line.replace(" ", "")
        if (counter < length):
            #print(raw_line.split("   "))
            temp_list = re.findall(regex, raw_line)
            # check if temp_list is empty
            #segments[-1][counter] = np.array(temp_list)
            x.append(float(temp_list[0])+x_offset)
            y.append(float(temp_list[1])+y_offset)
            counter += 1

        if (line.startswith("Wormno")): # Worm found
            # append None to break segements
            x.append(float('nan'))
            y.append(float('nan'))
            length = int(line.split('length')[1])
            #segments.append(np.empty((length, 2)))
            counter = 0

#data = np.loadtxt(file_path)
#print(x, y)
#print(x.shape)
#print(y.shape)
#x = np.arange(12)
#y = np.arange(12)


fig = go.Figure()
#for i in range(0, len(segments)):
#    fig.add_trace(go.Scatter(x=segments[i][:, 0], y=segments[i][:, 1],
#                             mode='lines', line_color='black'),)
fig.add_trace(go.Scatter(x=x, y=y, mode='lines', line_color='black',
                         opacity=0.5, showlegend=False),)
fig.show()
