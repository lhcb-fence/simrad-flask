#!/bin/env python

import re
import os
import json
import argparse
import numpy as np
from simrad_server.resources.scripts.scoring_codes import ScoringCodes # #

DATA_DIR = os.getenv("PLOTS_DIR", "")

scoringCodes = ScoringCodes()

exp_num_rx = r"[-+]?\d*\.\d+[eE][-+]?\d+" # pattern, example 3.2465e+10
rx = re.compile(exp_num_rx)
numbers_line_rx = r"/^ (\s* #{$rx} \s*)+ $/x"
line_rx = re.compile(numbers_line_rx)


#Examples of file elements
'''
 *****  LHCb 19.04.2021 RICH PEshield study                                               *****

           DATE: 10/11/23,  TIME: 17:12:39

          Total number of particles followed           1000, for a total weight of  1.0000E+03

'''
date_time_rx = re.compile(r'DATE: (\d{2}/\d{2}/\d{2}),  TIME: (\d{2}:\d{2}:\d{2})')
particles_info_rx = re.compile(r'Total number of particles followed (\d+), for a total weight of  ([\d.E+]+)')

'''
   Track n.  61  "spNSn25   " , generalized particle n.    8, region n.  1425
      detector volume:  1.0000E+00 cm**3
      logar. energy binning from  2.0000E-02 to  3.0000E+02 GeV,   127 bins (ratio : 1.0787E+00)
'''

track_rx = re.compile(r'Track n\.\s+(\d+)\s+"(\w+)\s+"\s*,\s*generalized particle n\.\s+(\d+),\s*region n\.\s+(\d+)')
#volume_rx = re.compile(r'detector\s+volume:\s+([\d.E+]+)\s+cm\*\*3')
energy_bins_rx = re.compile(r'logar\.?\s*energy\s+binning\s+from\s+([\d.E+-]+)\s+to\s+([\d.E+-]+)\s+GeV,\s+(\d+)\s+bins\s+\(ratio\s+:\s+([\d.E+-]+)\)')
low_rx = re.compile(r'Low\s+energy\s+neutron\s+data\s+from\s+group\s+(\d+)\s+to\s+group\s+(\d+)\s+follow\s+in\s+a\s+vector\s+A\(ig\),\s+format\s+\((\d+\(\d+x,\d+p,\d+\(\d+x,e\d{2}\.\d\)\))\)')

def extractParticleFromNumber(particle_number):
    particle = scoringCodes.get_info_from_id(particle_number)
    particle = particle.replace(' ', '-')
    return(particle)

def extractHeadersAndData(filename):
    headers = []
    data = []
    header_started = False
    after_header = False

    with open(filename, 'r') as file:
        current_header = []
        current_data = np.empty(0)
        for line in file:
            if not header_started:
                if after_header == True:
                    if not line.strip():
                        after_header = False
                        data.append(current_data)
                        current_data = []
                    else:
                        numbers = rx.findall(line)
                        current_data = np.append(current_data, [float(num) for num in numbers])
                else:
                    if track_rx.search(line):
                        header_started = True
                        current_header.append(line)
            if header_started:
                current_header.append(line)
                if not line.strip():
                    header_started = False
                    headers.append(''.join(current_header))
                    current_header = []
                    after_header = True
        if after_header == True:
            data.append(current_data)
    return headers, data

def extractLowEnergyNeutrons(filename):
    ndata = []
    after_neutrons = False
    skip_next_line = False
    with open(filename, 'r') as file:
        for line in file:
            if skip_next_line:
                skip_next_line = False
                continue
            if after_neutrons:
                if not line.strip():
                    after_neutrons = False
                else:
                    numbers = rx.findall(line)
                    ndata = np.append(ndata, [float(num) for num in numbers])
            else:
                if low_rx.search(line):
                    after_neutrons = True
                    skip_next_line = True
    return ndata

def readGeneralInfo(filename):
    general_info = dict()
    file = open(filename, 'r')
    lines = file.readlines()
    title = lines[0]
    general_info['title'] = title.strip('*').strip()
    date_line = lines[2]
    particles_line = lines[4]
    date_time_match = date_time_rx.search(date_line)
    if date_time_match:
        general_info['date']= date_time_match.group(1)
        general_info['time'] = date_time_match.group(2)
    return(general_info)

def readFromHeader(header):
    parameters = dict()
    track_info = header.split('\n')
    for line in track_info:
        track= track_rx.search(line)
        if track:
            found = track.groups()
            parameters['track_number'] = float(found[0])
            parameters['track_name'] = found[1]
            parameters['particle_number'] = float(found[2])
            parameters['region_number'] = float(found[3])
        energy_bins = energy_bins_rx.search(line)
        if energy_bins:
            found = energy_bins.groups()
            parameters['binning_from'] = float(found[0])
            parameters['binning_to'] = float(found[1])
            parameters['no_bins'] = float(found[2])
            parameters['ratio'] = float(found[3])
    return(parameters)


def prepareRegionDict(region_source_path,filename):

    headers, data = extractHeadersAndData(region_source_path)
    ndata = extractLowEnergyNeutrons(region_source_path)

    params = []
    i = 0
    for header in headers:
        params.append(readFromHeader(header))
        i += 1

    region_dict = {}

    i=0
    for single_params in params:
        particle_key = extractParticleFromNumber(single_params["particle_number"])

        if particle_key == 0:
            return 0
        else:
            if particle_key not in region_dict:
                if particle_key != 'Neutron':
                    region_dict[particle_key] = {'params': [], 'data': []}
                else:
                    region_dict[particle_key] = {'params': [], 'data': [], 'ndata':[]}

            region_dict[particle_key]['params'].append(single_params)
            region_dict[particle_key]['data'].append(data[i].tolist())
            if particle_key == 'Neutron':
                region_dict[particle_key]['ndata'].append(ndata)#.tolist()) #
        i+=1

    return region_dict


def main(dir):
    """
    The program analyzes .txt file with USTRACK data from FLUKA and returns dictionary
    containing all necessary data for generating plots INCLUDING data, in oppose to
    dict_spectra.py.

    I recommend paying attention to changes in the FLUKA version and possible changes
    in the output. The program is based on pattern recognition, so it may require
    adjustments in this regard. Especially in the context of neutrons, the program is
    not adapted to read low-energy neutrons from simulations performed on older versions
    of FLUKA (older than 4.4.0). There is handling of ndata (low energy neutrons), but
    seems to causing problems sometimes.
    """

    file_path = dir
    file_name = os.path.basename(file_path)

    region_dict = prepareRegionDict(file_path, file_name)
    return(region_dict)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Lethargy Fluence Spectra help")
    parser.add_argument("directory", help="relative directory to original file (.txt)")

    args = parser.parse_args()
    res = main(args.directory)
    print(res['Charged-hadrons']['params'])
