#!/bin/env python

import re
import os
import sys
import math
import numpy as np
import argparse
import json
import matplotlib.pyplot as plt
import plotly.express as px
from matplotlib.colors import Normalize

from .scoring_codes import ScoringCodes
from .size_config import SIZE_CONFIG
import plotly as pl
from PIL import Image

default_min, default_max = 0.4, 2.0

DATA_DIR = os.getenv("PLOTS_DIR", "")

scoringCodes = ScoringCodes()

exp_num_rx = r"[-+]?\d*\.\d+[eE][-+]?\d+"
rx = re.compile(exp_num_rx)

bname_rx = re.compile(r"Cartesian binning n. ([ 0-9]+)(\s+)\"(\w+)([\ \"\,.a-z]+)(\d+)$")
coord_rx = re.compile(r"(\w+) coordinate\: from ([ 0-9eE.+\-]+) to ([ 0-9eE.+\-]+) cm, ([ 0-9]+)")


## -----------------Loading info------------------ ##

def extractNumbers(file):
    all_numbers = []

    for line in file:
        numbers = rx.findall(line)
        if len(rx.sub("", line).strip()) != 0 :
            continue #
        numbers = map(float, numbers)
        all_numbers += numbers

    return all_numbers


def extractParameters(file):
    parameters = dict()
    for line in file:
        found = coord_rx.search(line)
        if found:
            found = found.groups()
            parameters[found[0]] = { 'from' : float(found[1]), 'to' : float(found[2]), 'count' : int(found[3]) }
        title = bname_rx.search(line)
        if title:
            parameters['type'] = { 'number' : int(title.group(1)), 'word' : title.group(3), 'part_nr' : int(title.group(5)) }
        if set(parameters.keys())==set(['X','Y','Z','type']):
            break
    return parameters


def loadFort(filename):
    ###filename = os.path.join(DATA_DIR,filename)
    file = open(filename, 'r')
    parameters = extractParameters(file)
    file.seek(0)
    numbers    = extractNumbers(file)
    file.close()
    return dict(header=parameters, data=numbers)


def deterUnits(part_nr,data):
    unit  = scoringCodes.get_unit_from_id(part_nr)
    title = scoringCodes.get_info_from_id(part_nr)
    conv_factor = 1.0

    #not really work
    if unit.startswith('GeV/g'):
           conv_factor = scoringCodes.get_conv_factor("GeV/g", "Gy")
           unit  = unit.replace("GeV/g", "Gy")
           title = title.replace("GeV/g", "Gy")
    if unit.startswith('pSv'):
           conv_factor = scoringCodes.get_conv_factor("pSv", "mSv")
           unit  = unit.replace("pSv", "mSv")
           title = title.replace("pSv", "mSv")

    data = data*conv_factor
    return data



def getPlotHtml(fig):
    return pl.io.to_html(fig, include_plotlyjs='cdn')

def makeDataArray(fort):
    h_name = fort['header']['type']['word']

    (a_name, a, a_lo, a_hi) = ('X', fort['header']['X']['count'], fort['header']['X']['from'], fort['header']['X']['to'])
    (b_name, b, b_lo, b_hi) = ('Y', fort['header']['Y']['count'], fort['header']['Y']['from'], fort['header']['Y']['to'])
    (c_name, c, c_lo, c_hi) = ('Z', fort['header']['Z']['count'], fort['header']['Z']['from'], fort['header']['Z']['to'])
    #print('Processing...:')
    #print("%s, a*b*c(%d), data len (%d)" % (h_name, (a*b*c), len(fort['data'])) )
    if a*b*c != len(fort['data']):
        print("Some problem with data", a*b*c, " , ", len(fort['data']) )
        return None
    data = fort['data']
    data = np.array(data).reshape(c, b, a)
    data = np.transpose(data, (2, 1, 0))
    data = deterUnits(fort['header']['type']['part_nr'],data)
    return(data)


def extendTitle(fort, title, key):
    binx = (fort['header']['X']['to']-fort['header']['X']['from'])/fort['header']['X']['count']
    biny = (fort['header']['Y']['to']-fort['header']['Y']['from'])/fort['header']['Y']['count']
    binz = (fort['header']['Z']['to']-fort['header']['Z']['from'])/fort['header']['Z']['count']
    unit = scoringCodes.get_unit_from_id(fort['header']['type']['part_nr'])
    if unit == "GeV/g":
        unit ="Gy"
    if unit == "pSv":
        unit = "mSv"
    binning = "(%dx%dx%d cm each bin) " % (binx,biny,binz)
    title = "%s [%s], %s" % (title, unit, binning)
    return title


def retBins(fort, proj):
    if proj=='x':
        proj_key = 'X'
    if proj=='y':
        proj_key = 'Y'
    if proj=='z':
        proj_key = 'Z'
    (lo, hi, count) = (fort['header'][proj_key]['from'],fort['header'][proj_key]['to'],fort['header'][proj_key]['count'])
    bins = np.linspace(lo, hi, count + 1)
    return(bins.tolist())


def makeInfoDict(fort): #(data,fort)
    particle_number = fort['header']['type']['part_nr']
    key   = fort['header']['type']['word']
    unit  = scoringCodes.get_unit_from_id(particle_number)
    title = scoringCodes.get_info_from_id(particle_number)

    (a_name, a, a_lo, a_hi) = ('X', fort['header']['X']['count'], fort['header']['X']['from'], fort['header']['X']['to'])
    (b_name, b, b_lo, b_hi) = ('Y', fort['header']['Y']['count'], fort['header']['Y']['from'], fort['header']['Y']['to'])
    (c_name, c, c_lo, c_hi) = ('Z', fort['header']['Z']['count'], fort['header']['Z']['from'], fort['header']['Z']['to'])
    title = extendTitle(fort, title, key)
    bins_x = retBins(fort,'x')
    bins_y = retBins(fort,'y')
    bins_z = retBins(fort,'z')
    info_dict = {
        "keyword": key,
        "unit": unit,
        "part_nr": particle_number,
        "title": title,
        'a_name': a_name,
        'a': a,
        'a_lo': a_lo,
        'a_hi': a_hi,
        'b_name': b_name,
        'b': b,
        'b_lo': b_lo,
        'b_hi': b_hi,
        'c_name': c_name,
        'c': c,
        'c_lo': c_lo,
        'c_hi': c_hi,
        'bins_x' : bins_x,
        'bins_y' : bins_y,
        'bins_z' : bins_z,
        }

    return(info_dict)

## -----------------Making plot------------------ ##

def checkInterval(lo, hi, count, number):
    interval_size = (hi - lo) / count
    interval_start = lo
    for i in range(count):
        interval_end = interval_start + interval_size
        if interval_start <= number < interval_end:
            return f': [{round(interval_start, 1)}, {round(interval_end, 1)}]'
        interval_start = interval_end
    return "error setting hover"

def chooseTickvals(x_lo, x_hi, y_lo, y_hi):

    def choose_step(span):
        possible_steps = [50, 200, 100, 250]
        while True:
            fitting_steps = [step for step in possible_steps if 5 <= span / step <= 10]
            if fitting_steps:
                return fitting_steps[0]
            elif all(span / step <= 5 for step in possible_steps):
                possible_steps = [step / 10 for step in possible_steps]
            elif all(span / step >= 10 for step in possible_steps):
                possible_steps = [step * 10 for step in possible_steps]

    span_x = x_hi - x_lo
    span_y = y_hi - y_lo
    step_x = choose_step(span_x)
    step_y = choose_step(span_y)

    x_val = x_lo + (step_x - x_lo % step_x)
    x_vals = []
    while x_val <= x_hi:
        x_vals.append(x_val)
        x_val += step_x
    y_val = y_lo + (step_y - y_lo % step_y)
    y_vals = []
    while y_val <= y_hi:
        y_vals.append(y_val)
        y_val += step_y

    return x_vals, y_vals


def ImshowCustom(data,x_lo,x_hi,x_count, x_name, y_lo,y_hi,y_count, y_name,colorbar_min, colorbar_max, **kwargs):

    colors = [
    '#7F00FF', '#5500ff', '#330099','#001f3f', '#004080', '#0070bf',
    '#0080ff', '#0099FF', '#00CCFF', '#009900','#00CC33','#66CC33','#93F600',
    '#ffff00', '#ff8000',  '#FF0000', '#CC0000','#990033'
    ]

    x = np.linspace(x_lo, x_hi, data.shape[1])
    y = np.linspace(y_lo, y_hi, data.shape[0])

    color_scale = []
    for i in range(len(colors)):
        color = colors[i]
        val1 = i / len(colors)
        val2 = (i + 1) / len(colors)
        color_scale.append((val1, color))
        color_scale.append((val2, color))

    data_plot_only = np.where((data > colorbar_max) | (data < colorbar_min), None, data)

    fig = px.imshow(
		img = data_plot_only,
        color_continuous_scale = color_scale,
        x=x,
        y=y,
		**kwargs,
	)

    x_array = fig['data'][0].x
    y_array = fig['data'][0].y

    x_bounds = []
    for val in x_array:
        x_bounds.append(checkInterval(x_lo,x_hi,x_count,val))
    x_bounds = [x_bounds.copy() for _ in range(y_count)]

    y_bounds = []
    for val in y_array:
        y_bounds.append(checkInterval(y_lo, y_hi, y_count, val))

    y_bounds = [y_bounds.copy() for _ in range(x_count)]
    y_bounds = np.array(y_bounds)
    y_bounds = y_bounds.transpose()

    x_vals, y_vals = chooseTickvals(x_lo, x_hi, y_lo, y_hi)
    fig.update_xaxes(tickvals=x_vals)
    fig.update_yaxes(tickvals=y_vals)
    fig.update_xaxes(showgrid=False)
    fig.update_yaxes(showgrid=False)

    for x_line in x_vals:
        fig.add_shape(
            type="line",
            x0=x_line,
            y0=y_lo,
            x1=x_line,
            y1=y_hi,
            line=dict(
                color="white",
                width=0.5,
            )
        )

    for y_line in y_vals:
        fig.add_shape(
            type="line",
            x0=x_lo,
            y0=y_line,
            x1=x_hi,
            y1=y_line,
            line=dict(
                color="white",
                width=0.5,
            )
        )

    fig.update(data=[{'customdata': np.dstack((x_bounds, y_bounds, data)),
        'hovertemplate': x_name+" %{customdata[0]} cm<br>"+y_name+" %{customdata[1]} cm<br>Value: %{customdata[2]:.5f}<extra></extra>"}])

    return fig


def checkDepth(lo, hi, count, depth):
    if depth<lo or depth>hi:
        print("Selected depth on the axis exceeds the available range!")
        print("No plot available")
        sys.exit(0)
    bins = np.linspace(lo, hi, count + 1)
    if depth in bins:
        bin_no = np.where(bins == depth)[0][0]
    else:
        print(f"Depth {depth} unavailable. Choose one of:")
        print(bins)
        sys.exit(0)
    return(bin_no)


def roundMin(x):
    if x<=0: return x
    else:    return pow(10,math.floor(math.log10(x)))

def roundMax(x):
    if x<=0: return x
    else:    return pow(10,math.ceil(math.log10(x)))


def subtrackRatioData(data1, data2):
    #data1 = np.array(loaded_dataf["data_to_plot"])
    #data2 = np.array(loaded_datas["data_to_plot"])
    if data1.shape == data2.shape:
        rows, cols, depth = data1.shape
        data = np.zeros((rows, cols, depth), dtype=np.float64)

        for i in range(rows):
            for j in range(cols):
                for k in range(depth):
                    if data2[i, j, k] != 0:
                        data[i, j, k] = data1[i, j, k] / data2[i, j, k]
                    else:
                        data[i, j, k] = 1.000000001
    data[np.isclose(data,1.0)]=1.000000001
    return data


def plotRatio(loaded_data, proj, depth, data, colorbar_min, colorbar_max, size):
    title = loaded_data["title"]
    a_name = loaded_data["a_name"]
    a = loaded_data["a"]
    a_lo = loaded_data["a_lo"]
    a_hi = loaded_data["a_hi"]
    b_name = loaded_data["b_name"]
    b = loaded_data["b"]
    b_lo = loaded_data["b_lo"]
    b_hi = loaded_data["b_hi"]
    c_name = loaded_data["c_name"]
    c = loaded_data["c"]
    c_lo = loaded_data["c_lo"]
    c_hi = loaded_data["c_hi"]

    if proj == 'x':
        x_lo, x_hi = c_lo, c_hi
        y_lo, y_hi = b_lo, b_hi
        x, y = c, b
        x_name, y_name = c_name, b_name
        bin_no = checkDepth(a_lo,a_hi,a,depth)
        summed_data = data[bin_no, :, :]
    elif proj == 'y':
        x_lo, x_hi = c_lo, c_hi
        y_lo, y_hi = a_lo, a_hi
        x, y = c, a
        x_name, y_name = c_name, a_name
        bin_no = checkDepth(b_lo,b_hi,b,depth)
        summed_data = data[:, bin_no, :]
    elif proj == 'z':
        x_lo, x_hi = b_lo, b_hi
        y_lo, y_hi = a_lo, a_hi
        x, y = b, a
        x_name, y_name = b_name, a_name
        bin_no = checkDepth(c_lo,c_hi,c,depth)
        summed_data = data[:, :, bin_no]

    pattern = r'\[(.*?)\]'
    matches = re.findall(pattern, title)

    #unit = matches[0]

    fig = ImshowCustom(summed_data,x_lo, x_hi, x, x_name, y_lo, y_hi, y, y_name, colorbar_min, colorbar_max,)

    num_of_dec = 3 # could be changed

    target_ticks = 18
    TICKS_VALS = np.linspace(colorbar_min, colorbar_max, target_ticks+1)
    #print(TICKS_VALS)
    TICKS_VALS = np.around(TICKS_VALS, decimals=num_of_dec)
    ticks_text = [str("{:g}".format(_)) for _ in TICKS_VALS]

    fig.update_xaxes(range=[x_lo, x_hi])
    fig.update_yaxes(range=[y_lo, y_hi], autorange=False)
    fig.update_layout(title_text = ('RATIO ' + title +  ' <br>'+''+proj+ ' = ' +str(depth)+ ' cm'),
                      xaxis_title = x_name + ' [cm]',
                      yaxis_title = y_name + ' [cm]',
                      coloraxis_colorbar = dict(title =  '  RATIO ', title_side='right'),
    )

    fig.update_layout(
        coloraxis_colorbar = dict(
            tickvals = TICKS_VALS,
            #ticktext = ticks_text,
        ),
        coloraxis = dict(
            cmin = colorbar_min,
            cmax = colorbar_max
        ),
    )


    width = SIZE_CONFIG.get(size)

    fig.update_layout(
        width=width
    )
    return(fig)


def showPlot(fig):
    fig.show()


def main(directory1, directory2, proj, depth, colorbar_min, colorbar_max, size):
    """
    Program takes two files with the same name from different simulations and creates a ratio plot
    of the first data set to the second data set. By default, it displays the range from 0.4 to 2.0 on the scale
    - this can be changed using optional arguments.

    Doing ratio makes sense only for files with same number of bins and boundries (and quantity), so I suggest
    give a choice of only files with exact same name.

    The program only handles original .txt files.
    In the future, it may be necessary to add an option for a custom title.
    """
    file_path1 = directory1
    directory1= os.path.join(DATA_DIR,file_path1)
    for root, _, files in os.walk(directory1):
        for file_name1 in files:
            if "abs" not in file_name1:
                if "rel" not in file_name1:
                    file_path1 = directory1 + "/" + file_name1

    file_path2 = directory2
    directory2= os.path.join(DATA_DIR,file_path2)
    for root, _, files in os.walk(directory2):
        for file_name2 in files:
            if "abs" not in file_name2:
                if "rel" not in file_name2:
                    file_path2 = directory2 + "/" + file_name2

    fort1 = loadFort(file_path1)
    data1 = makeDataArray(fort1)
    info1 = makeInfoDict(fort1)

    fort2 = loadFort(file_path2)
    data2 = makeDataArray(fort2)
    #info2 = makeInfoDict(fort2)

    ratio_data = subtrackRatioData(data1, data2)

    fig = plotRatio(info1, proj, depth, ratio_data, colorbar_min, colorbar_max, size)

    #showPlot(fig)  # display plot
    return getPlotHtml(fig) # plot to html


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="ratio")
    parser.add_argument("--file1", help = 'relative path to folder with original file1')
    parser.add_argument("--file2", help = 'relative path to folder with original file2')
    parser.add_argument("--proj", help="projection (x, y or z)")
    parser.add_argument("--depth", help="depth in cm")
    parser.add_argument("--min", default= 0.4,  help = "minimum of ratio")
    parser.add_argument("--max", default= 2, help = "maximum of ratio")
    parser.add_argument(
        '--size',
        nargs='?',
        default='medium',
        choices=['small', 'medium', 'large'],
        help="Size of the plot (for webpage purposes). Default = medium."
    )

    args = parser.parse_args()
    main(args.file1, args.file2, args.proj, float(args.depth), float(args.min), float(args.max), args.size)
