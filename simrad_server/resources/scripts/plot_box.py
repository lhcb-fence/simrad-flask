import re
import os
import sys
import math
import numpy as np
import argparse
import json
import plotly.express as px
from .scoring_codes import ScoringCodes
from .size_config import SIZE_CONFIG
import time
import plotly.express as px
import plotly.io as pio
import plotly as pl
from PIL import Image


DATA_DIR = os.getenv("PLOTS_DIR", "")

scoringCodes = ScoringCodes()

exp_num_rx = r"[-+]?\d*\.\d+[eE][-+]?\d+"
rx = re.compile(exp_num_rx)

bname_rx = re.compile(r"Cartesian binning n. ([ 0-9]+)(\s+)\"(\w+)([\ \"\,.a-z]+)(\d+)$")
coord_rx = re.compile(r"(\w+) coordinate\: from ([ 0-9eE.+\-]+) to ([ 0-9eE.+\-]+) cm, ([ 0-9]+)")


## -----------------Loading info------------------ ##

def extractNumbers(file):
    all_numbers = []

    for line in file:
        numbers = rx.findall(line)
        if len(rx.sub("", line).strip()) != 0 :
            continue #
        numbers = map(float, numbers)
        all_numbers += numbers

    return all_numbers


def extractParameters(file):
    parameters = dict()
    for line in file:
        found = coord_rx.search(line)
        if found:
            found = found.groups()
            parameters[found[0]] = { 'from' : float(found[1]), 'to' : float(found[2]), 'count' : int(found[3]) }
        title = bname_rx.search(line)
        if title:
            parameters['type'] = { 'number' : int(title.group(1)), 'word' : title.group(3), 'part_nr' : int(title.group(5)) }
        if set(parameters.keys())==set(['X','Y','Z','type']):
            break
    return parameters


def loadFort(filename):
    file = open(filename, 'r')
    parameters = extractParameters(file)
    file.seek(0)
    numbers    = extractNumbers(file)
    file.close()
    return dict(header=parameters, data=numbers)


def deterUnits(part_nr,data):
    #particle_number = fort['header']['type']['unit']
    #print(part_nr)
    unit  = scoringCodes.get_unit_from_id(part_nr)
    title = scoringCodes.get_info_from_id(part_nr)
    conv_factor = 1.0

    if unit.startswith('GeV/g'):
           conv_factor = scoringCodes.get_conv_factor("GeV/g", "Gy")
           unit  = unit.replace("GeV/g", "Gy")
           title = title.replace("GeV/g", "Gy")
    if unit.startswith('pSv'):
           conv_factor = scoringCodes.get_conv_factor("pSv", "mSv")
           unit  = unit.replace("pSv", "mSv")
           title = title.replace("pSv", "mSv")

    data = data*conv_factor
    return data


def makeDataArray(fort):
    h_name = fort['header']['type']['word']

    (a_name, a, a_lo, a_hi) = ('X', fort['header']['X']['count'], fort['header']['X']['from'], fort['header']['X']['to'])
    (b_name, b, b_lo, b_hi) = ('Y', fort['header']['Y']['count'], fort['header']['Y']['from'], fort['header']['Y']['to'])
    (c_name, c, c_lo, c_hi) = ('Z', fort['header']['Z']['count'], fort['header']['Z']['from'], fort['header']['Z']['to'])
    #print('Processing...:')
    #print("%s, a*b*c(%d), data len (%d)" % (h_name, (a*b*c), len(fort['data'])) )
    if a*b*c != len(fort['data']):
        print("Some problem with data", a*b*c, " , ", len(fort['data']) )
        return None
    data = fort['data']
    data = np.array(data).reshape(c, b, a)
    data = np.transpose(data, (2, 1, 0))
    data = deterUnits(fort['header']['type']['part_nr'],data)
    return(data)


def extendTitle(fort, title, key):
    binx = (fort['header']['X']['to']-fort['header']['X']['from'])/fort['header']['X']['count']
    biny = (fort['header']['Y']['to']-fort['header']['Y']['from'])/fort['header']['Y']['count']
    binz = (fort['header']['Z']['to']-fort['header']['Z']['from'])/fort['header']['Z']['count']
    unit = scoringCodes.get_unit_from_id(fort['header']['type']['part_nr'])
    if unit == "GeV/g":
        unit ="Gy"
    if unit == "pSv":
        unit = "mSv"
    binning = "(%dx%dx%d cm each bin) " % (binx,biny,binz)
    title = "%s [%s], %s" % (title, unit, binning)
    return title


def retBins(fort, proj):
    if proj=='x':
        proj_key = 'X'
    if proj=='y':
        proj_key = 'Y'
    if proj=='z':
        proj_key = 'Z'
    (lo, hi, count) = (fort['header'][proj_key]['from'],fort['header'][proj_key]['to'],fort['header'][proj_key]['count'])
    bins = np.linspace(lo, hi, count + 1)
    return(bins.tolist())


def makeInfoDict(fort): #(data,fort)
    particle_number = fort['header']['type']['part_nr']
    key   = fort['header']['type']['word']
    unit  = scoringCodes.get_unit_from_id(particle_number)
    title = scoringCodes.get_info_from_id(particle_number)

    (a_name, a, a_lo, a_hi) = ('X', fort['header']['X']['count'], fort['header']['X']['from'], fort['header']['X']['to'])
    (b_name, b, b_lo, b_hi) = ('Y', fort['header']['Y']['count'], fort['header']['Y']['from'], fort['header']['Y']['to'])
    (c_name, c, c_lo, c_hi) = ('Z', fort['header']['Z']['count'], fort['header']['Z']['from'], fort['header']['Z']['to'])
    title = extendTitle(fort, title, key)
    bins_x = retBins(fort,'x')
    bins_y = retBins(fort,'y')
    bins_z = retBins(fort,'z')
    info_dict = {
        "keyword": key,
        "unit": unit,
        "part_nr": particle_number,
        "title": title,
        'a_name': a_name,
        'a': a,
        'a_lo': a_lo,
        'a_hi': a_hi,
        'b_name': b_name,
        'b': b,
        'b_lo': b_lo,
        'b_hi': b_hi,
        'c_name': c_name,
        'c': c,
        'c_lo': c_lo,
        'c_hi': c_hi,
        'bins_x' : bins_x,
        'bins_y' : bins_y,
        'bins_z' : bins_z,
        }

    return(info_dict)

## -----------------Making plot------------------ ##

def roundMin(x):
    if x<=0: return x
    else:    return pow(10,math.floor(math.log10(x)))


def roundMax(x):
    if x<=0: return x
    else:    return pow(10,math.ceil(math.log10(x)))


def checkInterval(lo, hi, count, number):
    interval_size = (hi - lo) / count
    interval_start = lo
    for i in range(count):
        interval_end = interval_start + interval_size
        if interval_start <= number < interval_end:
            return f': [{round(interval_start, 1)}, {round(interval_end, 1)}]'
        interval_start = interval_end
    return "error setting hover"

def chooseTickvals(x_lo, x_hi, y_lo, y_hi):

    def choose_step(span):
        possible_steps = [50, 200, 100, 250]
        while True:
            fitting_steps = [step for step in possible_steps if 5 <= span / step <= 10]
            if fitting_steps:
                return fitting_steps[0]
            elif all(span / step <= 5 for step in possible_steps):
                possible_steps = [step / 10 for step in possible_steps]
            elif all(span / step >= 10 for step in possible_steps):
                possible_steps = [step * 10 for step in possible_steps]

    span_x = x_hi - x_lo
    span_y = y_hi - y_lo
    step_x = choose_step(span_x)
    step_y = choose_step(span_y)

    x_val = x_lo + (step_x - x_lo % step_x)
    x_vals = []
    while x_val <= x_hi:
        x_vals.append(x_val)
        x_val += step_x
    y_val = y_lo + (step_y - y_lo % step_y)
    y_vals = []
    while y_val <= y_hi:
        y_vals.append(y_val)
        y_val += step_y

    return x_vals, y_vals

def ImshowLogscale(data,x_lo,x_hi,x_count, x_name, y_lo,y_hi,y_count, y_name, minor_ticks='auto', **kwargs):

    if minor_ticks not in {True,False,'auto'}:
        raise ValueError(f'`minor_ticks` must be True, False or "auto", received {repr(minor_ticks)}. ')
    with np.errstate(divide='ignore'):
        log_data = np.log10(data)

    x = np.linspace(x_lo, x_hi, log_data.shape[1])
    y = np.linspace(y_lo, y_hi, log_data.shape[0])
    colors = [
    '#7F00FF', '#5500ff', '#330099','#001f3f', '#004080', '#0070bf',
    '#0080ff', '#0099FF', '#00CCFF', '#009900','#00CC33','#66CC33','#93F600',
    '#ffff00', '#ff8000',  '#FF0000', '#CC0000','#990033'
    ]

    color_scale = []
    for i in range(len(colors)):
        color = colors[i]
        val1 = i / len(colors)
        val2 = (i + 1) / len(colors)
        color_scale.append((val1, color))
        color_scale.append((val2, color))

    fig = px.imshow(
		img = log_data,
        color_continuous_scale = color_scale,
        x=x,
        y=y,
		**kwargs,
	)
    TICKS_VALS = [list(np.linspace(10**e,10**(e+1),10)[:-1]) for e in range(-28,12)] #rrr
    if minor_ticks == 'auto':
        if np.nanmax(log_data) - np.nanmin(log_data) > 3:
            minor_ticks = False
    if minor_ticks == False:
        TICKS_VALS = [[_[0]] for _ in TICKS_VALS]
    TICKS_VALS = [_ for l in TICKS_VALS for _ in l]
    ticks_text = [str("{:g}".format(_)) for _ in TICKS_VALS]

    fig.update_layout(
        coloraxis_colorbar = dict(
            tickvals = [np.log10(_) for _ in TICKS_VALS],
            ticktext = ticks_text,
        ),
    )

    x_array = fig['data'][0].x
    y_array = fig['data'][0].y

    x_bounds = []
    for val in x_array:
        x_bounds.append(checkInterval(x_lo,x_hi,x_count,val))
    x_bounds = [x_bounds.copy() for _ in range(y_count)]

    y_bounds = []
    for val in y_array:
        y_bounds.append(checkInterval(y_lo, y_hi, y_count, val))

    y_bounds = [y_bounds.copy() for _ in range(x_count)]
    y_bounds = np.array(y_bounds)
    y_bounds = y_bounds.transpose()


    x_vals, y_vals = chooseTickvals(x_lo, x_hi, y_lo, y_hi)
    fig.update_xaxes(tickvals=x_vals)
    fig.update_yaxes(tickvals=y_vals)
    fig.update_xaxes(showgrid=False)
    fig.update_yaxes(showgrid=False)

    for x_line in x_vals:
        fig.add_shape(
            type="line",
            x0=x_line,
            y0=y_lo,
            x1=x_line,
            y1=y_hi,
            line=dict(
                color="white",
                width=0.5,
            )
        )

    for y_line in y_vals:
        fig.add_shape(
            type="line",
            x0=x_lo,
            y0=y_line,
            x1=x_hi,
            y1=y_line,
            line=dict(
                color="white",
                width=0.5,
            )
        )

    fig.update(data=[{'customdata': np.dstack((x_bounds, y_bounds, data)),
        'hovertemplate': x_name+" %{customdata[0]} cm<br>"+y_name+" %{customdata[1]} cm<br>Value: %{customdata[2]:.4e}<extra></extra>"}])

    return fig


def checkDepth(lo, hi, count, depth):
    if depth<lo or depth>hi:
        print("Selected depth on the axis exceeds the available range!")
        print("No plot available")
        sys.exit(0)
    bins = np.linspace(lo, hi, count + 1)
    if depth in bins:
        bin_no = np.where(bins == depth)[0][0]
    else:
        print(f"Depth {depth} unavailable. Choose one of:")
        print(bins)
        sys.exit(0)
    return(bin_no)



def makePlot(loaded_info, loaded_data, proj, depth, is_error, number_of_coli,size):
    data = loaded_data
    data *= int(number_of_coli)

    title = loaded_info["title"]
    a_name = loaded_info["a_name"]
    a = loaded_info["a"]
    a_lo = loaded_info["a_lo"]
    a_hi = loaded_info["a_hi"]
    b_name = loaded_info["b_name"]
    b = loaded_info["b"]
    b_lo = loaded_info["b_lo"]
    b_hi = loaded_info["b_hi"]
    c_name = loaded_info["c_name"]
    c = loaded_info["c"]
    c_lo = loaded_info["c_lo"]
    c_hi = loaded_info["c_hi"]

    if proj == 'x':
        x_lo, x_hi = c_lo, c_hi
        y_lo, y_hi = b_lo, b_hi
        x, y = c, b
        x_name, y_name = c_name, b_name
        bin_no = checkDepth(a_lo,a_hi,a,depth)
        summed_data = data[bin_no, :, :]
    elif proj == 'y':
        x_lo, x_hi = c_lo, c_hi
        y_lo, y_hi = a_lo, a_hi
        x, y = c, a
        x_name, y_name = c_name, a_name
        bin_no = checkDepth(b_lo,b_hi,b,depth)
        summed_data = data[:, bin_no, :]
    elif proj == 'z':
        x_lo, x_hi = b_lo, b_hi
        y_lo, y_hi = a_lo, a_hi
        x, y = b, a
        x_name, y_name = b_name, a_name
        bin_no = checkDepth(c_lo,c_hi,c,depth)
        summed_data = data[:, :, bin_no]


    unit = loaded_info["unit"]
    if unit == "GeV/g":
        unit ="Gy"
    if unit == "pSv":
        unit = "mSv"


    fig = ImshowLogscale(summed_data,x_lo, x_hi, x, x_name, y_lo, y_hi, y, y_name, minor_ticks=False)
    fig.update_xaxes(range=[x_lo, x_hi])
    fig.update_yaxes(range=[y_lo, y_hi], autorange=False)
    if is_error == 'no_error':
        fig.update_layout(title_text = title +'<br>'+ proj+ ' = ' +str(depth)+ ' cm, ' + 'number of collisions = ' + str(number_of_coli),
                        xaxis_title = x_name + ' [cm]',
                        yaxis_title = y_name + ' [cm]',
                        coloraxis_colorbar = dict(title = unit, title_side='right')
        )
    else:
        fig.update_layout(title_text = title + ', '+proj+ ' = ' +str(depth)+ ' cm,' + ' <br>'+' number of collisions = ' + str(number_of_coli) + ', ERROR ' + is_error,
                        xaxis_title = x_name + ' [cm]',
                        yaxis_title = y_name + ' [cm]',
                        coloraxis_colorbar = dict(title = unit, title_side='right')
        )

    with np.errstate(divide='ignore'):
        log_data = np.log10(data)

    width = SIZE_CONFIG.get(size)
    fig.update_layout(
        width=width,
        coloraxis = dict(
            cmax= roundMax(np.max(log_data)),
            cmin= roundMax(np.max(log_data))-9,
        ),
    )
    return(fig)


def showPlot(fig):
    fig.show()


def getPlotHtml(fig):
    return pl.io.to_html(fig, include_plotlyjs='cdn')


## -----------------Calculating info for given boundaries------------------ ##

def getBinNumbersMaxAndMin(ax_min, ax_max, bins):

    min = bins.index(ax_min)
    max = bins.index(ax_max)
    return(min,max)


def obtainSelectedBox(info,data,number_of_coli, boundaries_xyz):

    data *= int(number_of_coli)
    xmin,xmax = getBinNumbersMaxAndMin(boundaries_xyz[0],boundaries_xyz[1],info["bins_x"])
    ymin,ymax = getBinNumbersMaxAndMin(boundaries_xyz[2],boundaries_xyz[3],info["bins_y"])
    zmin,zmax = getBinNumbersMaxAndMin(boundaries_xyz[4],boundaries_xyz[5],info["bins_z"])

    box = data[xmin:xmax,ymin:ymax,zmin:zmax]
    return(box)


def obtainBoxBoundries(bounds, proj):

    if proj == 'x':
        x0 = bounds[4]
        x1 = bounds[5]
        y0 = bounds[2]
        y1 = bounds[3]

    elif proj == 'y':
        x0 = bounds[4]
        x1 = bounds[5]
        y0 = bounds[0]
        y1 = bounds[1]
    elif proj == 'z':
        x0 = bounds[2]
        x1 = bounds[3]
        y0 = bounds[0]
        y1 = bounds[1]

    return x0,x1,y0,y1


def isDepthInBox(bounds, proj, depth):
    isIn = True
    if proj == 'x':
        if depth < bounds[0] or depth > bounds[1]:
            isIn = False
    elif proj == 'y':
        if depth < bounds[2] or depth > bounds[3]:
            isIn = False
    elif proj == 'z':
        if depth < bounds[4] or depth > bounds[5]:
            isIn = False

    return isIn

def addSelectedBox(info, data, proj, depth, number_of_coli, boundaries_xyz,fig):

    box= obtainSelectedBox(info, data, number_of_coli,boundaries_xyz)
    mean = np.mean(box)
    min_value = np.min(box)
    max_value = np.max(box)
    sum = np.sum(box)

    xmin,xmax,ymin,ymax = obtainBoxBoundries(boundaries_xyz,proj)

    if isDepthInBox(boundaries_xyz,proj,depth):
        fig.add_shape(type="rect",
            x0=xmin, y0=ymin, x1=xmax, y1=ymax,
                line=dict(
                    color="white",
                    width=3,
                ),
        )
    return fig,mean,min_value,max_value,sum


## -----------------MAIN------------------ ##


def main(directory,proj,depth,is_error,number_of_coli,x0,x1,y0,y1,z0,z1,size):

    file_path = directory
    directory= os.path.join(DATA_DIR,file_path)
    for root, _, files in os.walk(directory):
        for file_name in files:
            if "abs" not in file_name:
                if "rel" not in file_name:
                    file_path = directory + "/" + file_name

    file_path_no_ext = os.path.splitext(file_path)[0]
    if is_error != 'no_error':
        if is_error == 'rel':
            file_path = file_path_no_ext + '.rel.ERR'
        else:
            file_path = file_path_no_ext + '.abs.ERR'


    fort = loadFort(file_path)
    data = makeDataArray(fort)
    info = makeInfoDict(fort)

    fig= makePlot(info, data, proj, depth, is_error, number_of_coli,size)
    boundaries_xyz =[x0,x1,y0,y1,z0,z1]
    fig,mean,min_value,max_value,sum = addSelectedBox(info, data, proj, depth, number_of_coli, boundaries_xyz,fig)
    print(mean,min_value,max_value,sum)
    showPlot(fig)  # display plot
    plot = getPlotHtml(fig)
    return (plot,mean,min_value,max_value)



if __name__ == "__main__":
    """
    The program corresponds to the "plot.py" program, according to the description:

        "The program accepts a original file path, and the projection axis (x, y, or z),
        as well as the projection depth (must match the bins, available projections are listed in the dictionary
        under keys 'bins_x', 'bins_y', 'bins_z'). By default, it displays the main plot - the error plot can be viewed
        using an optional argument. Default scaling is set to one collision. The plot can be rescaled using an optional
        argument.

        .ERR files must be stored in exact same location as main file."

    But it accepts an additional 6 arguments: x0, x1, y0, y1, z0, z1, which correspond to the boundaries
    of the box over which the averaging will take place. The arguments must match the bin values, meaning
    they should use the metadata for selection. The program draws a box on the plot and returns to standard
    output the following in order: the HTML plot, the mean, the minimum value in the area, the maximum
    value in the area, and the sum in the area.
    """

    parser = argparse.ArgumentParser(description=" ")
    parser.add_argument("--file", help="relative path to the folder with original file")
    parser.add_argument("--proj", help="projection (x, y or z)")
    parser.add_argument("--depth", help="depth in cm")
    parser.add_argument(
        '--is_error',
        nargs='?',
        default='no_error',
        choices=['rel', 'no_error', 'abs'],
        help="Error plot option. Available 'rel' for relative error, 'no_error' for normal plotting, and 'abs' for absolute error."
    )
    parser.add_argument(
        '--coli',
        nargs='?',
        default=1,
        help="Number of collisions. Default = 1."
    )
    parser.add_argument(
        '--x0',
        nargs='?',
    )
    parser.add_argument(
        '--x1',
        nargs='?',
    )
    parser.add_argument(
        '--y0',
        nargs='?',
    )
    parser.add_argument(
        '--y1',
        nargs='?',
    )
    parser.add_argument(
        '--z0',
        nargs='?',
    )
    parser.add_argument(
        '--z1',
        nargs='?',
    )
    parser.add_argument(
        '--size',
        nargs='?',
        default='medium',
        choices=['small', 'medium', 'large'],
        help="Size of the plot (for webpage purposes). Default = medium."
    )

    args = parser.parse_args()
    main(args.file, args.proj, float(args.depth), args.is_error, args.coli, float(args.x0), float(args.x1), float(args.y0), float(args.y1), float(args.z0), float(args.z1), args.size)
