#!/bin/env python

import os
import json
import sys
import argparse
import numpy as np
import pandas as pd
import plotly.graph_objects as go
from . import spectra_help
#import spectra_dict
import plotly.express as px
from .size_config import SIZE_CONFIG
import plotly.io as pio
import plotly as pl
from PIL import Image
from .low_energy_neutrons_binning import neut_binning


DATA_DIR = os.getenv("PLOTS_DIR", "")

global program_version
global no_of_part


def custom_tick_format(value, _):
    power = int(np.log10(value))
    mantissa = value / 10**power
    if mantissa == 1:
        return f'$10^{{{power}}}$'
    else:
        return f'$10^{{{power}}}$ {mantissa:.0f}'


def PreperaLogarythmicGrid(fig,given_x_min,given_x_max,given_y_min,given_y_max):

    '''It's necessary to create a custom logarithmic grid because the default one
    doesn't display minor ticks when the scale is too wide.'''

    x_mins = []
    x_maxs = []
    for trace_data in fig.data:
        x_no_zero = [value for value in trace_data.x if value != 0]
        x_mins.append(min(x_no_zero))
        x_maxs.append(max(trace_data.x))
    x_mins = [value for value in x_mins if value != 0]
    x_min = min(x_mins)
    x_max = max(x_maxs)

    y_mins = []
    y_maxs = []
    for trace_data in fig.data:
        y_no_zero = [value for value in trace_data.y if value != 0]
        if len(y_no_zero) != 0:
            y_mins.append(min(y_no_zero))
            y_maxs.append(max(trace_data.y))
    y_min = min(y_mins)
    y_max = max(y_maxs)

    # If user asked for specific axis ranges:
    if given_x_min:
        x_min = float(given_x_min)

    if given_x_max:
        x_max = float(given_x_max)

    if given_y_min:
        y_min = float(given_y_min)

    if given_y_max:
        y_max = float(given_y_max)

    xstart = int(np.floor(np.log10(x_min)))
    xstop = int(np.ceil(np.log10(x_max)))
    ystart = int(np.floor(np.log10(y_min)))
    ystop = int(np.ceil(np.log10(y_max)))

    x_grid = np.logspace(xstart, xstop,xstop-xstart+1)
    for xs in range(xstart, xstop): #adding minor ticks
        for mp in range(2,10):
            grid = mp*10**xs
            x_grid = np.append(x_grid, grid)
    x_grid.sort()

    y_grid = np.logspace(ystart, ystop, ystop - ystart + 1)
    for ys in range(ystart, ystop):  # adding minor ticks
        for mp in range(2, 10):
            grid = mp * 10 ** ys
            y_grid = np.append(y_grid, grid)
    y_grid.sort()
    return x_grid, y_grid


def getPlotHtml(fig):
    return pl.io.to_html(fig, include_plotlyjs='cdn')


class TrackData:

    '''The class does not return a plot but rather edits the global fig object'''

    def __init__(self, particle, params, data, ndata, err_data, err_ndata, fig):

        self.fig = fig
        self.particle = particle
        self.params = params
        self.track_number = params.get('track_number')
        self.track_name = params.get('track_name')
        self.particle_number = params.get('particle_number')
        self.region_number = params.get('region_number')
        self.volume = params.get('volume')
        self.energy_range = (params.get('binning_from'), params.get('binning_to'))
        self.energy_bins = {
            'bins_from': params.get('binning_from'),
            'bins_to': params.get('binning_to'),
            'no_of_bins': int(params.get('no_bins')),
            'ratio': params.get('ratio')
        }
        self.df= []
        self.data = data
        self.err_data = err_data
        self.ndata = ndata
        self.err_ndata = err_ndata
        self.volume=volume
        self.setDataFrame()
        self.plotDf()

    def setDataFrame(self):

        '''The function creates a DataFrame from a JSON file'''

        bins_from = self.energy_bins['bins_from']
        bins_to = self.energy_bins['bins_to']
        no_of_bins = self.energy_bins['no_of_bins']
        energy_vector = np.logspace(np.log10(bins_from), np.log10(bins_to), num=no_of_bins+1)
        data = self.data
        err_data = self.err_data

        if self.ndata is not None:
            if len(self.ndata) > 0:
                extra_err_data = self.err_ndata
                extra_data = self.ndata
                extra_data = np.flip(extra_data)
                extra_err_data = np.flip(extra_err_data)
                if self.particle == "Neutron":
                    low_energy_vector= np.loadtxt("low_energy_neutrinos_binning.txt")
                    energy_vector = np.concatenate((low_energy_vector[:-1], energy_vector),axis=0)
                    data = np.concatenate((extra_data, data), axis=0)
                    err_data = np.concatenate((extra_err_data, err_data))
        self.df = pd.DataFrame({'E_min': energy_vector[:-1]})
        self.df['E_max'] = energy_vector[1:]
        self.df['Data'] = data
        if err_data:
            self.df['Err'] = err_data
            self.df['Err'] = self.df.apply(lambda row: row['Err'], axis=1).apply(lambda x: x/self.volume)
        print('\n\n---------------------------------------------')
        print(energy_vector)
        self.df['Eav'] = self.df.apply(lambda row: row['E_max'] * row['E_min'], axis=1).apply(lambda x: x**0.5)
        print(self.df['Eav'])
        self.df['Average'] = self.df.apply(lambda row: row['Data'], axis=1).apply(lambda x: x/self.volume)
        self.df['Av_lethargy'] = self.df.apply(lambda row: row['Average'] * row['Eav'], axis=1).apply(lambda x: x)
        if err_data:
            self.df['Err_lethargy'] = self.df.apply(lambda row: row['Err'] * row['Eav'], axis=1).apply(lambda x: x)

    def plotDf(self):

        '''The function creates a scatter plot using the Plotly library, adding a trace to the
        specified fig object. This allows subsequent traces to be added to the same plot. The
        subsequent plots are displayed in different colors taken from the custom_colors_ list to
        maintain the convention of displaying the same particle in a lighter version of the
        color (if program_version = 2).

        The function appears complex because it was choosed not to display the lower error bars if
        they would extend below the plot (due to a logarithmic scale issue). Therefore, the first two
        scatter traces are used to draw error bars with points of size 0, while the next two
        draw the actual points with invisible error bars.

        If you want to display all error bars, comment out the first two scatter plots (from the
        for loop) and remember to set a thickness greater than 0 for error_y in the next two.'''

        custom_colors_first = ['#336699', '#e88504', '#3b8132', '#c30010','#4B0082','#b68c05','#a66f9e', '#595959','#695049','#5c6d9c']
        custom_colors_second = ['#6699cc', '#e89f4c', '#6fbf7c', '#e57373','#6600FF','#e5b106','#d4a3c9', '#8e8e8e','#695049','#5c6d9c']
        global plot_count

        #Displaying only error bars
#Displaying only error bars
        if 'Err_lethargy' in self.df.columns:
            for i in range(len(self.df)):
                x = self.df['Eav'][i]
                y = self.df['Av_lethargy'][i]
                yerr = self.df['Err_lethargy'][i]

                if y != 0:
                    power = np.floor(np.log10(y))
                    divider = 10**int(power)
                    if (y-yerr)/divider>=0.1 :
                        scatter = go.Scatter(
                            showlegend=False,
                            x=[x],
                            y=[y],
                            mode='markers',
                            marker=dict(size=0, opacity=0),
                            hoverinfo='skip',
                            error_y=dict(type='data', array=[yerr], visible=True,color='black', width=3, thickness=1)) # to change error bars
                        self.fig.add_trace(scatter)
                    else:
                        scatter = go.Scatter(
                            showlegend=False,
                            x=[x],
                            y=[y],
                            mode='markers',
                            marker=dict(size=0, opacity=0),
                            hoverinfo='skip',
                            error_y=dict(type='data', array=[yerr], visible=True, symmetric=False,color='black', width=3, thickness=2))
                        self.fig.add_trace(scatter)

        global plot_count

        #Displaying data points
        name = f"{self.particle} (1st dataset)"
        if plot_count <= no_of_part - 1:
            if 'Err_lethargy' in self.df.columns:
                scatter = go.Scatter(
                    x=self.df['Eav'],
                    y=self.df['Av_lethargy'],
                    name=name,
                    text='',
                    mode='markers',
                    marker=dict(size=9, color=custom_colors_first[plot_count]),
                    hovertemplate=(
                        f'<b>{name}</b>' +
                        '<br>Energy: %{x} GeV' +
                        '<br>Lethargy fluence: %{y} cm⁻²<br>' +
                        '<extra></extra>'
                    ),
                    error_y=dict(
                        type='data',
                        array=self.df['Err_lethargy'],
                        visible=True,
                        color='black',
                        width=3,
                        thickness=0
                    )
                )
            else:
                scatter = go.Scatter(
                    x=self.df['Eav'],
                    y=self.df['Av_lethargy'],
                    name=name,
                    text='',
                    mode='markers',
                    marker=dict(size=9, color=custom_colors_first[plot_count]),
                    hovertemplate=(
                        f'<b>{name}</b>' +
                        '<br>Energy: %{x} GeV' +
                        '<br>Lethargy fluence: %{y} cm⁻²<br>' +
                        '<extra></extra>'
                    )
                )

            self.fig.add_trace(scatter)


        name = (self.particle+" (2nd dataset) ")
        if plot_count >no_of_part-1:
            scatter = go.Scatter(
                x=self.df['Eav'],
                y=self.df['Av_lethargy'],
                name=name,
                mode='markers',
                marker=dict(size=9, color = custom_colors_second[plot_count-no_of_part]),
                hovertemplate =
                    f'<b>{name}</b>'+
                    '<br>Energy: %{x} GeV'+
                    '<br>Lethargy fluence: %{y} cm⁻²<br>'+
                    '<extra></extra>',
                error_y=dict(type='data', array=self.df['Err_lethargy'], visible=True, color='black', width=3, thickness=0))
            self.fig.add_trace(scatter)

        plot_count = plot_count + 1

    def toCsv(self):

        '''The function overwrites the current file last_lethargy_spectra.csv in the current
        directory with the data used to generate the plot.'''

        empty_df = pd.DataFrame()
        params_df = pd.DataFrame(list(self.params.items()), columns=['Info', ' '])
        params_df.to_csv('last_lethargy_spectra.csv', mode='a', index=False)
        self.df = self.df.replace(0, '')
        empty_df.to_csv('last_lethargy_spectra.csv', mode = 'a', index = False)
        self.df.to_csv('last_lethargy_spectra.csv', mode='a', index=False)
        empty_df.to_csv('last_lethargy_spectra.csv', mode = 'a', index = False)



def main(file1, volume1, file2, volume2, particles, given_x_min, given_x_max, given_y_min, given_y_max, errors, size):
    '''
    Program for creating lethargy fluence spectra plots. It can accept one or two data sets
    (for comparison). It generates a plot with two logarithmic scales and specially processed
    data for each particle. You can choose which particles to display by specifying them
    sequentially after the --particles variable. If you want to see all particles from the
    file, please pass 'All' under the --particles variable.

    There is also possibility to specify axis boundaries. If you want them to be automatic,
    don't use last four arguments for parser, or set them "None".
    In principle program accepts also volumes of regions.

    The program displays a plot and potentially saves the preprocessed data to the file
    last_lethargy_spectra.csv. (For now disabled)

    Be aware program requires .ERR files in exact same location as main file, and exact same name.
    '''

    global plot_count
    plot_count = 0

    #open('last_lethargy_spectra.csv', 'w').close()

    fig = go.Figure()

    # Checking number of provided arguments
    if file2 == None and volume2== None:
        program_version = 1
    elif file2 != None and volume2 != None:
        program_version = 2
    else:
        print('ERROR')
        print("Mistake in provided arguments for second file! You provided: ")
        print("Second region = ", file2)
        print("Second volume = ", volume2)
        print("If you want to use just one set of data, skip ALL of this arguments.")
        print("If you want to use 2 sets of data, you have to provide all of --srun, --sregion and --sgiven_volume. They can't be None")
        sys.exit(1)

    global volume #check
    volume = float(volume1) #check

    file1 = os.path.join(DATA_DIR,file1) #change
    for root, _, files in os.walk(file1):
        for file_name in files:
            if "abs" not in file_name:
                if "rel" not in file_name:
                    file1 = file1 + "/" + file_name


    if errors == "on":
        error_file1 = file1.replace('.TXT', '.abs.ERR')
        try:
            loaded_error_data1 = spectra_help.main(error_file1)
        except Exception as e:
            loaded_error_data1 = None

    else:
        error_file1 = None
        loaded_error_data1 = None
    loaded_data1 = spectra_help.main(file1)


    if program_version == 2:
        file2 = os.path.join(DATA_DIR,file2)
        for root, _, files in os.walk(file2):
            for file_name in files:
                if "abs" not in file_name:
                    if "rel" not in file_name:
                        file2 = file2 + "/" + file_name
        if errors == "on":
            error_file2 = file2.replace('.TXT', '.abs.ERR')
            try:
                loaded_error_data2 = spectra_help.main(error_file2)
            except Exception as e:
                loaded_error_data1 = None
        else:
            error_file2 = None
            loaded_error_data2 = None
        loaded_data2 = spectra_help.main(file2)

    ndata = None
    err_ndata = None

    if particles == ['All']:
        particle_key_list = list(loaded_data1.keys())
    else:
        particle_key_list = particles

    global no_of_part
    part_count = 0
    for particle in particle_key_list:
        part_count+=1
    no_of_part = part_count

    for particle in particle_key_list:
        params = loaded_data1[particle]['params'][0]
        data = loaded_data1[particle]['data'][0]
        if errors == "on":
            err_data = loaded_error_data1[particle]['data'][0]
        else:
            err_data = None
        if particle == 'Neutron':
            if 'ndata' in loaded_data1['Neutron']:
                ndata = loaded_data1['Neutron']['ndata'][0]
                if errors == "on":
                    err_ndata = loaded_error_data1['Neutron']['ndata'][0]
                else:
                    err_ndata = None
        testTack = TrackData(particle,params, data, ndata, err_data, err_ndata, fig)

    region1 = os.path.basename(os.path.dirname(file1))
    fig.update_layout(title=('Lethargy Fluence Spectra for region: ' + region1), title_x=0.5)

    ndata = None
    err_ndata = None


    if program_version == 2:
        volume = float(volume2)
        for particle in particle_key_list:
            params = loaded_data2[particle]['params'][0]
            data = loaded_data2[particle]['data'][0]
            err_data = loaded_error_data2[particle]['data'][0]
            if particle == 'Neutron':
                if 'ndata' in loaded_data2['Neutron']:
                    ndata = loaded_data2['Neutron']['ndata'][0]
                    err_ndata = loaded_error_data2['Neutron']['ndata'][0]

            testTack = TrackData(particle,params, data, ndata, err_data, err_ndata, fig)

        region2 = os.path.basename(os.path.dirname(file2))
        fig.update_layout(title=('Lethargy Fluence Spectra for regions:' + region1 +' and for ' + region2))

        # CUSTOM TITLE
        #fig.update_layout(title=('Lethargy Fluence Spectra for front (1st dataset) and back (2nd dataset) - INNER part of new ECAL center ') , title_x=0.5)


    x_grid, y_grid = PreperaLogarythmicGrid(fig,given_x_min,given_x_max,given_y_min,given_y_max)

    fig.update_layout( # to change properties of the plot
        hoverlabel_align = 'left',
        xaxis_title='Energy [GeV]',
        yaxis_title='Lethargy fluence [cm⁻²]',
        xaxis=dict(
            tickvals=x_grid,
            ticktext=[f'$10^{{{int(np.log10(val))}}}$' if np.log10(val) % 1 == 0 else '' for val in x_grid],
            showgrid=True,
            gridcolor='white',
            gridwidth=0.5,
            type="log",
            exponentformat='power',
            range=[min(np.log10(x_grid)), max(np.log10(x_grid))]
        ),
        yaxis=dict(
            tickvals=y_grid,
            ticktext=[f'$10^{{{int(np.log10(val))}}}$' if np.log10(val) % 1 == 0 else '' for val in y_grid],
            showgrid=True,
            gridcolor='white',
            gridwidth=0.5,
            type="log",
            exponentformat='power',
            range=[min(np.log10(y_grid)), max(np.log10(y_grid))]
        )
    )

    width = SIZE_CONFIG.get(size)
    width = 2000.

    fig.update_layout(
        width=width
    )
    fig.show()  # display plot
    return getPlotHtml(fig) # plot to html



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Lethargy Fluence Spectra")
    parser.add_argument("--file1", help="path to folder with .txt file.")
    parser.add_argument("--volume1", help="Volume for selected region.")
    parser.add_argument('--particles', nargs='+', help='list of particles to display')

    parser.add_argument(
        '--file2',
        nargs='?',
        default=None,
        help="Second path to folder with .txt file."
    )
    parser.add_argument(
        '--volume2',
        nargs='?',
        default=None,
        help="Volume for second selected region."
    )

    # Additional arguments - for different boundaries of axes
    parser.add_argument(
        '--xmin',
        nargs='?',
        default=None,
        help="Custom min of x-axis. Default = None."
    )
    parser.add_argument(
        '--xmax',
        nargs='?',
        default=None,
        help="Custom max of x-axis. Default = None."
    )
    parser.add_argument(
        '--ymin',
        nargs='?',
        default=None,
        help="Custom min of y-axis. Default = None."
    )
    parser.add_argument(
        '--ymax',
        nargs='?',
        default=None,
        help="Custom max of y-axis. Default = None."
    )
    parser.add_argument(
        '--errors',
        nargs='?',
        default='on',
        choices=['on', 'off'],
        help="on/off for error bars"
    )
    parser.add_argument(
        '--size',
        nargs='?',
        default='medium',
        choices=['small', 'medium', 'large'],
        help="Size of the plot (for webpage purposes). Default = medium."
    )

    args = parser.parse_args()
    main(args.file1, args.volume1, args.file2, args.volume2, args.particles, args.xmin, args.xmax, args.ymin, args.ymax, args.errors, args.size)
