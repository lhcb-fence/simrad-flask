
import ast
import os
import copy
import math
import numpy as np
import re
import sys
import time
import threading
from multiprocessing import Pool
from urllib.request import unquote
from itertools import permutations
import zipfile
from datetime import datetime

import plotly.express as px
from sympy import divisors
import plotly.graph_objects as go

import dash

from dash import set_props, ctx, Patch, ALL, MATCH, dash_table, \
                 DiskcacheManager, CeleryManager, no_update
from dash.exceptions import PreventUpdate
from dash.dash_table.Format import Format, Scheme, Trim
import dash_bootstrap_components as dbc
from dash_breakpoints import WindowBreakpoints
from dash_extensions.enrich import DashProxy, Output, Input, State, \
                                   Serverside, html, dcc, callback, \
                                   ServersideOutputTransform, \
                                   FileSystemBackend, clientside_callback

from simrad_server.resources.scripts.parsers.scoring_parsers.usrbin_parser\
                                                             import UsrbinParser
from simrad_server.resources.scripts.plots.usrbin_plot import UsrbinPlot

from simrad_server.resources.scripts.utils.constants.plot_colors import \
                                                        PLOT_2D_COLORS as COLORS
from simrad_server.resources.scripts.utils.constants.color_scales import \
                                         BUILTIN_COLORSCALES, PLOTLY_COLORSCALES
from simrad_server.resources.scripts.utils.constants.file_parameters import \
                                   DELIMITER_CHAR_OPTIONS, END_LINE_CHAR_OPTIONS
from simrad_server.resources.scripts.utils.constants.lhcb_operations import \
                                      DELIVERED_LUMI, PP_CROSS_SECTIONS, RUN_DEF
from simrad_server.resources.scripts.utils.helpers.plot_helpers import \
                        str_tag_html_to_dash, create_colorscale_from_colorlist,\
                        get_saturated_colorscale, str_tag_html_to_unicode
from simrad_server.resources.scripts.utils.helpers.css_helpers import \
                                                                blue_roundbutton

FONT_AWESOME = "https://use.fontawesome.com/releases/v5.10.2/css/all.css"
CODEPEN = "https://codepen.io/chriddyp/pen/bWLwgP.css"

BASE_URL = '/usrbin_plot/'

REQUESTS_PATHNAME_PREFIX = os.getenv("URL_PREFIX", "") + BASE_URL

BACKEND_FOLDER = ''

RE_QUERY = re.compile(r"[(\?|\&)]([^=]+)\=([^&#]+)")

LHCB_MAP_NAME = 'LHCb_2D_map'

PLOT_BOX_NAME = 'box_rectangle'

MAX_NBR_WINDOWS = 4

MAX_NBR_PLOTS = 50

WINDOW_DIVIDER = 100

SIZE_DIV = 1.3  # divide the values of the plot size slider dep. on windows

INIT_ACTIVE_TAB_VALUE = '-1'

LOADING_FIGURE = px.scatter()
LOADING_FIGURE.add_annotation(text="Loading...", showarrow=False,
                              font={"size":100})
LOADING_FIGURE.update_layout(xaxis=dict(visible=False),
                             yaxis=dict(visible=False),
                             plot_bgcolor='rgba(0, 0, 0, 0)',
                             paper_bgcolor='rgba(0, 0, 0, 0)')

DIV_INLINE_FLEX_STYLE = {'display': 'flex', 'justifyContent': 'space-between',
                         'marginLeft': '1%', 'marginRight': '1%',
                         'verticalAlign': 'middle'}

# num: (abs_err, rel_err)
PLOT_VALUES_TO_ERR_FLAGS = {'mean': (False, False), 'abs_err': (True, False),
                            'rel_err': (False, True)}

COLORNAMES = BUILTIN_COLORSCALES
COLORNAME_TO_COLORSCALES = {'Custom1': create_colorscale_from_colorlist(COLORS)}
for colorname in COLORNAMES:
    COLORNAME_TO_COLORSCALES[colorname] = PLOTLY_COLORSCALES[colorname]


def decode_bins_from_table(bins):
    bins_ = bins.split(',')

    return np.array([float(elem) for elem in bins_])


def encode_bins_for_table(bins):

    return ','.join(str(el) for el in bins)


def extract_bins(arguments):
    i, extracted_plots, value_file_path, abs_err_file_path,\
                     rel_err_file_path, inp_file_path, particles = arguments
    if (abs_err_file_path):
        bins = UsrbinParser.get_bins(value_file_path,
                                     abs_err_file_path,
                                     rel_err=False)
    elif (rel_err_file_path):
        bins = UsrbinParser.get_bins(value_file_path,
                                     rel_err_file_path,
                                     rel_err=True)
    else:
        bins = UsrbinParser.get_bins(value_file_path)
    new_plots = []
    for bin in bins:
        to_add = False
        if (particles):
             if (bin['particle_name'] in particles):
                 to_add = True
        else:
            to_add = True
        if (to_add):
            new_plots.append(UsrbinPlot(bin))
    extracted_plots[i] = new_plots

    return i, new_plots


def get_colorbar_x_axis_pos(x0, x1, y0, y1, window_height, window_width,
                            window_multiplicator: float = 30.0,
                            window_divider: float = WINDOW_DIVIDER,
                            window_split=True):
    axis_ratio = (x1-x0) / (y1-y0)
    new_plot_height = int(window_height*window_multiplicator/window_divider)
    x_width = axis_ratio*new_plot_height
    if (window_split):
        plot_width = window_width/2
    else:
        plot_width = 10*window_width/12
    offset = (x_width/plot_width)/2.0

    return 0.5 + offset

if 'REDIS_URL' in os.environ:
    # Use Redis & Celery if REDIS_URL set as an env variable
    from celery import Celery
    celery_app = Celery(__name__, broker=os.environ['REDIS_URL'],
                        backend=os.environ['REDIS_URL'])
    background_callback_manager = CeleryManager(celery_app)
    print('------------------------------------- USING CELERY')

else:
    # Diskcache for non-production apps when developing locally
    import diskcache
    diskcache_dir = os.getenv("DISKCACHE_DIR", "diskcache_tmp/")
    cache = diskcache.Cache(diskcache_dir)
    background_callback_manager = DiskcacheManager(cache)
    print('------------------------------------ USING DISKCACHE')



def init_dashboard(server, transforms=None):

    #if (not os.path.isdir(backend_dir)):
    #    error_message: str = ("The provided directory for backend was "
    #                          + "not found")
    #    raise Exception(error_message)
    if (transforms is None):
        backend_dir = os.getenv("CACHE_DIR", "temporary_backend/")
        backend= FileSystemBackend(backend_dir)
        transforms = [ServersideOutputTransform(backends=[backend])]

    app = DashProxy(__name__, server=server,
                    routes_pathname_prefix=BASE_URL,  # Backend routes
                    requests_pathname_prefix=REQUESTS_PATHNAME_PREFIX,  # Frontend requests
                    external_stylesheets=[dbc.themes.BOOTSTRAP, FONT_AWESOME,
                                          CODEPEN],
                    transforms=transforms,
                    suppress_callback_exceptions = True,
                    prevent_initial_callbacks = True,
                    #background_callback_manager=background_callback_manager,
                    )

    # Create dropdown menu with available colorscales
    color_dropdown = dcc.Dropdown(options=list(COLORNAME_TO_COLORSCALES.keys()),
                                  value='Custom1', id='bin_color_dropdown',
                                  clearable=False)
    # Create dropdown to choose projection axis
    default_proj = 'x'
    proj_dropdown = dcc.Dropdown(['x', 'y', 'z'], default_proj,
                                 id='proj_dropdown', clearable=False)
    # Create slider to control the depth of the projected axis
    depth_slider = dcc.Slider(min=0, max=0, id='depth_slider',
                              )#tooltip={"placement": "bottom",
                                #       "transform": "displayFloat"})
    depth_slider_header = dbc.Stack([html.Strong('', id='depth_slider_label'),
                                     html.Div('', id='depth_slider_header')],
                                     direction='horizontal', gap=3)
    # Create input for the number of collisions
    colli_input = dbc.Input(type="text", value='1.0',
                            debounce=True,  # must press enter to propagate
                            id="colli_input",)
    accordion_1 = dbc.AccordionItem(colli_input,
                                    title=html.Label('NUMBER OF COLLISIONS:'))
    year_lum_options = []
    for key in list(DELIVERED_LUMI.keys()):
        crt_year = datetime.now().year
        if (crt_year > int(key)):
            key_label = 'Up to ' + str(key)
        else:
            key_label = 'Up to ' + str(key) + ' (Nominal)'
        year_lum_options.append({'label': key_label, 'value': key})
    year_lum_dropdown = dcc.Dropdown(options=year_lum_options,
                                     id='year_lum_dropdown', clearable=False,
                                     style={'width': '100%'})
    year_lum_div = html.Div('', id='year_lum_div', style={'width': '70%'})
    accordion_2 = dbc.AccordionItem(dbc.Stack([html.Label('Year:'),
                                               year_lum_dropdown,
                                               year_lum_div],
                                              direction='horizontal', gap=3),
                                    title='DELIVERED LUMINOSITY (YEARS):')
    year_run_dropdown = dcc.Dropdown(list(RUN_DEF.keys()),
                                     id='year_run_dropdown', clearable=False,
                                     style={'width': '100%'})
    year_run_div = html.Div('', id='year_run_div', style={'width': '70%'})
    accordion_3 = dbc.AccordionItem(dbc.Stack([html.Label('Run:'),
                                               year_run_dropdown,
                                               year_run_div],
                                              direction='horizontal', gap=3),
                                    title='DELIVERED LUMINOSITY (RUNS):')
    energy_lum_div = html.Div('Beam Energy (TeV)', id='energy_lum_div')
    energy_lum_dropdown = dcc.Dropdown(list(PP_CROSS_SECTIONS.keys()),
                                       id='energy_lum_dropdown',
                                       clearable=False, style={'width': '70%'})
    lumi_div = html.Div('Luminosity (fb\u207B\u00B9)', id='lumi_div')
    lumi_input = dbc.Input(id='lumi_input', type='text', debounce=True,
                           style={'width': '100%'})
    custom_lumi_button = dbc.Button('Calculate', color='primary',
                                    className="me-1", outline=True,
                                    id='custom_lumi_button',
                                    style={'textTransform':'none'})
    accordion_4 = dbc.AccordionItem(dbc.Stack([energy_lum_div,
                                               energy_lum_dropdown,
                                               lumi_div, lumi_input,
                                               custom_lumi_button],
                                              direction='horizontal', gap=3),
                                    title='CUSTOM DELIVERED LUMINOSITY:')
    colli_accordion = html.Div(dbc.Accordion([accordion_1, accordion_2,
                                              accordion_3, accordion_4],
                                             flush=True))
    # Switch to control free or fixed plot ratio
    info_text_fix_ratio_label = ('If selected, this option allows to keep '
                                 + 'the aspect ratio betwen the real '
                                 + 'dimensions of the 2D map axes.')
    info_text_fix_ratio_label = html.Div(info_text_fix_ratio_label,
                                         style={'font-size': '150%'})
    fix_ratio_label = html.Div([html.Strong('Fix Aspect Ratio'),
                                dbc.Button(className="far fa-question-circle",
                                           style=blue_roundbutton,
                                           id='info_fix_ratio_label'),
                                dbc.Popover(info_text_fix_ratio_label,
                                            target='info_fix_ratio_label',
                                            body=True, trigger="hover")],
                                style={'display': 'flex',
                                       'flex-direction': 'row'})
    fix_ratio = dbc.Checklist(options=[{'label': fix_ratio_label,
                                        'value': 1}],
                              value=[1], id='fix_ratio', switch=True)
    box_display = dbc.Checklist(options=[{'label': html.Strong('Display Box'),
                                          'value': 1}],
                                value=[1], id='box_display', switch=True)
    plot_display_elements = html.Div([box_display, fix_ratio],
                                     style=DIV_INLINE_FLEX_STYLE)
    # Overlay 2d-map on the plot
    lhcb_map_calc = dbc.Button('COMPUTE LHCb 2D-MAP', color='primary',
                               className="me-1", outline=True, id='map_calc',
                               style={'textTransform':'none'})
    info_text_map_display_label = ('If selected, this option allows to overlay '
                                    + 'a 2d-map of LHCb. Note that this is '
                                    + 'for information puropose only and does '
                                    + 'not represent the exact geometry used '
                                    + 'for the computation of the scoring.')
    info_text_map_display_label = html.Div(info_text_map_display_label,
                                           style={'font-size': '150%'})
    map_display_label = html.Div([html.Strong('Display LHCb 2D-map'),
                                  dbc.Button(className="far fa-question-circle",
                                             style=blue_roundbutton,
                                             id='info_map_display_label'),
                                  dbc.Popover(info_text_map_display_label,
                                              target='info_map_display_label',
                                              body=True, trigger="hover")],
                                  style={'display': 'flex',
                                         'flex-direction': 'row'})
    map_display = dbc.Checklist(options=[{'label': map_display_label,
                                          'value': 1}],
                                value=[1], id='map_display', switch=True)
    map_display_constr_message = html.Strong("(Map Display Only available Offline for Time being)")
    control_2d_map_div = html.Div([map_display_constr_message,
                                   dbc.Stack([lhcb_map_calc, map_display],
                                             direction='horizontal', gap=2),
                                   ], id='control_2d_map_div')
    # Create dropdowns for averaging in each direction
    x_averaging = html.Div([html.Div('X-voxel size: '),
                            dcc.Dropdown(dict(),
                                         id={'type': 'averaging', 'axis': 'x'},
                                         clearable=False,
                                         style={'width': "100%"})],
                           style=DIV_INLINE_FLEX_STYLE)
    y_averaging = html.Div([html.Div('Y-voxel size: '),
                            dcc.Dropdown(dict(),
                                         id={'type': 'averaging', 'axis': 'y'},
                                         clearable=False,
                                         style={'width': "100%"})],
                           style=DIV_INLINE_FLEX_STYLE)
    z_averaging = html.Div([html.Div('Z-voxel size: '),
                            dcc.Dropdown(dict(),
                                         id={'type': 'averaging', 'axis': 'z'},
                                         clearable=False,
                                         style={'width': "100%"})],
                           style=DIV_INLINE_FLEX_STYLE)
    # Create inputs for box averaging
    box_x_min = dbc.Input(id={'type': 'box_min', 'axis': 'x'}, type='text',
                          debounce=True, placeholder='min. value')
    box_x_max = dbc.Input(id={'type': 'box_max', 'axis': 'x'}, type='text',
                          debounce=True, placeholder='max. value')
    box_y_min = dbc.Input(id={'type': 'box_min', 'axis': 'y'}, type='text',
                          debounce=True, placeholder='min. value')
    box_y_max = dbc.Input(id={'type': 'box_max', 'axis': 'y'}, type='text',
                          debounce=True, placeholder='max. value')
    box_z_min = dbc.Input(id={'type': 'box_min', 'axis': 'z'}, type='text',
                          debounce=True, placeholder='min. value')
    box_z_max = dbc.Input(id={'type': 'box_max', 'axis': 'z'}, type='text',
                          debounce=True, placeholder='max. value')
    box_x = dbc.Stack([html.Label('X: '), box_x_min, box_x_max], gap=3,
                      direction='horizontal')
    box_y = dbc.Stack([html.Label('Y: '), box_y_min, box_y_max], gap=3,
                      direction='horizontal')
    box_z = dbc.Stack([html.Label('Z: '), box_z_min, box_z_max], gap=3,
                      direction='horizontal')
    box_calc = dbc.Button('COMPUTE BOX STATISTIC', color='primary',
                           className='me-1', outline=True, id='box_calc',
                           style={'textTransform':'none'})
    box_zoom = dbc.Button('ZOOM TO BOX', color='primary',
                           className='me-1', outline=True, id='box_zoom',
                           style={'textTransform':'none'})
    box_reset = dbc.Button('RESET BOX', color='primary',
                           className='me-1', outline=True, id='box_reset',
                           style={'textTransform':'none'})
    box_control = html.Div([box_calc, box_zoom, box_reset],
                            style=DIV_INLINE_FLEX_STYLE)
    card_header = dbc.CardHeader(html.Strong('Average over Box: '))
    card_body = dbc.Stack([html.Br(), box_x, box_y, box_z, html.Br(),
                           box_control,], gap=3)
    box_average_card = dbc.Card([card_header, card_body],
                                style={"border": "none"})
    # Create tabs for different version of the plot
    usrbin_fig_divs = []
    fig_progress_bars = []
    scale_u_trigs = []
    scale_s_trigs = []
    clear_trigs = []
    box_c_trigs = []
    depth_trigs = []
    size_trigs = []
    box_d_trigs = []
    box_z_trigs = []
    map_c_trigs = []
    fix_r_trigs = []
    color_trigs = []
    fig_a_trigs = []
    for i in range(MAX_NBR_WINDOWS):
        bin_tabs = dbc.Tabs(id={'type': 'bin_tabs', 'window_nbr': i},
                            active_tab=INIT_ACTIVE_TAB_VALUE)
        scale_u_trigs.append(html.Div(id={'type': 'scale_u_trigs',
                                          'window_nbr': i}))
        scale_s_trigs.append(html.Div(id={'type': 'scale_s_trigs',
                                          'window_nbr': i}))
        clear_trigs.append(html.Div(id={'type': 'clear_trigs',
                                        'window_nbr': i}))
        box_c_trigs.append(html.Div(id={'type': 'box_c_trigs',
                                        'window_nbr': i}))
        depth_trigs.append(html.Div(id={'type': 'depth_trigs',
                                        'window_nbr': i}))
        size_trigs.append(html.Div(id={'type': 'size_trigs',
                                        'window_nbr': i}))
        box_d_trigs.append(html.Div(id={'type': 'box_d_trigs',
                                        'window_nbr': i}))
        box_z_trigs.append(html.Div(id={'type': 'box_z_trigs',
                                        'window_nbr': i}))
        map_c_trigs.append(html.Div(id={'type': 'map_c_trigs',
                                        'window_nbr': i}))
        fix_r_trigs.append(html.Div(id={'type': 'fix_r_trigs',
                                        'window_nbr': i}))
        color_trigs.append(html.Div(id={'type': 'color_trigs',
                                        'window_nbr': i}))
        fig_a_trigs.append(html.Div(id={'type': 'fig_a_trigs',
                                        'window_nbr': i}))
        # Create button to download raw data
        info_text_download_header = ("If plot data, the data are downloaded as "
                                     + "a single .txt file. Where the columns "
                                     + "represents the mid-point of the x-, y- "
                                     + "and z- boundaries of one mesh slice, "
                                     + "followed by the value and error of the "
                                     + "dose. If raw data, the entire mesh "
                                     + "is downloaded in 2 separate files "
                                     + "representing the values and the errors."
                                     + " In each file, the first line is the "
                                     + "X-bins (size M+1), the second line is "
                                     + "the Y-bins (size N+1), and the third "
                                     + "line is the Z-bins (size P+1). These "
                                     + "three lines are followed by M times a "
                                     + "matrix of size (N x P).")
        info_text_download_header = html.Div(info_text_download_header,
                                             style={'font-size': '150%'})
        str_id_header = 'info_download_label_{}'.format(i)
        download_header = dbc.Stack([html.Strong('Exporting Data: '),
                                     dbc.Button(className='far fa-question-circle',
                                                style=blue_roundbutton,
                                                id=str_id_header),
                                     dbc.Popover(info_text_download_header,
                                                 target=str_id_header,
                                                 body=True, trigger="hover")],
                                    direction='horizontal', gap=0)
        download_delimiter_header = html.Div('Delimiter Character: ',
                                             style={'width': "25%"})
        download_delimiter_dropdown = dcc.Dropdown(
                                       options=DELIMITER_CHAR_OPTIONS,
                                       value=DELIMITER_CHAR_OPTIONS[0]['value'],
                                       id={'type': 'download_delimiter',
                                           'window_nbr': i},
                                       clearable=False, style={'width': "45%"})
        download_end_line_header = html.Div('End-of-line Character: ',
                                            style={'width': "25%"})
        download_end_line_dropdown = dcc.Dropdown(
                                        options=END_LINE_CHAR_OPTIONS,
                                        value=END_LINE_CHAR_OPTIONS[0]['value'],
                                        id={'type': 'download_end_line',
                                            'window_nbr': i},
                                        clearable=False, style={'width': "45%"})
        download_button = dbc.Button('Download Plot Data', color='primary',
                                     className='me-1', outline=True,
                                     id={'type': 'download_bin_data_btn',
                                         'window_nbr': i}, style={'width': "50%"})
        download_dcc = dcc.Download(id={'type': 'download_bin_data',
                                        'window_nbr': i})
        raw_download_button = dbc.Button('Download Raw Data', color='primary',
                                         className='me-1', outline=True,
                                         id={'type': 'download_bin_raw_data_btn',
                                             'window_nbr': i},
                                         style={'width': "50%"})
        raw_download_dcc = dcc.Download(id={'type': 'download_bin_raw_data',
                                            'window_nbr': i})
        download_button_stack = html.Div([download_delimiter_header,
                                          download_delimiter_dropdown,
                                          download_end_line_header,
                                          download_end_line_dropdown,
                                          download_button, download_dcc,
                                          raw_download_button,
                                          raw_download_dcc],
                                         style=DIV_INLINE_FLEX_STYLE)
        #fig_progress_bar = html.Div(dbc.Stack([html.Br(),
        #                                       html.Strong("Applying Changes ..."),
        #                                       html.Br(),
        #                                       html.Progress(id={'type': 'fig_progress_bar',
        #                                        'window_nbr': i}, value='0'),
        #                                        html.Br(),
        #                                        ],
        #                                        gap=3), style={'display': 'none'})

        fig_progress_bar = dcc.Loading(html.Div([]),
                                       id={'type': 'fig_progress_bar',
                                           'window_nbr': i})
        fig_progress_bars.append(fig_progress_bar)


        # Plot tabs
        plot_tabs = dbc.Tabs([dbc.Tab(label='Averaged Values', tab_id='mean',
                                      tabClassName="fw-bold"),
                              dbc.Tab(label='Absolute Errors', tab_id='abs_err',
                                      tabClassName="fw-bold"),
                              dbc.Tab(label='Relative Errors', tab_id='rel_err',
                                      tabClassName="fw-bold"),],
                              id={'type': 'plot_tabs', 'window_nbr': i},
                              active_tab='mean')

        # Combine all in a single div
        usrbin_fig_div = html.Div([plot_tabs, bin_tabs,
                                   html.Br(), download_header,
                                   download_button_stack],
                                  id={'type': 'usrbin_fig_div',
                                      'window_nbr': i},
                                  style={'display': 'none'})

        #usrbin_fig_divs.append(dcc.Loading(usrbin_fig_div, type='circle'))
        usrbin_fig_divs.append(usrbin_fig_div)
    # Create data table to display bin information
    bin_table = dash_table.DataTable(id='bin_table', page_size=10,
                                     merge_duplicate_headers=True,
                                     style_header={'textAlign': 'center'},
                                     #style_as_list_view=True,
                                     #row_selectable='multi',
                                     )
    bin_table_div = html.Div([bin_table], id='bin_table_div')
    table_hid_btn = dbc.Button(className='me-1', outline=True,
                               id="table_hid_btn",
                               style={'width': "100%"})
    # Create labels for entries
    info_text_proj_axis_label = ("Select the axis along which the slice is "
                                 + "taken.")
    info_text_proj_axis_label = html.Div(info_text_proj_axis_label,
                                         style={'font-size': '150%'})
    proj_axis_label = dbc.Stack([html.Strong('Slice Axis: '),
                                 dbc.Button(className='far fa-question-circle',
                                             style=blue_roundbutton,
                                             id='info_proj_axis_label'),
                                 dbc.Popover(info_text_proj_axis_label,
                                             target='info_proj_axis_label',
                                             body=True, trigger="hover")],
                                direction='horizontal', gap=0)
    info_text_color_label = 'Select the color scale of the 2D map.'
    info_text_color_label = html.Div(info_text_color_label,
                                     style={'font-size': '150%'})
    color_label = dbc.Stack([html.Strong('Color Scale: '),
                             dbc.Button(className="far fa-question-circle",
                                        style=blue_roundbutton,
                                        id='info_color_label'),
                             dbc.Popover(info_text_color_label,
                                         target='info_color_label',
                                         body=True, trigger="hover")],
                            direction='horizontal', gap=0)
    info_text_averaging_label = ("The value over one voxel is already the "
                                 + "mean value over the volume of this voxel. "
                                 + "This option allows to average over "
                                 + "multiple voxels.")
    info_text_averaging_label = html.Div(info_text_averaging_label,
                                         style={'font-size': '150%'})
    averaging_label = dbc.Stack([html.Strong('Voxel Averaging: '),
                                 dbc.Button(className="far fa-question-circle",
                                            style=blue_roundbutton,
                                            id='info_averaging_label'),
                                 dbc.Popover(info_text_averaging_label,
                                             target='info_averaging_label',
                                             body=True, trigger="hover")],
                                direction='horizontal', gap=0)
    card_header = dbc.CardHeader(averaging_label)
    card_body = dbc.Stack([html.Br(), x_averaging, y_averaging, z_averaging],
                          gap=3)
    voxel_average_card = dbc.Card([card_header, card_body],
                                  style={"border": "none"})
    card_header = dbc.CardHeader(html.Strong('Irradiation Settings: '))
    card_body = dbc.Stack([html.Br(), colli_accordion], gap=3)
    colli_card = dbc.Card([card_header, card_body], style={"border": "none"})
    # Create button to display plot side by side
    screen_1_str = html.Div("\u25FB", style={'fontSize': '4.0rem',
                                             'margin-left': '70px',
                                             'margin-right': '70px'})
    screen_2_str = html.Div("\u25EB", style={'fontSize': '4.0rem',
                                             'margin-left': '70px',
                                             'margin-right': '70px'})
    screen_3_str = html.Div("\u2BD0", style={'fontSize': '4.0rem',
                                             'margin-left': '70px',
                                             'margin-right': '70px'})
    split_button = dbc.RadioItems(id='split_radio', className='btn-group',
                                  inputClassName='btn-check',
                                  labelClassName='btn btn-outline-primary',
                                  labelCheckedClassName='active',
                                  options=[{'label': screen_1_str, 'value': 1},
                                           {'label': screen_2_str, 'value': 2},
                                           {'label': screen_3_str, 'value': 4}])
    split_button = html.Div([split_button], style={'textAlign': 'center'})
    try:
        # make a try to prevent int casting errors
        min_plot_size = int(os.getenv("MIN_USRBIN_PLOT_SIZE"))
        max_plot_size = int(os.getenv("MAX_USRBIN_PLOT_SIZE"))
        dflt_plot_size = int(os.getenv("DFLT_USRBIN_PLOT_SIZE"))
    except:
        min_plot_size = 30
        max_plot_size = 120
        dflt_plot_size = 60
    plot_size_slider = dcc.Slider(min=min_plot_size, max=max_plot_size,
                                  value=dflt_plot_size,
                                  id='plot_size_slider',
                                  marks={min_plot_size: 'Small',
                                         max_plot_size: 'Large'})
    plot_size_slider = html.Div([plot_size_slider],
                                style={'marginRight': '30%',
                                       'marginLeft': '30%'})
    # Create card for expert user
    expert_label_1 = html.Strong('Welcome LHCb Radiation Expert !')
    expert_label_2 = html.Div("As an expert, you have access to unique "
                              "features. Click on the button if you would like "
                              "to change the view to user or back to expert.")
    expert_button = dbc.Button(id='expert_bin_button')
    expert_card = dbc.Card(dbc.CardBody([dbc.Stack([expert_label_1,
                                                    expert_label_2,
                                                    expert_button],
                                                   direction='horizontal',
                                                   gap=3)],
                                        className='mt-3'))
    # Create the entire layout per rows
    data_loading_div = html.Strong("Please wait, data are loading, this may "
                                   "take a few minutes ...",
                                   style={'font-size': '300%'})
    data_loading_bar = html.Progress(id='bin_progress_bar',
                                     value='0.0', max='1.0')
    row_progress = html.Div([
                    dbc.Row([dbc.Col([dbc.Stack([html.Br(),
                                                 data_loading_div,
                                                 html.Br(),
                                                 data_loading_bar,
                                                 html.Br(),
                                                 ],
                                                gap=3)
                                   ], width={'size': 10, "offset": 1})
                                ], align='center', id='progress_bin_row_div',
                                style={'display': 'block'})
                              ])
    expert_row = html.Div([
                    dbc.Row([dbc.Col([dbc.Stack([html.Br(),
                                                 expert_card,
                                                 html.Br(),
                                                 ],
                                                gap=3)
                                   ], width={'size': 10, "offset": 1})
                                ], align='center', id='expert_bin_row_div',
                                style={'display': 'none'})
                              ])
    row_1 = html.Div([
             dbc.Row([dbc.Col([dbc.Stack([proj_axis_label, proj_dropdown,
                                          html.Br(),
                                          color_label, color_dropdown,
                                          html.Br(),
                                          voxel_average_card], gap=3)],
                               width={'size': 2, 'offset': 1}, align='start'),
                      dbc.Col([box_average_card, html.Br(),
                               plot_display_elements, html.Br(),
                               control_2d_map_div],
                              width={'size': 3, 'offset': 1}, align='start'),
                      dbc.Col([dbc.Stack([colli_card, html.Br()],
                                         gap=3)],
                               width={'size': 3, 'offset': 1}, align='start'),
                      ], align='center')
                  ])
    '''
    row_2 = html.Div([
                dbc.Row([dbc.Col([dbc.Stack([html.Br(),
                                             html.Br(),
                                             plot_tabs],
                                            gap=3,)
                               ], width={'size': 12})
                            ], className='p-4',
                            align='center',
                            justify='center')
                          ])
    '''
    row_3_bis = html.Div([dbc.Row(dbc.Col([html.Br(), html.Br(),
                                           split_button, html.Br(),
                                           plot_size_slider],
                                         ),# width={'size': 4, 'offset': 4}),
                        )])
    row_3 = html.Div([
                dbc.Row([dbc.Col([dbc.Stack([html.Br(), table_hid_btn,
                                             bin_table_div,
                                             html.Br(),
                                             depth_slider_header,
                                             depth_slider,
                                             html.Br()],
                                            gap=3)
                               ], width={'size': 10, 'offset': 1})
                            ], align='center')
                          ])
    row_4 = html.Div([
                dbc.Row([dbc.Col([dbc.Stack([fig_progress_bars[0],
                                             usrbin_fig_divs[0]], gap=3)],
                                 width={'size': 12},
                                 id={'type': 'fig_col', 'window_nbr': 0}),
                         dbc.Col([dbc.Stack([fig_progress_bars[1],
                                             usrbin_fig_divs[1]], gap=3)],
                                 width={'size': 0},
                                 id={'type': 'fig_col', 'window_nbr': 1}),
                            ], className='p-4', align='center',
                            justify='center', id='plot_row_1'),
                dbc.Row([dbc.Col([dbc.Stack([fig_progress_bars[2],
                                             usrbin_fig_divs[2]], gap=3)],
                                 width={'size': 0},
                                 id={'type': 'fig_col', 'window_nbr': 2}),
                         dbc.Col([dbc.Stack([fig_progress_bars[3],
                                             usrbin_fig_divs[3]], gap=3)],
                                 width={'size': 0},
                                 id={'type': 'fig_col', 'window_nbr': 3}),
                            ], className='p-4', align='center',
                            justify='center', id='plot_row_2'),
                          ])
    plot_rows = html.Div([row_3, row_3_bis, row_4],
                         id='bin_plot_rows_div', style={'display': 'block'})
    bin_options_div = html.Div(dbc.Stack([expert_row, row_1]),
                             id='bin_options_div')
    options_hid_btn = dbc.Button(className='me-1', outline=True,
                                 id="options_hid_btn",
                                 style={'width': "100%"})
    rows = html.Div([options_hid_btn, bin_options_div, #row_2,
                     plot_rows],
                    id='bin_rows_div', style={'display': 'none'})

    # Adding backend components
    modal_timeout_header = dbc.ModalHeader(dbc.ModalTitle("This plot expired."),
                                           close_button=True)
    modal_timeout_body = dbc.ModalBody("Please reload your web page to "
                                       "access the content.")
    modal_timeout_footer = dbc.ModalFooter(dbc.Button("Close",
                                                       id="foot_timeout_bin",
                                                       className="ms-auto",
                                                       n_clicks=0,))
    modal_timeout = dbc.Modal([modal_timeout_header, modal_timeout_body,
                               modal_timeout_footer],
                               id="timeout_bin", centered=True, is_open=False)
    modal_map_header = dbc.ModalHeader(dbc.ModalTitle("Unavailable 2D-map."),
                                       close_button=True)
    modal_map_body = dbc.ModalBody("The custom LHCb 2D-map for this scoring "
                                   "is not available.")
    modal_map_footer = dbc.ModalFooter(dbc.Button("Close",
                                                  id="close-modal-map",
                                                  className="ms-auto",
                                                  n_clicks=0,))
    modal_map = dbc.Modal([modal_map_header, modal_map_body,
                               modal_map_footer],
                               id="modal-map", centered=True, is_open=False)
    modal_split_header = dbc.ModalHeader(dbc.ModalTitle("Unavailable Option."),
                                         close_button=True)
    modal_split_body = dbc.ModalBody("Only USRBIN with the same mesh "
                                     "can be compared side-by-side.")
    modal_split_footer = dbc.ModalFooter(dbc.Button("Close",
                                         id="close-modal-split",
                                         className="ms-auto",
                                         n_clicks=0,))
    modal_split = dbc.Modal([modal_split_header, modal_split_body,
                             modal_split_footer],
                             id="modal-split", centered=True, is_open=False)
    modal_max_plot_header = dbc.ModalHeader(dbc.ModalTitle("Warning"),
                                            close_button=True)
    modal_max_plot_body = dbc.ModalBody("The number of plots is limited to "
                                        + str(MAX_NBR_PLOTS) + ". All the "
                                        "required plots could not be "
                                        "displayed.")
    modal_max_plot_footer = dbc.ModalFooter(dbc.Button("Close",
                                            id="close-modal-max-plot",
                                            className="ms-auto",
                                            n_clicks=0,))
    modal_max_plot = dbc.Modal([modal_max_plot_header, modal_max_plot_body,
                                modal_max_plot_footer],
                               id="modal-max-plot", centered=True,
                               is_open=False)
    modal_load_error = dbc.Modal([], id="load_error_bin", keyboard=False,
                                 backdrop="static",)

    breakpoints = WindowBreakpoints(
            id='breakpoints_bin',
            # Define the breakpoint thresholds
            widthBreakpointThresholdsPx=[80, 10000],
            # And their name, note that there is one more name than breakpoint thresholds
            widthBreakpointNames=["sm", "md", "lg"],
        )

    store_children = []
    for i in range(MAX_NBR_PLOTS+1):    # +1 bcs last one use as a temp. store
        store_children.append(dcc.Store(id={'type': 'cache_store_bin',
                                            'plot_nbr': i}))
    bin_tabs_store = []
    for i in range(MAX_NBR_WINDOWS):
        bin_tabs_store.append(dcc.Store(id={'type': 'bin_tabs_store',
                                            'window_nbr': i}))
    backend_components = [dcc.Location(id='url_bin', refresh=False),
                          html.Div(id='cache_store_bin_container',
                                   children=store_children),
                          modal_timeout, modal_map, modal_split,
                          modal_load_error, modal_max_plot,
                          dbc.Alert('', dismissable=True, id='warning_bin',
                                    is_open=False),
                          breakpoints,
                          dcc.Store(id='temp_store_plot_cache_transit'),
                          dcc.Store(id='temp_store_init_value_transit'),
                          dcc.Store(id='split_radio_store'),
                          html.Div(id='are_plots_comparable', hidden=True),
                          html.Div(id='trigger_init_callback', hidden=True),
                          ]
    backend_components += bin_tabs_store
    backend_components += scale_u_trigs
    backend_components += scale_s_trigs
    backend_components += clear_trigs
    backend_components += box_c_trigs
    backend_components += depth_trigs
    backend_components += size_trigs
    backend_components += box_d_trigs
    backend_components += box_z_trigs
    backend_components += map_c_trigs
    backend_components += fix_r_trigs
    backend_components += color_trigs
    backend_components += fig_a_trigs

    app.layout = html.Div(backend_components + [row_progress, rows])

    #return server
    return app

################################################################################
# Error handling
################################################################################

@callback(
    Output("timeout_bin", "is_open"),
    Input("foot_timeout_bin", "n_clicks"),
    prevent_initial_call=True)
def toggle_modal_timeout_bin(n):

    return False

def error_handler(err):

    set_props("timeout_bin", dict(is_open=True))


@callback(
    Output("modal-map", "is_open"),
    Input("close-modal-map", "n_clicks"),
    prevent_initial_call=True)
def toggle_modal_map(n):

    return False

def map_error_handler():

    set_props("modal-map", dict(is_open=True))

@callback(
    Output("modal-split", "is_open"),
    Input("close-modal-split", "n_clicks"),
    prevent_initial_call=True)
def toggle_modal_split(n):

    return False


def split_error_handler():

    set_props("modal-split", dict(is_open=True))


@callback(
    Output("modal-max-plot", "is_open"),
    Input("close-modal-max-plot", "n_clicks"),
    prevent_initial_call=True)
def toggle_modal_max_plot(n):

    return False


def max_plot_error_handler():

    set_props("modal-max-plot", dict(is_open=True))


def load_error_handler(err):
    children = [dbc.ModalHeader(dbc.ModalTitle("Data Loading Error."),
                                close_button=False),
                dbc.ModalBody(str(err)), dbc.ModalFooter('')]

    set_props("load_error_bin", dict(children=children, is_open=True))



################################################################################
# Callack formatting
################################################################################

def check_box_bound(val, other_val, extr, bins, bin_step):
    """Check if the provided value is a valid float and round it up to the
    closest available bin."""
    invalid = False
    try:
        val_ = float(val)
    except:
        invalid = True
    if (not invalid):

        ind = int(min(max(round((val_-bins[0])/bin_step, 0), 0), len(bins)-1))
        try:    # respect the ordering
            other_val_ = float(other_val)
            if (extr == 'min'):
                # the min value can not be greater than the crt max value
                if (bins[ind] >= other_val_):
                    invalid = True
                # if equal, round it up one to the previous bin
                if ((bins[ind] == other_val_) and (ind > 0)
                        and (val_ < other_val_)):
                    ind -= 1
                    invalid = False
            else:   # extr == 'max'
                # the max value can not be smaller than the crt min value
                if (bins[ind] <= other_val_):
                    invalid = True
                # if equal, round it up one to the next bin
                if ((bins[ind] == other_val_) and (ind < (len(bins)-2))
                        and (val_ > other_val_)):
                    ind += 1
                    invalid = False
        except:
            pass

    str_val = None if invalid else str("{:.2f}".format(bins[ind]))

    return str_val, invalid


@callback(
    Output({'type': 'box_min', 'axis': MATCH}, 'value', allow_duplicate=True),
    Output({'type': 'box_min', 'axis': MATCH}, 'invalid', allow_duplicate=True),
    Input({'type': 'box_min', 'axis': MATCH}, 'value'),
    State({'type': 'box_min', 'axis': MATCH}, 'id'),
    State({'type': 'box_max', 'axis': MATCH}, 'value'),
    State({'type': 'bin_tabs', 'window_nbr': 0}, 'active_tab'),
    State('bin_table', 'data'),
    Input('box_calc', 'n_clicks'),
    #on_error=error_handler,
    prevent_initial_call=True)
def check_box_min(val, val_id, other_val, bin_tab, bin_data, n):
    # 'window_nbr': 0 because if splitted, means that all plots have same mesh dim
    if (val != 'resetting ...'):
        bins = bin_data[int(bin_tab)]['{}_orig_bins'.format(val_id['axis'])]
        bins = decode_bins_from_table(bins)
        bin_step = bin_data[int(bin_tab)]['{}_bin_step'.format(val_id['axis'])]

        return check_box_bound(val, other_val, 'min', bins, bin_step)
    else:

        return None, False


@callback(
    Output({'type': 'box_max', 'axis': MATCH}, 'value', allow_duplicate=True),
    Output({'type': 'box_max', 'axis': MATCH}, 'invalid', allow_duplicate=True),
    Input({'type': 'box_max', 'axis': MATCH}, 'value'),
    State({'type': 'box_max', 'axis': MATCH}, 'id'),
    State({'type': 'box_min', 'axis': MATCH}, 'value'),
    State({'type': 'bin_tabs', 'window_nbr': 0}, 'active_tab'),
    State('bin_table', 'data'),
    Input('box_calc', 'n_clicks'),
    #on_error=error_handler,
    prevent_initial_call=True)
def check_box_max(val, val_id, other_val, bin_tab, bin_data, n):
    # 'window_nbr': 0 because if splitted, means that all plots have same mesh dim
    if (val != 'resetting ...'):
        bins = bin_data[int(bin_tab)]['{}_orig_bins'.format(val_id['axis'])]
        bins = decode_bins_from_table(bins)
        bin_step = bin_data[int(bin_tab)]['{}_bin_step'.format(val_id['axis'])]

        return check_box_bound(val, other_val, 'max', bins, bin_step)
    else:

        return None, False


@callback(
    Output({'type': 'usrbin_fig', 'window_nbr': ALL, 'plot_nbr': ALL}, 'figure',
           allow_duplicate=True),
    Output({'type': 'box_min', 'axis': ALL}, 'value'),
    Output({'type': 'box_min', 'axis': ALL}, 'invalid'),
    Output({'type': 'box_max', 'axis': ALL}, 'value'),
    Output({'type': 'box_max', 'axis': ALL}, 'invalid'),
    Output('box_calc', 'n_clicks'), # to trigger box_calculation
    Input('box_reset', 'n_clicks'),
    State('box_calc', 'n_clicks'),
    State({'type': 'usrbin_fig', 'window_nbr': ALL, 'plot_nbr': ALL},
          'figure'),
    State('plot_size_slider', 'value'),
    State('breakpoints_bin', 'height'),
    State('breakpoints_bin', 'width'),
    State('split_radio', 'value'),
    #on_error=error_handler,
    prevent_initial_call=True)
def on_reset_zoom_click(_, n, crt_figs, size_slider_value, window_height,
                        window_width, split_value):
    patched_figs = []
    for fig in crt_figs:
        patched_fig = Patch()
        patched_fig['layout']['xaxis']['autorange'] = True
        patched_fig['layout']['yaxis']['autorange'] = True
        patched_figs.append(patched_fig)
    values = ['resetting ...']*3
    invalids = [False]*3

    return patched_figs, values, invalids, values, invalids, n



################################################################################
# Initial creation of plot object
################################################################################

@callback(Output({'type': 'cache_store_bin', 'plot_nbr': ALL}, 'data'),
          Input('url_bin', 'href'),
          State('cache_store_bin_container', 'children'),
          background=True,
          manager=background_callback_manager,
          progress=[Output('bin_progress_bar', 'value')],
                    #Output('bin_progress_bar', 'max')],
          prevent_initial_call=True,
          #on_error=load_error_handler,
          )
def query_data_bin(set_progress, href, crt_store_children):
    THREADING = bool(os.getenv("THREADING", 0))
    MULTIPROCESSING = bool(os.getenv("MULTIPROCESSING", 0))
    args = RE_QUERY.findall(href)
    source_suffixes = []
    simu_names = []
    data_sources = []
    particles = []
    type = ''
    ratios = True
    for arg in args:
        key, value = unquote(arg[0]), unquote(arg[1])
        if (key.startswith('dir')):
            data_sources.append(value)
            source_suffixes.append(key[3:])
        if (key.startswith('file')):
            data_sources.append(value)
            source_suffixes.append(key[4:])
        if (key == 'type'):
            type = value
        if (key == 'ratios'):
            ratios = bool(int(value))
        if (key.startswith('particle')):
            particles.append(value)
    for suffix in source_suffixes:
        name_matching_value = ''
        for arg in args:
            key, value = unquote(arg[0]), unquote(arg[1])
            if (key == 'name'+suffix):
                name_matching_value = value
        if (name_matching_value):
            simu_names.append(copy.copy(name_matching_value))
        else:
            simu_names.append(None)

    if (not data_sources):
        error_message: str = ("No directory or file containing the data was "
                               + "provided. Hint: check 'dir' or 'file' "
                               + " parameter in query string.")

        raise Exception(error_message)
    file_simu_names = []
    value_file_paths = []
    abs_err_file_paths = []
    rel_err_file_paths = []
    inp_file_paths = []
    main_bin_ext = '.txt'
    for i, crt_source in enumerate(data_sources):
        if (os.path.isdir(crt_source)):
            list_files = os.listdir(crt_source)
        elif (os.path.isfile(crt_source)):
            list_files = [crt_source]
        else:
            error_message: str = ("The provided directory or file "
                                  + " '{}'".format(crt_source)
                                  + "containing the data was not found")

            raise Exception(error_message)
        # For now, assumed that there is only 1 .txt, 1 .rel.err, and 1 .abs.err
        value_file_path = ''
        for file in list_files:
            if (file.lower().endswith(main_bin_ext)):
                value_file_paths.append(os.path.join(crt_source, file))
                abs_err_file_path, rel_err_file_path, inp_file_path = '', '', ''
                for file_bis in list_files: # look for input and errors files
                    if (file_bis.lower().endswith('.abs.err')
                            and (file_bis[:-8] == file[:-len(main_bin_ext)])):
                        abs_err_file_path = os.path.join(crt_source, file_bis)
                    if (file_bis.lower().endswith('.rel.err')
                            and (file_bis[:-8] == file[:-len(main_bin_ext)])):
                        rel_err_file_path = os.path.join(crt_source, file_bis)
                    if (file_bis.lower().endswith('.inp')
                            and (file_bis[:-8] == file[:-len(main_bin_ext)])):
                        inp_err_file_path = os.path.join(crt_source, file_bis)
                abs_err_file_paths.append(abs_err_file_path)
                rel_err_file_paths.append(rel_err_file_path)
                inp_file_paths.append(inp_file_path)
                file_simu_names.append(simu_names[i])
    base_progress = 0.1
    frac_load = 1.0 - (3*base_progress) # first and last state of bar reserved
    set_progress(str(base_progress))  # update progress bar
    nbr_files = len(value_file_paths)
    weight_copy_plots = 5   # takes less time to create new plots from existing
    # total_steps = #dirs + #arrangements of dirs
    total_steps = nbr_files
    #if ((nbr_files > 1) and (ratios)):
    #    total_steps += (math.factorial(nbr_files)/math.factorial(nbr_files-2)
    #                    /weight_copy_plots)

    usrbin_plots = []
    extracted_plots = [None for _ in range(len(value_file_paths))]
    plot_simu_names = []
    arguments = []
    for i in range(len(value_file_paths)):
        arguments.append((i, extracted_plots, value_file_paths[i],
                         abs_err_file_paths[i], rel_err_file_paths[i],
                         inp_file_paths[i], particles))
    try:
        if (THREADING):
            print('STARTING THREADING')
            threads = []
            for i in range(len(value_file_paths)):
                thread = threading.Thread(target=extract_bins,
                                          args=(arguments[i],))
                threads.append(thread)
                thread.start()
            for i, thread in enumerate(threads):
                thread.join()
                set_progress(str(base_progress+((i+1)/total_steps*frac_load)))
            for i, plots in enumerate(extracted_plots):
                if (plots):
                    usrbin_plots.extend(plots)
                    plot_simu_names.extend([file_simu_names[i]]*len(plots))
        elif (MULTIPROCESSING):
            print('STARTING MULTIPROCESSING')
            with Pool(processes=len(value_file_paths)) as pool:
                extracted_plots = pool.map(extract_bins, arguments)
            zip_extracted_plots = zip([elem[0] for elem in extracted_plots],
                                      [elem[1] for elem in extracted_plots])
            ordered_extracted_plots = [plot for _, plot
                                       in sorted(zip_extracted_plots)]
            for i, plots in enumerate(ordered_extracted_plots):
                if (plots):
                    usrbin_plots.extend(plots)
                    plot_simu_names.extend([file_simu_names[i]]*len(plots))
        else:
            print('STARTING SEQUENTIAL')
            for i in range(len(value_file_paths)):
                extract_bins(arguments[i])
                set_progress(str(base_progress+((i+1)/total_steps*frac_load)))
            for i, plots in enumerate(extracted_plots):
                if (plots):
                    usrbin_plots.extend(plots)
                    plot_simu_names.extend([file_simu_names[i]]*len(plots))
    except Exception as error:

        raise Exception(str(error))

    ############################################################################
    bin_info_table = []
    columns = []
    columns.append(dict(id='plot_name', name=['Plot Information',
                                              'Plot Number'],
                        type='numeric',
                        format=Format(precision=0,
                                      scheme=Scheme.decimal_integer)))
    columns.append(dict(id='simu_name', name=['Plot Information',
                                              'Simulation Name']))
    columns.append(dict(id='particle_name', name=['Plot Information',
                                                 'Type of Particles']))
    columns.append(dict(id='unit', name=['Plot Information', 'Unit']))
    for axis in ['x', 'y', 'z']:
        columns.append(dict(id='{}_orig_bins'.format(axis),
                       name=['Original Bins']))
        first_name = '{}-axis Bins Parameters (cm)'.format(axis.capitalize())
        columns.append(dict(id='{}_bins_min'.format(axis),
                            name=[first_name, 'Min.'],
                            type='numeric',
                            format=Format(precision=2, scheme=Scheme.fixed)))
        columns.append(dict(id='{}_bins_max'.format(axis),
                            name=[first_name, 'Max.'],
                            type='numeric',
                            format=Format(precision=2, scheme=Scheme.fixed)))
        columns.append(dict(id='{}_bin_step'.format(axis),
                            name=[first_name, 'Spacing'],
                            type='numeric',
                            format=Format(precision=2, scheme=Scheme.fixed)))

    # Look for possible plot comparison
    nbr_primary_plots = len(usrbin_plots)
    comp_plot_items = []
    if ((1 < len(usrbin_plots) < MAX_NBR_PLOTS) and ratios):
        indices = [i for i in range(len(usrbin_plots))]
        arrangements = permutations(indices, 2)
        for j, item in enumerate(arrangements):
            #set_progress((str(nbr_files+(j/weight_copy_plots)+1),
            #              str(total_steps)))  # update progress bar
            new_usrbin_plot = usrbin_plots[item[0]] / usrbin_plots[item[1]]
            if (new_usrbin_plot is not None):
                usrbin_plots.append(new_usrbin_plot)
                comp_plot_items.append(item)
    # Limit the number of plots
    if (len(usrbin_plots) >= MAX_NBR_PLOTS):    # >= bcs will not compute ratio
        usrbin_plots = usrbin_plots[:MAX_NBR_PLOTS]
        max_plot_error_handler()
    # Extract data from valid plots and populate dash components
    new_tabs = []
    tab_children = []
    scale_u_trig_children = []
    scale_s_trig_children = []
    clear_trig_children = []
    box_c_trig_children = []
    depth_trig_children = []
    size_trig_children = []
    box_d_trig_children = []
    box_z_trig_children = []
    map_c_trig_children = []
    fix_r_trig_children = []
    color_trig_children = []
    fig_a_trig_children = []
    are_plots_comparable = '1'  # False in str -> later cast as int('0')
    for j in range(MAX_NBR_WINDOWS):
        new_tabs.append([])
        scale_u_trig_children.append([])
        scale_s_trig_children.append([])
        clear_trig_children.append([])
        box_c_trig_children.append([])
        depth_trig_children.append([])
        size_trig_children.append([])
        box_d_trig_children.append([])
        box_z_trig_children.append([])
        map_c_trig_children.append([])
        fix_r_trig_children.append([])
        color_trig_children.append([])
        fig_a_trig_children.append([])
        for i, usrbin_plot in enumerate(usrbin_plots):
            bin_info = {}
            if (i < nbr_primary_plots):
                label = 'Plot '+ str(i+1)
                bin_info['plot_name'] = str(i+1)
                bin_info['simu_name'] = str(plot_simu_names[i])
            else:
                item = comp_plot_items[i-nbr_primary_plots]
                label = ('Plot ' + str(item[0]+1) + ' / ' + 'Plot '
                         + str(item[1]+1))
                bin_info['plot_name'] = (str(item[0]+1) + ' / '
                                         + str(item[1]+1))
                bin_info['simu_name'] = (str(plot_simu_names[item[0]])
                                         + ' / '
                                         + str(plot_simu_names[item[1]]))
            bin_info['particle_name'] = usrbin_plot.particle_name
            bin_info['unit'] = str_tag_html_to_unicode(usrbin_plot.unit)
            for axis in ['x', 'y', 'z']:
                bins, bin_step = usrbin_plot.get_original_bins(axis)
                if (i):
                    prev_bins, _ = usrbin_plots[i-1].get_original_bins(axis)
                    if (not np.array_equal(bins, prev_bins)):
                        are_plots_comparable = '0'
                bin_info[axis+'_orig_bins'] = encode_bins_for_table(bins)
                bin_info[axis+'_bins_min'] = bins[0]
                bin_info[axis+'_bins_max'] = bins[-1]
                bin_info[axis+'_bin_step'] = bin_step
            bin_info_table.append(bin_info)
            new_tabs[-1].append(dbc.Tab(label=label, tab_id=str(i),
                                        id={'type': 'bin_tab', 'window_nbr': j,
                                            'plot_nbr': i}))
            # Adding fig for each plot
            usrbin_fig = html.Div(dcc.Graph(figure=LOADING_FIGURE,
                                            id={'type': 'usrbin_fig',
                                                'window_nbr': j, 'plot_nbr': i},
                                  ))#responsive=True),
            usrbin_fig = dcc.Loading(usrbin_fig,
                                     id={'type': 'usrbin_fig_loading',
                                         'window_nbr': j, 'plot_nbr': i},
                                     delay_hide=100,
                                     )#overlay_style={"visibility": "visible",
                                    #                })#"filter": "blur(5px)"})
            # Adding statistic display
            stat_header = html.Strong('Box Statistics: ')
            stat_mean = dbc.Stack([html.Label('Mean Value: '),
                                   html.Label('-', id={'type': 'stat_mean',
                                                       'window_nbr': j,
                                                       'plot_nbr': i})],
                                  gap=3, direction='horizontal')
            stat_extr = dbc.Stack([html.Label('Minimum: '),
                                   html.Label('-', id={'type': 'stat_min',
                                                       'window_nbr': j,
                                                       'plot_nbr': i}),
                                   html.Label('/  Maximum: '),
                                   html.Label('-', id={'type': 'stat_max',
                                                       'window_nbr': j,
                                                       'plot_nbr': i})],
                                  gap=3, direction='horizontal')
            stat_err = dbc.Stack([html.Label('Mean Absolute Error: '),
                                  html.Label('-', id={'type': 'stat_abs_err',
                                                      'window_nbr': j,
                                                      'plot_nbr': i}),
                                  html.Label('/  Mean Relative Error: '),
                                  html.Label('-', id={'type': 'stat_rel_err',
                                                      'window_nbr': j,
                                                      'plot_nbr': i})],
                                 gap=3, direction='horizontal')
            stat_display = dbc.ListGroup([dbc.ListGroupItem(stat_mean),
                                          dbc.ListGroupItem(stat_extr),
                                          dbc.ListGroupItem(stat_err)],
                                         flush=True)
            stat_display = dcc.Loading(stat_display)
            # Create slider to control the range of the scale value
            scale_slider = dcc.RangeSlider(min=0, max=0, allowCross=False,
                                           tooltip=dict(),
                                           updatemode='mouseup',
                                           id={'type': 'scale_slider',
                                               'window_nbr': j, 'plot_nbr': i})
            scale_slider_div = html.Div('', id={'type': 'scale_slider_header',
                                                'window_nbr': j, 'plot_nbr': i})
            scale_slider_header = dbc.Stack([html.Strong('Scale Boundaries: '),
                                             scale_slider_div,
                                            ], direction='horizontal', gap=3)
            tab_children.append([html.Br(), usrbin_fig,
                                 html.Br(), scale_slider_header, scale_slider,
                                 html.Br(), stat_header, stat_display,
                                ])
            scale_u_trig_children[-1].append(html.Div(id={'type': 'scale_u_trig',
                                                          'window_nbr': j,
                                                          'plot_nbr': i}))
            scale_s_trig_children[-1].append(html.Div(id={'type': 'scale_s_trig',
                                                          'window_nbr': j,
                                                          'plot_nbr': i}))
            clear_trig_children[-1].append(html.Div(id={'type': 'clear_trig',
                                                        'window_nbr': j,
                                                        'plot_nbr': i}))
            box_c_trig_children[-1].append(html.Div(id={'type': 'box_c_trig',
                                                        'window_nbr': j,
                                                        'plot_nbr': i}))
            depth_trig_children[-1].append(html.Div(id={'type': 'depth_trig',
                                                        'window_nbr': j,
                                                        'plot_nbr': i}))
            size_trig_children[-1].append(html.Div(id={'type': 'size_trig',
                                                       'window_nbr': j,
                                                       'plot_nbr': i}))
            box_d_trig_children[-1].append(html.Div(id={'type': 'box_d_trig',
                                                        'window_nbr': j,
                                                        'plot_nbr': i}))
            box_z_trig_children[-1].append(html.Div(id={'type': 'box_z_trig',
                                                        'window_nbr': j,
                                                        'plot_nbr': i}))
            map_c_trig_children[-1].append(html.Div(id={'type': 'map_c_trig',
                                                        'window_nbr': j,
                                                        'plot_nbr': i}))
            fix_r_trig_children[-1].append(html.Div(id={'type': 'fix_r_trig',
                                                        'window_nbr': j,
                                                        'plot_nbr': i}))
            color_trig_children[-1].append(html.Div(id={'type': 'color_trig',
                                                        'window_nbr': j,
                                                        'plot_nbr': i}))
            fig_a_trig_children[-1].append(html.Div(id={'type': 'fig_a_trig',
                                                        'window_nbr': j,
                                                        'plot_nbr': i}))

    # temp -------
    if (type == 'expert'):
        expert_button_value = 0
        expert_row_type = {'display': 'block'}
    elif (type == 'user'):
        expert_button_value = 1
        expert_row_type = {'display': 'none'}
    else:
        #error_message: str = ("Unknow type of users specified.")
        #raise Exception(error_message)
        # return the base version, i.e. user version
        expert_button_value = 1
        expert_row_type = {'display': 'none'}

    # Work around of dash:
    # - only keep the store in the store_div that are unnecessary
    # - temporarly save all outputs in a store
    # - trigger first callback "split_data_into_stores" to divide plots into
    #   different stores
    # - trigeer second callback "trigger_init_callbacks" to start updating
    #   other elements of the app

    res = [no_update for _ in range(MAX_NBR_PLOTS)]
    for i in range(len(usrbin_plots)):
        res[i] = Serverside(usrbin_plots[i])

    res.append(Serverside([[scale_u_trig_children, scale_s_trig_children,
                            clear_trig_children, box_c_trig_children,
                            depth_trig_children, size_trig_children,
                            box_d_trig_children, box_z_trig_children,
                            map_c_trig_children, fix_r_trig_children,
                            color_trig_children, fig_a_trig_children],
                            new_tabs, len(usrbin_plots),
                            [tab_children, bin_info_table, columns,
                             expert_button_value, expert_row_type,
                             are_plots_comparable]]))

    return res


@callback(
    Output({'type': 'scale_u_trigs', 'window_nbr': ALL}, 'children'),
    Output({'type': 'scale_s_trigs', 'window_nbr': ALL}, 'children'),
    Output({'type': 'clear_trigs', 'window_nbr': ALL}, 'children'),
    Output({'type': 'box_c_trigs', 'window_nbr': ALL}, 'children'),
    Output({'type': 'depth_trigs', 'window_nbr': ALL}, 'children'),
    Output({'type': 'size_trigs', 'window_nbr': ALL}, 'children'),
    Output({'type': 'box_d_trigs', 'window_nbr': ALL}, 'children'),
    Output({'type': 'box_z_trigs', 'window_nbr': ALL}, 'children'),
    Output({'type': 'map_c_trigs', 'window_nbr': ALL}, 'children'),
    Output({'type': 'fix_r_trigs', 'window_nbr': ALL}, 'children'),
    Output({'type': 'color_trigs', 'window_nbr': ALL}, 'children'),
    Output({'type': 'fig_a_trigs', 'window_nbr': ALL}, 'children'),
    Output({'type': 'bin_tabs', 'window_nbr': ALL}, 'children'),
    Output('cache_store_bin_container', 'children'),
    Output('temp_store_init_value_transit', 'data'),
    #Output('trigger_init_callback', 'children'),    # force to occur afterward
    Input({'type': 'cache_store_bin', 'plot_nbr': MAX_NBR_PLOTS}, 'data'),
    State('cache_store_bin_container', 'children'),
    prevent_initial_call=True,
    #on_error=error_handler
)
def populate(temp_data, crt_store_children):
    # temp_data[0] are the trigger variables
    # temp_data[1] are the tabs
    # temp_data[2] are the number of plots
    # temp_data[3] are the init variables (must be set after the tabs), so first
    # temporarily store in a dcc.Store which triggers another callabck

    set_props('bin_progress_bar', dict(value='0.8'))

    return (*temp_data[0], temp_data[1], crt_store_children[:temp_data[2]],
            temp_data[3])


@callback(
    Output({'type': 'bin_tab', 'window_nbr': ALL, 'plot_nbr': ALL}, 'children'),
    Output('bin_table', 'data'),
    Output('bin_table', 'columns'),
    Output('expert_bin_button', 'n_clicks'),
    Output('expert_bin_row_div', 'style'),
    Output('are_plots_comparable', 'children'),
    Output('options_hid_btn', 'n_clicks'),
    Output('table_hid_btn', 'n_clicks'),
    Output('split_radio', 'value'),     # active_tab of bin_tabs set in callback
    #Input('trigger_init_callback', 'children'), # force to happen in that order
    Input('temp_store_init_value_transit', 'data'),
    prevent_initial_call=True,
    #on_error=error_handler
)
def trigger_init_callbacks(init_data):
    set_props('bin_progress_bar', dict(value='0.9'))

    return (*init_data, 1, 1, 1)


################################################################################
# Callbacks (old , should do that again)
# ------------------------------------------------------------------------------
# > Figure Initiation Callabck Logic:
#   'url_bin' -> 'cache_store_bin'
# > Figure Update Callback Logic:
#   'cache_store_bin', 'plot_radio', 'colli_input' -> 'scale_slider'
#   'cache_store_bin', 'proj_dropdown' -> depth_slider
#   'scale_slider', 'depth_slider', 'fixe_ratio' -> 'usrbin_fig'
################################################################################


@callback(
    Output('expert_bin_button', 'children'),
    Output('expert_bin_button', 'color'),
    Output('control_2d_map_div', 'style'),
    Input('expert_bin_button', 'n_clicks'),
    prevent_initial_call=True,
    #on_error=error_handler
)
def on_expert_button_click(n):
    if (n is not None):
        if (n%2):   # first click

            return 'Show Expert View', 'success', {'display': 'none'}
        else:   # initial no click (n=0)

            return 'Show User View', 'primary', {'display': 'block'}

@callback(
    Output('table_hid_btn', 'children'),
    Output('table_hid_btn', 'color'),
    Output('bin_table_div', 'style'),
    Input('table_hid_btn', 'n_clicks'),
    prevent_initial_call=True,
    #on_error=error_handler
)
def on_table_hid_btn_click(n):
    if (n is not None):
        if (n%2):   # first click

            return 'Show Plot Information', 'success', {'display': 'none'}
        else:   # initial no click (n=0)

            return 'Hide Plot Information', 'primary', {'display': 'block'}

@callback(
    Output('options_hid_btn', 'children'),
    Output('options_hid_btn', 'color'),
    Output('bin_options_div', 'style'),
    Input('options_hid_btn', 'n_clicks'),
    prevent_initial_call=True,
    #on_error=error_handler
)
def on_table_hid_btn_click(n):
    if (n is not None):
        if (n%2):   # first click

            return 'Show Plot Settings', 'success', {'display': 'none'}
        else:   # initial no click (n=0)

            return 'Hide Plot Settings', 'primary', {'display': 'block'}

@callback(
    Output({'type': 'averaging', 'axis': MATCH}, 'options'),
    Output({'type': 'averaging', 'axis': MATCH}, 'value'),
    State({'type': 'averaging', 'axis': MATCH}, 'id'),
    Input({'type': 'bin_tabs', 'window_nbr': 0}, 'active_tab'),
    State('bin_table', 'data'),
    State({'type': 'averaging', 'axis': MATCH}, 'options'),
    prevent_initial_call=True,
    #on_error=error_handler
    )
def update_axis_averaging(avg_id, bin_tab, bin_data, crt_options):
    # bin_tabs[0] because either 1 plot on page ([0]), or multiple which are
    # of the same mesh dimensions than the [0] plot.
    bins = bin_data[int(bin_tab)]['{}_orig_bins'.format(avg_id['axis'])]
    bins = decode_bins_from_table(bins)
    bin_step = bin_data[int(bin_tab)]['{}_bin_step'.format(avg_id['axis'])]
    divisors_n = divisors(len(bins)-1)  # #bins = len(bins)-1
    options = {str(n): str(bin_step*n) + ' cm' for n in divisors_n}
    # '1' is always one of the divisors
    if (crt_options == options):
        # This case occurs if there is more than 1 plot, and the bin tab of the
        # first plot is changed. If more than 1 plot, all plot have same mesh
        # dimensions, axis averaging should not changed

        raise PreventUpdate
    else:
        # This case will not occur if multiple plots of the same dimensions,
        # bcs if options are different means that the mesh size is different
        # and so that the splitting of plots has been prevented

        return options, '1'


# all other irradiation settings changed the #collisions and the rest is same
def calc_collision_and_luminosity_from_year(*years):
    total_colli = 0.
    total_lumi = 0.
    for year in years:
        if (DELIVERED_LUMI.get(year) is None):
            # should not happen, because set with DELIVERED_LUMI
            raise Exception()
        lumi_data = DELIVERED_LUMI[year]
        colli = 0.
        lumi = 0.
        for elem in lumi_data:
            if (elem[2] == 'pp'):
                if (PP_CROSS_SECTIONS.get(elem[1]) is None):

                    raise Exception()
                cross_sec = PP_CROSS_SECTIONS[elem[1]]
                colli += cross_sec*elem[0]*1e12  # mb*fb^{-1} = 1e-12
                lumi += elem[0]
        total_colli += colli
        total_lumi += lumi

    return (total_colli, total_lumi)

@callback(
    Output('colli_input', 'value', allow_duplicate=True),
    Output('year_lum_div', 'children'),
    Input('year_lum_dropdown', 'value'),
    prevent_initial_call=True,
    #on_error=error_handler
    )
def on_year_lum_dropdown_change(year):
    years = []
    for key in list(DELIVERED_LUMI.keys()):
        if (key <= year):
            years.append(key)
    total_colli, total_lumi = calc_collision_and_luminosity_from_year(*years)
    new_year_lum_div = ("Integrated Luminosity: {:.4f} fb".format(total_lumi)
                        + "\u207B" + "\u00B9")
    if (total_colli):

        return ("{:.4e}".format(total_colli), new_year_lum_div)
    else:

        return ("1.0", new_year_lum_div)


@callback(
    Output('colli_input', 'value', allow_duplicate=True),
    Output('year_run_div', 'children'),
    Input('year_run_dropdown', 'value'),
    prevent_initial_call=True,
    #on_error=error_handler
    )
def on_year_run_dropdown_change(run):
    if (RUN_DEF.get(run) is None):
        # should not happen, because set with DELIVERED_LUMI
        raise PreventUpdate
    years = RUN_DEF[run]
    total_colli, total_lumi = calc_collision_and_luminosity_from_year(*years)
    new_year_run_div = ("Integrated Luminosity: {:.4f} fb".format(total_lumi)
                        + "\u207B" + "\u00B9")

    if (total_colli):

        return ("{:.4e}".format(total_colli), new_year_run_div)
    else:

        return ("1.0", new_year_run_div)


@callback(
    Output('colli_input', 'value'),
    Output('lumi_input', 'value'),
    Output('lumi_input', 'placeholder'),
    Output('energy_lum_dropdown', 'placeholder'),
    Input('custom_lumi_button', 'n_clicks'),
    State('energy_lum_dropdown', 'value'),
    State('lumi_input', 'value'),
    prevent_initial_call=True,
    #on_error=error_handler
    )
def on_custom_lumi_change(n, energy, lumi):
    if (energy is None):

        return no_update, no_update, no_update, 'NON VALID'
    try:
        lumi_float = float(lumi)
    except:

        return no_update, '', 'NON VALID', no_update

    total_colli = PP_CROSS_SECTIONS[energy]*lumi_float*1e12 # mb*fb^{-1} = 1e-12

    return "{:.4e}".format(total_colli), lumi_float, '', ''


@callback(
    Output({'type': 'bin_tabs_store', 'window_nbr': MATCH}, 'data'),
    #Output({'type': 'clear_trig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
    #       'children', allow_duplicate=True),
    Input({'type': 'bin_tabs', 'window_nbr': MATCH}, 'active_tab'),
    State({'type': 'bin_tabs_store', 'window_nbr': MATCH}, 'data'),
    prevent_initial_call=True,
    #on_error=error_handler,
)
def store_bin_tabs_values(bin_tab, bin_tab_store):

    if (bin_tab_store is None):
        new_values = []
    else:
        new_values = bin_tab_store
        if (bin_tab in new_values): # will be appended at the end as a stack
            new_values.remove(bin_tab)
        # max 3 plots in memory for now (and all different)
        new_values = new_values[:1]
    new_values.append(bin_tab)

    return new_values


@callback(
    Output({'type': 'scale_u_trig', 'window_nbr': MATCH, 'plot_nbr': ALL},
           'children', allow_duplicate=True),
    Output({'type': 'box_c_trig', 'window_nbr': MATCH, 'plot_nbr': ALL},
           'children', allow_duplicate=True),
    Input('colli_input', 'value'),
    Input({'type': 'plot_tabs', 'window_nbr': MATCH}, 'active_tab'),
    Input({'type': 'bin_tabs', 'window_nbr': MATCH}, 'active_tab'),
    State('split_radio', 'value'),
    prevent_initial_call=True,
    #on_error=error_handler
)
def callback_to_trigger_update_scale_slider(_, __, bin_tab, split_value):
    # In order for the input to trigger the callback, all the MATCH in the
    # input must also be in the output.
    # As this is not the case for 'plot_tabs' and 'bin_tabs', this callback
    # uses dummy Div() components to trigger only the plot in the current
    # plot tab and the current bin tab
    # It was also done for the input 'colli_input' to trigger only the
    # displayed graph
    window_nbr = ctx.outputs_list[0][0]['id']['window_nbr']
    #window_nbr = ctx.outputs_list[0]['id']['window_nbr']

    res = trigger_active_tabs(ctx.outputs_list[0], split_value, bin_tab)

    return res, res


@callback(
    Output({'type': 'scale_slider', 'window_nbr': MATCH, 'plot_nbr': MATCH},
           'min'),
    Output({'type': 'scale_slider', 'window_nbr': MATCH, 'plot_nbr': MATCH},
           'max'),
    Output({'type': 'scale_slider', 'window_nbr': MATCH, 'plot_nbr': MATCH},
           'step'),
    Output({'type': 'scale_slider', 'window_nbr': MATCH, 'plot_nbr': MATCH},
           'marks'),
    Output({'type': 'scale_slider', 'window_nbr': MATCH, 'plot_nbr': MATCH},
           'tooltip'),
    Output({'type': 'scale_slider', 'window_nbr': MATCH, 'plot_nbr': MATCH},
           'value'),
    Input({'type': 'scale_u_trig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
          'children'),
    State('colli_input', 'value'),
    State('split_radio', 'value'),
    State({'type': 'plot_tabs', 'window_nbr': MATCH}, 'active_tab'),
    State({'type': 'cache_store_bin', 'plot_nbr': MATCH}, 'data'),
    State({'type': 'averaging', 'axis': ALL}, 'value'),
    prevent_initial_call=True,
    #on_error=error_handler
    )
def update_scale_slider(_, nbr_collisions, split_value, vplot, crt_plot,
                        avg_values):
    abs_err, rel_err = PLOT_VALUES_TO_ERR_FLAGS[vplot]
    flt_nbr_coll = float(nbr_collisions)
    tooltip={'placement': 'bottom'}
    min_val_, max_val_ = np.inf, -np.inf
    if (None in avg_values):
        avg_values = [1, 1, 1]
    else:
        avg_values = [int(elem) for elem in avg_values]
    if (not rel_err):
        if (not abs_err):
            min_val, max_val = crt_plot.get_log10_extrema(avg_values,
                                                          flt_nbr_coll)
            _, abs_max_val = crt_plot.get_extrema(avg_values)
            percentile_min = crt_plot.get_log10_percentile(1, avg_values,
                                                           flt_nbr_coll)
            percentile_max = crt_plot.get_log10_percentile(99, avg_values,
                                                           flt_nbr_coll)
        else:
            min_val, max_val = crt_plot.get_log10_abs_err_extrema(avg_values,
                                                                  flt_nbr_coll)
            _, abs_max_val = crt_plot.get_abs_err_extrema(avg_values)
            percentile_min = crt_plot.get_log10_abs_err_percentile(1,
                                                                   avg_values,
                                                                   flt_nbr_coll)
            percentile_max = crt_plot.get_log10_abs_err_percentile(99,
                                                                   avg_values,
                                                                   flt_nbr_coll)
        min_val, max_val = int(math.floor(min_val)), int(math.ceil(max_val))
        percentile_min = int(math.floor(percentile_min))
        percentile_max = int(math.ceil(percentile_max))

        # look for ratio plot with values < 1
        if (False):#crt_plot.data_ratio and (abs_max_val < 1)):
            scale_slider_marks = {0: '0.0',  0.5: '0.5', 1: '1.0'}
            tooltip['template'] = '{value}'

            return (0, 1, 0.01, scale_slider_marks, tooltip,
                    [0., percentile_max])
        else:
            scale_slider_marks = dict()
            val_step = max((max_val+1-min_val)//5, 1)   # must be at least 1
            for val in range(min_val, max_val+1, val_step):
                scale_slider_marks[val] = "1e{}".format(str(val))
            tooltip['template'] = '1e{value}'

        return (min_val, max_val, 1, scale_slider_marks, tooltip,
                [percentile_min, percentile_max])
    else:
        scale_slider_marks = {0: '0', 25: '25', 50: '50', 75: '75', 100: '100'}
        tooltip['template'] = '{value}%'
        rel_percentile = crt_plot.get_rel_err_percentile(99, avg_values)
        percentile_max = math.ceil(100*rel_percentile)

        return (0, 100, 1, scale_slider_marks, tooltip, [0., percentile_max])



@callback(
    Output('depth_slider', 'min'),
    Output('depth_slider', 'max'),
    Output('depth_slider', 'step'),
    Output('depth_slider', 'marks'),
    Output('depth_slider', 'value'),
    Output('depth_slider_label', 'children'),
    Input({'type': 'averaging', 'axis': ALL}, 'value'),
    Input('proj_dropdown', 'value'),
    State({'type': 'bin_tabs', 'window_nbr': 0}, 'active_tab'),
    State('bin_table', 'data'),
    prevent_initial_call=True,
    #on_error=error_handler
    )
def update_depth_slider(avg_value, projection, bin_tab, bin_data):
    proj_ind = ord(projection)-ord('x')
    bins = bin_data[int(bin_tab)]['{}_orig_bins'.format(projection)]
    bins = decode_bins_from_table(bins)
    bins = UsrbinPlot.reduce_bins(bins, int(avg_value[proj_ind]))
    #bins, bin_step = usrbin_plot.get_bins(projection, int(avg_value[proj_ind]))
    depth_slider_marks = {}
    #marks = [bins[i] for i in range(0, len(bins), max(1, len(bins)//5))]
    marks = [i for i in range(0, len(bins), max(1, len(bins)//5))]
    # N.B.: known dash issues not showing labels when float in slider
    # see issue #159 in plotly git, temp. fix add a tiny epsilon to orig. value
    for mark in marks:
        #if (i < (len(marks)-1)):
        #    key = float(mark)+1e-9
        #else:
        #    key = float(mark)-1e-9
        #depth_slider_marks[key] = {'label': '{:.1f}'.format(mark)}
        depth_slider_marks[mark] = {'label': '{:.1f}'.format(bins[mark])}

    new_proj_label = 'Slice Axis Depth ({}):'.format(projection.capitalize())
    bin_value = (len(bins)-1)//2

    return (0, len(bins)-2, 1, depth_slider_marks, bin_value, new_proj_label)


@callback(
    Output('box_calc', 'disabled', allow_duplicate=True),
    Input('box_calc', 'n_clicks'),
    Input('box_calc', 'outline'),
    prevent_initial_call=True,
    #on_error=error_handler,
    )
def disable_box_calc(n, o):

    return True


@callback(
    Output('box_calc', 'disabled', allow_duplicate=True),
    Input({'type': 'usrbin_fig', 'window_nbr': ALL, 'plot_nbr': ALL}, 'figure'),
    prevent_initial_call=True,
    #on_error=error_handler,
    )
def enable_box_calc(figs):

    return False


@callback(
    Output({'type': 'usrbin_fig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
            'figure', allow_duplicate=True),
    Output({'type': 'fig_a_trig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
            'figure', allow_duplicate=True),
    Input({'type': 'box_z_trig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
          'children'),
    State({'type': 'box_min', 'axis': ALL}, 'value'),
    State({'type': 'box_max', 'axis': ALL}, 'value'),
    State('proj_dropdown', 'value'),
    State('box_display', 'value'),
    State({'type': 'usrbin_fig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
          'figure'),
    State('fix_ratio', 'value'),
    State('plot_size_slider', 'value'),
    State('breakpoints_bin', 'height'),
    State('breakpoints_bin', 'width'),
    State('split_radio', 'value'),
    prevent_initial_call=True,
    #on_error=error_handler
    )
def box_zoom(n, min_vals, max_vals, projection, box_disp_val,
             crt_fig, fix_ratio_value, size_slider_value, window_height,
             window_width, split_value):
    # input entry == None if never filled, == '' if clicked but not filled
    # if input entry valid, should be a float (handle in each entry callback)
    patched_fig = Patch()
    try:
        x_min, y_min, z_min = min_vals[0], min_vals[1], min_vals[2]
        x_max, y_max, z_max = max_vals[0], max_vals[1], max_vals[2]
        # Check if all entries are float and can be processed
        args = [float(x_min), float(x_max), float(y_min), float(y_max),
                float(z_min), float(z_max)]
        # Zoom to box if possible
        if (projection.lower()=='x'):
            xs = [float(z_min), float(z_max)]
            ys = [float(y_min), float(y_max)]
        elif (projection.lower()=='y'):
            xs = [float(z_min), float(z_max)]
            ys = [float(x_min), float(x_max)]
        else:
            xs = [float(x_min), float(x_max)]
            ys = [float(y_min), float(y_max)]
        patched_fig['layout']['xaxis']['autorange'] = False
        patched_fig['layout']['xaxis']['range'] = xs
        patched_fig['layout']['yaxis']['autorange'] = False
        patched_fig['layout']['yaxis']['range'] = ys
        # Reposition the colorbar
        offset = get_colorbar_x_axis_pos(xs[0], xs[1], ys[0], ys[1],
                                         window_height, window_width,
                                         size_slider_value, WINDOW_DIVIDER,
                                         (split_value>1))
        patched_fig['data'][0]['colorbar']['x'] = offset

    except:

        raise PreventUpdate


    return patched_fig, None



def trigger_active_tabs(outputs_list, split_value, bin_tab):
    res = [no_update for _ in range(len(outputs_list))]
    if (int(bin_tab) >= 0):  # '-1' before fist init
        window_nbr = outputs_list[0]['id']['window_nbr']
        if (window_nbr < split_value):  # 1 -> 0; 2 -> 0, 1; 4 -> 0, 1, 2, 3
            res[int(bin_tab)] = None

    return res



@callback(
    Output({'type': 'depth_trig', 'window_nbr': MATCH, 'plot_nbr': ALL},
           'children', allow_duplicate=True),
    Input('depth_slider', 'value'),
    State({'type': 'bin_tabs', 'window_nbr': MATCH}, 'active_tab'),
    State('split_radio', 'value'),
    prevent_initial_call=True,
    #on_error=error_handler
)
def filter_depth_slider_trigger(_, bin_tab, split_value):

    return trigger_active_tabs(ctx.outputs_list, split_value, bin_tab)


@callback(
    Output({'type': 'size_trig', 'window_nbr': MATCH, 'plot_nbr': ALL},
           'children', allow_duplicate=True),
    Output({'type': 'fig_a_trig', 'window_nbr': MATCH, 'plot_nbr': ALL},
           'children', allow_duplicate=True),
    Input('plot_size_slider', 'value'),
    State({'type': 'bin_tabs', 'window_nbr': MATCH}, 'active_tab'),
    State('split_radio', 'value'),
    prevent_initial_call=True,
    #on_error=error_handler
)
def filter_plot_size_trigger(_, bin_tab, split_value):
    res = trigger_active_tabs(ctx.outputs_list[0], split_value, bin_tab)

    return res, res


@callback(
    Output({'type': 'box_c_trig', 'window_nbr': MATCH, 'plot_nbr': ALL},
           'children', allow_duplicate=True),
    Input('box_calc', 'n_clicks'),
    State({'type': 'bin_tabs', 'window_nbr': MATCH}, 'active_tab'),
    State('split_radio', 'value'),
    prevent_initial_call=True,
    #on_error=error_handler
)
def filter_box_calc_trigger(_, bin_tab, split_value):

    return trigger_active_tabs(ctx.outputs_list, split_value, bin_tab)


@callback(
    Output({'type': 'box_d_trig', 'window_nbr': MATCH, 'plot_nbr': ALL},
           'children', allow_duplicate=True),
    Input('box_display', 'value'),
    State({'type': 'bin_tabs', 'window_nbr': MATCH}, 'active_tab'),
    State('split_radio', 'value'),
    prevent_initial_call=True,
    #on_error=error_handler
)
def filter_box_display_trigger(_, bin_tab, split_value):

    return trigger_active_tabs(ctx.outputs_list, split_value, bin_tab)


@callback(
    Output({'type': 'box_z_trig', 'window_nbr': MATCH, 'plot_nbr': ALL},
           'children', allow_duplicate=True),
    Input('box_zoom', 'n_clicks'),
    State({'type': 'bin_tabs', 'window_nbr': MATCH}, 'active_tab'),
    State('split_radio', 'value'),
    prevent_initial_call=True,
    #on_error=error_handler
)
def filter_box_zoom_trigger(_, bin_tab, split_value):

    return trigger_active_tabs(ctx.outputs_list, split_value, bin_tab)


@callback(
    Output({'type': 'map_c_trig', 'window_nbr': MATCH, 'plot_nbr': ALL},
           'children', allow_duplicate=True),
    Input('map_calc', 'n_clicks'),
    State({'type': 'bin_tabs', 'window_nbr': MATCH}, 'active_tab'),
    State('split_radio', 'value'),
    prevent_initial_call=True,
    #on_error=error_handler
)
def filter_map_calc_trigger(_, bin_tab, split_value):

    return trigger_active_tabs(ctx.outputs_list, split_value, bin_tab)


@callback(
    Output({'type': 'fix_r_trig', 'window_nbr': MATCH, 'plot_nbr': ALL},
           'children', allow_duplicate=True),
    Input('fix_ratio', 'value'),
    State({'type': 'bin_tabs', 'window_nbr': MATCH}, 'active_tab'),
    State('split_radio', 'value'),
    prevent_initial_call=True,
    #on_error=error_handler
)
def filter_fix_ratio_trigger(_, bin_tab, split_value):

    return trigger_active_tabs(ctx.outputs_list, split_value, bin_tab)


@callback(
    Output({'type': 'color_trig', 'window_nbr': MATCH, 'plot_nbr': ALL},
           'children', allow_duplicate=True),
    Input('bin_color_dropdown', 'value'),
    State({'type': 'bin_tabs', 'window_nbr': MATCH}, 'active_tab'),
    State('split_radio', 'value'),
    prevent_initial_call=True,
    #on_error=error_handler
)
def filter_color_dropdown_trigger(_, bin_tab, split_value):

    return trigger_active_tabs(ctx.outputs_list, split_value, bin_tab)


@callback(
    Output({'type': 'scale_s_trig', 'window_nbr': MATCH, 'plot_nbr': ALL},
           'children', allow_duplicate=True),
    Input({'type': 'scale_slider', 'window_nbr': MATCH, 'plot_nbr': ALL},
           'value'),
    State({'type': 'bin_tabs', 'window_nbr': MATCH}, 'active_tab'),
    State('split_radio', 'value'),
    prevent_initial_call=True,
    #on_error=error_handler
)
def filter_scale_slider_trigger(_, bin_tab, split_value):

    return trigger_active_tabs(ctx.outputs_list, split_value, bin_tab)


@callback(
    Output({'type': 'stat_mean', 'window_nbr': MATCH, 'plot_nbr': MATCH},
           'children'),
    Output({'type': 'stat_min', 'window_nbr': MATCH, 'plot_nbr': MATCH},
           'children'),
    Output({'type': 'stat_max', 'window_nbr': MATCH, 'plot_nbr': MATCH},
           'children'),
    Output({'type': 'stat_abs_err', 'window_nbr': MATCH, 'plot_nbr': MATCH},
           'children'),
    Output({'type': 'stat_rel_err', 'window_nbr': MATCH, 'plot_nbr': MATCH},
           'children'),
    Output({'type': 'box_d_trig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
           'children', allow_duplicate=True),   # only if trigger id box_c_trig
    Input({'type': 'box_c_trig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
          'children'),
    Input({'type': 'depth_trig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
          'children'),
    State({'type': 'box_min', 'axis': ALL}, 'value'),
    State({'type': 'box_max', 'axis': ALL}, 'value'),
    State({'type': 'cache_store_bin', 'plot_nbr': MATCH}, 'data'),
    State({'type': 'bin_tabs', 'window_nbr': MATCH}, 'active_tab'),
    State('colli_input', 'value'),
    State('proj_dropdown', 'value'),
    State('box_display', 'value'),
    prevent_initial_call=True,
    #on_error=error_handler
    )
def box_calculation(_, __, min_vals, max_vals, crt_plot, bin_tab,
                    nbr_collisions, projection, box_display):
    # input entry == None if never filled, == '' if clicked but not filled
    # if input entry valid, should be a float (handle in each entry callback)
    flt_nbr_coll = float(nbr_collisions)
    trigger_box_d = no_update
    try:
        x_min, y_min, z_min = min_vals[0], min_vals[1], min_vals[2]
        x_max, y_max, z_max = max_vals[0], max_vals[1], max_vals[2]
        # Check if all entries are float and can be processed
        args = [float(x_min), float(x_max), float(y_min), float(y_max),
                float(z_min), float(z_max)]
        outputs = []
        mean, abserr, relerr, vmin, vmax = crt_plot.get_box_statistics(*args,
                                                 multiplicator=flt_nbr_coll)
        # Change information if compute statistic requested otherwise display
        unit = str_tag_html_to_dash(crt_plot.unit)
        def to_div(v):

            return html.Div(["{:.2e}".format(v) + ' '] + unit)
        outputs = [to_div(mean), to_div(vmin), to_div(vmax),
                   to_div(abserr), "{:.2f}".format(relerr) + ' %']
        # If there is valid new box detected, trigger its display
        if (ctx.triggered_id['type'] == 'box_c_trig'):
            trigger_box_d = None
    except:
        outputs = ['-' for _ in range(5)]

    return (*outputs, trigger_box_d)


@callback(
    Output('map_calc', 'disabled', allow_duplicate=True),
    Input('map_calc', 'n_clicks'),
    prevent_initial_call=True,
    #on_error=error_handler,
    )
def update_map_calc_state(map_calc_click):

    return True


@callback(
    Output('split_radio_store', 'data'),
    Input('split_radio', 'value'),
    prevent_initial_call=True,
    #on_error=error_handler,
)
def store_split_radio_values(split_value):
    # Only save the last value

    return split_value


@callback(
    Output({'type': 'bin_tabs', 'window_nbr': ALL}, 'active_tab',
           allow_duplicate=True),
    Output({'type': 'fig_col', 'window_nbr': ALL}, 'width',
           allow_duplicate=True),
    Output({'type': 'usrbin_fig_div', 'window_nbr': ALL}, 'style'),
    Input('split_radio', 'value'),
    State('split_radio_store', 'data'),
    State('are_plots_comparable', 'children'),
    State({'type': 'bin_tabs', 'window_nbr': ALL}, 'active_tab'),
    State('plot_size_slider', 'value'),
    State('plot_size_slider', 'min'),
    State('plot_size_slider', 'max'),
    prevent_initial_call=True,
    #on_error=error_handler,
    )
def update_plot_splitting(split_value, prev_split_value, are_plots_comparable,
                          bin_tabs, size_value, size_min, size_max):
    if (split_value > 1):
        res_bin_tabs = []
        res_fig_cols = []
        res_fig_divs = []
        # from single window to multi
        from_single_to_multi = False
        if ((prev_split_value is not None) and (prev_split_value == 1)):
            from_single_to_multi = True
        if (int(are_plots_comparable)):
            for i, bin_tab in enumerate(bin_tabs):
                if (i < split_value):    # i == window_nbr
                    # update all tabs, in case a global parameter changed
                    # and trigger graph if first time this window is open
                    if (from_single_to_multi
                            and (bin_tab != INIT_ACTIVE_TAB_VALUE)):
                        res_bin_tabs.append(bin_tab)
                    else:
                        res_bin_tabs.append('0')
                    res_fig_cols.append({'size': 6})
                    res_fig_divs.append({'display': 'block'})
                else:
                    res_bin_tabs.append(no_update)
                    res_fig_cols.append({'size': 0})
                    res_fig_divs.append({'display': 'none'})
            # reduce the values of the plot size if single to multi
            if (from_single_to_multi):
                new_min, new_max = size_min/SIZE_DIV, size_max/SIZE_DIV
                set_props('plot_size_slider', dict(min=new_min, max=new_max,
                                                   value=size_value/SIZE_DIV,
                                                   marks={new_min: 'Small',
                                                          new_max: 'Large'}))

            return res_bin_tabs, res_fig_cols, res_fig_divs

        else:
            split_error_handler()
            set_props('split_radio', dict(value=1))
            no_res = [no_update for _ in range(len(bin_tabs))]

            return no_res, no_res, no_res

    else:   # split_value == 1 -> only show the first window and disable others
        # from multi windows to single
        if ((prev_split_value is not None) and (prev_split_value > 1)):
            # force reload of first plot for aspect ratio considerations
            res_bin_tab = bin_tabs[0]
            # increase the values of the plot size if multi to single
            new_min, new_max = size_min*SIZE_DIV, size_max*SIZE_DIV
            set_props('plot_size_slider', dict(min=new_min, max=new_max,
                                               value=size_value*SIZE_DIV,
                                               marks={new_min: 'Small',
                                                      new_max: 'Large'}))
        else:
            # if the current tab has never been activate it, force it a 0
            if (bin_tabs[0] != INIT_ACTIVE_TAB_VALUE):
                res_bin_tab = no_update
            else:
                res_bin_tab = '0'

        return ([res_bin_tab] + [no_update]*(len(bin_tabs)-1),
                [{'size': 12}] + [{'size': 0}]*(len(bin_tabs)-1),
                # margin = 100/12% (12 blocks total)
                [{'display': 'block', 'marginLeft': '8.3%',
                  'marginRight': '8.3%'}]
                + [{'display': 'none'}]*(len(bin_tabs)-1))


@callback(
    Output({'type': 'download_bin_data', 'window_nbr': MATCH}, 'data'),
    Input({'type': 'download_bin_data_btn', 'window_nbr': MATCH}, 'n_clicks'),
    State('proj_dropdown', 'value'),
    State('depth_slider', 'value'),
    State('colli_input', 'value'),
    State({'type': 'plot_tabs', 'window_nbr': MATCH}, 'active_tab'),
    State({'type': 'cache_store_bin', 'plot_nbr': ALL}, 'data'),
    State({'type': 'bin_tabs', 'window_nbr': MATCH}, 'active_tab'),
    State({'type': 'averaging', 'axis': ALL}, 'value'),
    State({'type': 'download_delimiter', 'window_nbr': MATCH}, 'value'),
    State({'type': 'download_end_line', 'window_nbr': MATCH}, 'value'),
    prevent_initial_call=True,
    #on_error=error_handler,
    )
def download_bin_data(n_clicks, projection, depth_ind, nbr_collisions, vplot,
                      usrbin_plots, bin_tab, avg_values, delimiter_char,
                      end_line_char):
    """Extraction is done manually from the plot object and not from the figure
    in order to output a txt file in a given format. Additionally, data are
    processed to be plot with plotly which might not be the real data that the
    user sees, e.g. with the logarithmic scale."""
    crt_plot = usrbin_plots[int(bin_tab)]
    abs_err, rel_err = PLOT_VALUES_TO_ERR_FLAGS[vplot]
    flt_nbr_coll = float(nbr_collisions)
    if (abs_err and (not rel_err)):
        data_type = 'abs'
    elif (rel_err and (not abs_err)):
        data_type = 'rel'
    else:
        data_type = 'avg'
    avg_values_ = [int(elem) for elem in avg_values]
    proj_ind = ord(projection)-ord('x')
    bins, bin_step = crt_plot.get_bins(projection, avg_values_[proj_ind])
    depth_ = bins[int(depth_ind)]
    mid_depth_ = depth_ = (bins[int(depth_ind)] + bins[int(depth_ind)+1])/2.0
    text_data = crt_plot.get_slice_data_as_string(projection, depth_,
                                                  avg_values_, data_type,
                                                  multiplicator=flt_nbr_coll,
                                                  percentage_rel_err=False,
                                                  delimiter_char=delimiter_char,
                                                  end_line_char=end_line_char)
    # Should add here eventually the simulation name
    other_axes = ['x', 'y', 'z']
    other_axes.remove(projection)
    proj_ind = ord(other_axes[0])-ord('x')
    bins, bin_step = crt_plot.get_bins(other_axes[0], avg_values_[proj_ind])
    bins_1 = [bins[0], bins[-1]]
    proj_ind = ord(other_axes[1])-ord('x')
    bins, bin_step = crt_plot.get_bins(other_axes[1], avg_values_[proj_ind])
    bins_2 = [bins[0], bins[-1]]
    file_name = ('simrad_usrbin_plot_' + str(int(bin_tab)+1)
                 + '_{}_'.format(projection.capitalize())
                 + '{}_cm_'.format(str(round(mid_depth_, 2)).replace('.', '_'))
                 + '{}_from_'.format(other_axes[0].capitalize())
                 + '{}_to_'.format(str(round(bins_1[0], 2)).replace('.', '_'))
                 + '{}_cm_'.format(str(round(bins_1[1], 2)).replace('.', '_'))
                 + '{}_from_'.format(other_axes[1].capitalize())
                 + '{}_to_'.format(str(round(bins_2[0], 2)).replace('.', '_'))
                 + '{}_cm'.format(str(round(bins_2[1], 2)).replace('.', '_'))
                 + '.txt')

    return dict(content=text_data, filename=file_name)


@callback(
    Output({'type': 'download_bin_raw_data', 'window_nbr': MATCH}, 'data'),
    Input({'type': 'download_bin_raw_data_btn', 'window_nbr': MATCH}, 'n_clicks'),
    State({'type': 'cache_store_bin', 'plot_nbr': ALL}, 'data'),
    State({'type': 'bin_tabs', 'window_nbr': MATCH}, 'active_tab'),
    State({'type': 'download_delimiter', 'window_nbr': MATCH}, 'value'),
    State({'type': 'download_end_line', 'window_nbr': MATCH}, 'value'),
    prevent_initial_call=True,
    #on_error=error_handler,
    )
def download_bin_raw_data(n_clicks, usrbin_plots, bin_tab, delimiter_char,
                          end_line_char):
    crt_plot = usrbin_plots[int(bin_tab)]
    list_data = crt_plot.get_raw_data_as_string(delimiter_char=delimiter_char,
                                                end_line_char=end_line_char)

    def write_archive(bytes_io):
        base_file_name = 'simrad_usrbin_plot_'
        with zipfile.ZipFile(bytes_io, mode='w') as zf:
            for i in range(len(list_data)):
                if (not i):
                    crt_file_name = base_file_name + 'values'
                else:
                    crt_file_name = base_file_name + 'errors'
                crt_file_name += '.txt'
                zf.writestr(crt_file_name, list_data[i])

    return dcc.send_bytes(write_archive, 'simrad_usrbin_plot_data.zip')



# update_scale_slider_header
clientside_callback(
    """
    function(scale_extrs, bin_tab, vplot, bin_table_data) {
        let scale_extr_1 = scale_extrs[0].toString();
        let scale_extr_2 = scale_extrs[1].toString();
        let new_proj_tag = "";
        let first = "";
        let unit = "";
        if (vplot == "rel_err") {
            first = "[";
            new_proj_tag = first.concat(scale_extr_1, ", ", scale_extr_2,
                                        "] %");
        } else {
            first = "[1e";
            unit = bin_table_data[parseInt(bin_tab)]['unit'];
            new_proj_tag = first.concat(scale_extr_1, ", 1e", scale_extr_2,
                                        "] ", unit);
        }

        return new_proj_tag;
    }
    """,
    Output({'type': 'scale_slider_header', 'window_nbr': MATCH,
            'plot_nbr': MATCH},
           'children'),
    Input({'type': 'scale_slider', 'window_nbr': MATCH, 'plot_nbr': MATCH},
           'drag_value'),
    State({'type': 'bin_tabs', 'window_nbr': MATCH}, 'active_tab'),
    State({'type': 'plot_tabs', 'window_nbr': MATCH}, 'active_tab'),
    State('bin_table', 'data'),
    prevent_initial_call=True,
    #on_error=error_handler
)

# update_depth_slider_header
clientside_callback(
    """
    function(depth_ind, bin_tab, projection, bin_table_data) {
        let min_str = "";
        let step_str = "";
        if (projection == 'x') {
            min_str = "x_bins_min";
            step_str = "x_bin_step";
        } else if (projection == 'y') {
            min_str = "y_bins_min";
            step_str = "y_bin_step";
        } else {
            min_str = "z_bins_min";
            step_str = "z_bin_step";
        }
        let min_bin = parseFloat(bin_table_data[parseInt(bin_tab)][min_str]);
        let step_bin = parseFloat(bin_table_data[parseInt(bin_tab)][step_str]);
        let start_bin = min_bin + (parseFloat(depth_ind)*step_bin);
        let end_bin = min_bin + ((parseFloat(depth_ind)+1.0)*step_bin);
        let first = "[";
        let new_depth_tag = first.concat(start_bin.toFixed(1), ", ",
                                         end_bin.toFixed(1), "] cm");

        return new_depth_tag;
    }
    """,
    Output('depth_slider_header', 'children'),
    Input('depth_slider', 'drag_value'),
    State({'type': 'bin_tabs', 'window_nbr': 0}, 'active_tab'),
    State('proj_dropdown', 'value'),
    State('bin_table', 'data'),
    prevent_initial_call=True,
    #on_error=error_handler
)



@callback(
    Output({'type': 'usrbin_fig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
           'figure', allow_duplicate=True),
    Input({'type': 'clear_trig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
          'children'),
    State({'type': 'usrbin_fig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
           'figure'),
    prevent_initial_call=True,
    #on_error=error_handler
)
def clear_graph_bin(_, crt_fig):
    window_nbr = ctx.outputs_list['id']['window_nbr']
    plot_nbr = ctx.outputs_list['id']['plot_nbr']

    return LOADING_FIGURE


@callback(
    Output({'type': 'usrbin_fig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
           'figure', allow_duplicate=True),
    Input({'type': 'fix_r_trig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
           'children'),
    State('fix_ratio', 'value'),
    State('split_radio', 'value'),
    State({'type': 'usrbin_fig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
          'figure'),
    running=[(Output({'type': 'usrbin_fig_loading', 'window_nbr': MATCH,
                      'plot_nbr': MATCH}, 'display'), 'show', 'auto'),],
    prevent_initial_call=True,
    #on_error=error_handler,
    )
def update_fig_ratio(_, fix_ratio, split_value, crt_fig):
    window_nbr = ctx.outputs_list['id']['window_nbr']
    if ((window_nbr < split_value) and crt_fig and (crt_fig != LOADING_FIGURE)):
        patched_fig = Patch()
        if (fix_ratio):
            patched_fig['layout']['yaxis']['scaleanchor'] = 'x'
            patched_fig['layout']['yaxis']['scaleratio'] = 1
        else:
            patched_fig['layout']['yaxis']['scaleanchor'] = None
            patched_fig['layout']['yaxis']['scaleratio'] = None

        return patched_fig

    raise PreventUpdate


@callback(
    Output({'type': 'usrbin_fig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
           'figure', allow_duplicate=True),
    Input('map_display', 'value'),
    State({'type': 'usrbin_fig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
          'figure'),
    prevent_initial_call=True,
    #on_error=error_handler,
    )
def update_map_display(map_display, crt_fig):
    patched_fig = Patch()
    for i, trace in enumerate(crt_fig['data']):
        if (trace['name'] == LHCB_MAP_NAME):
            if (map_display):
                patched_fig['data'][i]['visible'] = True
            else:
                patched_fig['data'][i]['visible'] = False

    return patched_fig


@callback(
    Output({'type': 'usrbin_fig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
           'figure', allow_duplicate=True),
    Input({'type': 'color_trig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
           'children'),
    State('bin_color_dropdown', 'value'),
    State('split_radio', 'value'),
    State({'type': 'usrbin_fig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
          'figure'),
    running=[(Output({'type': 'usrbin_fig_loading', 'window_nbr': MATCH,
                      'plot_nbr': MATCH}, 'display'), 'show', 'auto'),],
    prevent_initial_call=True,
    #on_error=error_handler,
    )
def update_colorscale(_, colorscale, split_value, crt_fig):
    window_nbr = ctx.outputs_list['id']['window_nbr']
    if ((window_nbr < split_value) and crt_fig and (crt_fig != LOADING_FIGURE)):
        patched_fig = Patch()
        colorname = COLORNAME_TO_COLORSCALES[colorscale]
        colorscale_ = get_saturated_colorscale(colorname)
        patched_fig['data'][0]['colorscale'] = colorscale_

        return patched_fig

    raise PreventUpdate


@callback(
    Output({'type': 'usrbin_fig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
           'figure', allow_duplicate=True),
    Input({'type': 'fig_a_trig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
           'children'),
    State('split_radio', 'value'),
    State({'type': 'usrbin_fig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
          'figure'),
    State('plot_size_slider', 'value'),
    State('breakpoints_bin', 'height'),
    State('breakpoints_bin', 'width'),
    running=[(Output({'type': 'usrbin_fig_loading', 'window_nbr': MATCH,
                      'plot_nbr': MATCH}, 'display'), 'show', 'auto'),],
    prevent_initial_call=True,
    #on_error=error_handler,
    )
def update_fig_aspect(_, split_value, crt_fig, size_slider_value,
                      window_height, window_width):
    window_nbr = ctx.outputs_list['id']['window_nbr']
    if ((window_nbr < split_value) and crt_fig and (crt_fig != LOADING_FIGURE)):
        patched_fig = Patch()
        # --------------------------------------------------------------------------
        # Slider value repr. percentage, additional /2 if more than 1 plot displayed
        #if (split_value > 1):
        #    size_slider_value /= 1.5
        new_plot_height = int(window_height*size_slider_value/WINDOW_DIVIDER)
        patched_fig['layout']['height'] = new_plot_height
        font_size = int(size_slider_value*window_height/(WINDOW_DIVIDER*40))
        patched_fig['layout']['title']['font']['size'] = int(font_size*1.2)
        patched_fig['layout']['yaxis']['tickfont']['size'] = font_size
        patched_fig['layout']['xaxis']['tickfont']['size'] = font_size
        patched_fig['layout']['yaxis']['title']['font']['size'] = font_size
        patched_fig['layout']['xaxis']['title']['font']['size'] = font_size
        patched_fig['data'][0]['colorbar']['tickfont']['size'] = font_size
        patched_fig['data'][0]['colorbar']['title']['font']['size'] = font_size
        # Displace the colorbar next to the left border of the plot
        #if (new_fig is None):
        x0, x1 = crt_fig['layout']['xaxis']['range']
        y0, y1 = crt_fig['layout']['yaxis']['range']
        offset = get_colorbar_x_axis_pos(x0, x1, y0, y1, window_height,
                                         window_width, size_slider_value,
                                         WINDOW_DIVIDER, (split_value>1))
        patched_fig['data'][0]['colorbar']['x'] = offset

        patched_fig['layout']['xaxis']['showgrid'] = False
        patched_fig['layout']['xaxis']['constrain'] = 'domain'
        patched_fig['layout']['yaxis']['showgrid'] = False
        patched_fig['layout']['yaxis']['constrain'] = 'domain'

        return patched_fig

    raise PreventUpdate


@callback(
    Output({'type': 'usrbin_fig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
           'figure', allow_duplicate=True),
    Input({'type': 'box_d_trig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
          'children'),
    State('split_radio', 'value'),
    State({'type': 'usrbin_fig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
          'figure'),
    State('box_display', 'value'),
    State({'type': 'box_min', 'axis': ALL}, 'value'),
    State({'type': 'box_max', 'axis': ALL}, 'value'),
    State('proj_dropdown', 'value'),
    running=[(Output({'type': 'usrbin_fig_loading', 'window_nbr': MATCH,
                      'plot_nbr': MATCH}, 'display'), 'show', 'auto'),],
    prevent_initial_call=True,
    #on_error=error_handler,
    )
def update_display_box(_, split_value, crt_fig, box_display_val, min_vals,
                       max_vals, projection):
    window_nbr = ctx.outputs_list['id']['window_nbr']
    if ((window_nbr < split_value) and crt_fig and (crt_fig != LOADING_FIGURE)):
        patched_fig = Patch()
        # --------------------------------------------------------------------------
        # Delete any prior displayed boxes (even if input are not valid)
        if (crt_fig and (crt_fig != LOADING_FIGURE)):
            i = 0
            while (i < len(crt_fig['data'])):
                if (('name' in crt_fig['data'][i])
                            and (crt_fig['data'][i]['name'] == PLOT_BOX_NAME)):
                    patched_fig['data'].remove(crt_fig['data'][i])
                i += 1
        if (box_display_val): # display box if requested
            try: # for float conversion
                x_min, y_min, z_min = min_vals[0], min_vals[1], min_vals[2]
                x_max, y_max, z_max = max_vals[0], max_vals[1], max_vals[2]
                if (projection.lower()=='x'):
                    xs = [float(z_min), float(z_min), float(z_max), float(z_max)]
                    ys = [float(y_min), float(y_max), float(y_max), float(y_min)]
                elif (projection.lower()=='y'):
                    xs = [float(z_min), float(z_min), float(z_max), float(z_max)]
                    ys = [float(x_min), float(x_max), float(x_max), float(x_min)]
                else:
                    xs = [float(x_min), float(x_min), float(x_max), float(x_max)]
                    ys = [float(y_min), float(y_max), float(y_max), float(y_min)]
                xs.append(xs[0])    # closing the rectangle
                ys.append(ys[0])    # closing the rectangle
                patched_fig['data'].append(go.Scatter(x=xs, y=ys, mode='lines',
                                                      showlegend=False,
                                                      name=PLOT_BOX_NAME,
                                                      line={'color': 'black',
                                                            'width': 4}))
            except:
                pass

        return patched_fig

    raise PreventUpdate


@callback(
    Output({'type': 'usrbin_fig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
           'figure'),
    Output({'type': 'fix_r_trig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
           'children'),
    Output({'type': 'color_trig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
           'children'),
    Output({'type': 'fig_a_trig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
           'children'),
    Output({'type': 'box_d_trig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
          'children'),
    Input({'type': 'depth_trig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
          'children'),
    Input({'type': 'size_trig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
          'children'),
    Input({'type': 'map_c_trig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
          'children'),
    Input({'type': 'scale_s_trig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
          'children'),
    State({'type': 'scale_slider', 'window_nbr': MATCH, 'plot_nbr': MATCH},
          'value'),
    State({'type': 'usrbin_fig', 'window_nbr': MATCH, 'plot_nbr': MATCH},
          'figure'),
    State('depth_slider', 'value'),
    State('map_calc', 'n_clicks'),
    State('split_radio', 'value'),
    State('proj_dropdown', 'value'),    # chained -> altering depth_slider
    State('colli_input', 'value'),  # chained -> altering scale_slider
    State({'type': 'plot_tabs', 'window_nbr': MATCH}, 'active_tab'),   # chained -> altering scale_slider
    State({'type': 'cache_store_bin', 'plot_nbr': MATCH}, 'data'),
    State({'type': 'bin_tabs', 'window_nbr': MATCH}, 'active_tab'),
    State({'type': 'averaging', 'axis': ALL}, 'value'), # chained -> altering depth_slider
    State('map_display', 'value'),
    State('progress_bin_row_div', 'style'),
    running=[(Output({'type': 'usrbin_fig_loading', 'window_nbr': MATCH,
                      'plot_nbr': MATCH}, 'display'), 'show', 'show'),],
    prevent_initial_call=True,
    #on_error=error_handler
    )
def update_graph_bin(_, __, ___, ____, scale_extrs, crt_fig, depth_ind,
                     map_calc_click, split_value, projection, nbr_collisions,
                     vplot, crt_plot, bin_tab, avg_values, map_disp_val,
                     progress_bin_row_div_style):

    if ((scale_extrs is None) or (depth_ind is None)):

        raise PreventUpdate

    new_fig = None
    # Patch the current figure if exist, otherwise create new one
    if (crt_fig and (crt_fig != LOADING_FIGURE)):   # crt_fig != None and crt_fig != {}
        output_fig = Patch()
    else:
        output_fig = {}
    # Extract the trigger calling the callback
    if (isinstance(ctx.triggered_id, dict)):
        triggered_id = ctx.triggered_id['type']
    else:
        triggered_id = ctx.triggered_id
    # --------------------------------------------------------------------------
    # Update the figure if callback trigger by scale/depth slider or map display
    #if ((triggered_id == 'scale_slider') or (triggered_id == 'depth_slider')
    #        or (triggered_id == 'map_calc')):
        # Save potential 2d map to restore it later
    map_backup = None
    if (crt_fig and (crt_fig != LOADING_FIGURE)):
        for trace in crt_fig['data']:
            if (trace['name'] == LHCB_MAP_NAME):
                map_backup = trace
    abs_err, rel_err = PLOT_VALUES_TO_ERR_FLAGS[vplot]
    plot_map = True if (triggered_id == 'map_calc') else False
    if (plot_map and (not crt_plot.inp_file_path)):
        map_error_handler()
        plot_map = False
    # should check value e.g. nbr_collisions is an int
    proj_ind = ord(projection)-ord('x')
    bins, bin_step = crt_plot.get_bins(projection,
                                       int(avg_values[proj_ind]))
    depth_ = bins[int(depth_ind)]
    new_fig = crt_plot.get_plot(projection=projection, depth=depth_,
                                nbr_collisions=float(nbr_collisions),
                                scale_min_expo_val=int(scale_extrs[0]),
                                scale_max_expo_val=int(scale_extrs[1]),
                                axes_avgs=[int(el) for el in avg_values],
                                abs_err=abs_err, rel_err=rel_err,
                                plot_map=plot_map,
                                map_name=LHCB_MAP_NAME,
                                )
    # Restore 2d map if any (fig.add_trace slow, append to list instead)
    new_fig_data = [trace for trace in new_fig['data']]
    if ((map_backup is not None) and (not plot_map) and map_disp_val):
        new_fig_data.append(map_backup)

    output_fig['data'] = new_fig_data
    output_fig['layout'] = new_fig['layout']

    # Very first call to the function (equiv. to all figs == None)
    if (progress_bin_row_div_style['display'] == 'block'):
        set_props('bin_progress_bar', dict(value='1.0'))
        set_props('progress_bin_row_div', dict(style={'display': 'none'}))
        set_props('bin_rows_div', dict(style={'display': 'block'}))

    # Returning the figure as a dict instead of the fig object is faster
    return output_fig, None, None, None, None#, None#, 'hide'



if __name__ == "__main__":

    from flask import Flask

    server = Flask(__name__)

    # inject Dash
    app = init_dashboard(server)

    app_port = 5023
    print(f'http://localhost:{app_port}{BASE_URL}')
    app.enable_dev_tools(debug=True)
    app.server.run(debug=True, port=app_port)
