
import os
import copy
import math
import numpy as np
import re
import sys
from urllib.request import unquote

import plotly as plt
import plotly.express as px
from sympy import divisors
import plotly.graph_objects as go
import zipfile

from dash import set_props, ctx, Patch, dash_table
from dash.exceptions import PreventUpdate
from dash.dash_table.Format import Format, Scheme, Trim
import dash_bootstrap_components as dbc
from dash_breakpoints import WindowBreakpoints
from dash_extensions.enrich import DashProxy, Output, Input, State, \
                                   Serverside, html, dcc, callback, \
                                   ServersideOutputTransform, \
                                   FileSystemBackend, clientside_callback

from simrad_server.resources.scripts.utils.constants.plot_colors import \
                                                      PLOTLY_COLOR_PALETTES_DICT
from simrad_server.resources.scripts.utils.constants.file_parameters import \
                                   DELIMITER_CHAR_OPTIONS, END_LINE_CHAR_OPTIONS
from simrad_server.resources.scripts.parsers.scoring_parsers.usrtrack_parser\
                                                           import UsrtrackParser
from simrad_server.resources.scripts.plots.usrtrack_plot import UsrtrackPlot
from simrad_server.resources.scripts.utils.helpers.plot_helpers import \
                      create_colorscale_from_colorlist, get_saturated_colorscale
from simrad_server.resources.scripts.utils.helpers.css_helpers import \
                                                                blue_roundbutton

FONT_AWESOME = "https://use.fontawesome.com/releases/v5.10.2/css/all.css"
CODEPEN = "https://codepen.io/chriddyp/pen/bWLwgP.css"

BASE_URL = '/usrtrack_plot/'

REQUESTS_PATHNAME_PREFIX = os.getenv("URL_PREFIX", "") + BASE_URL

BACKEND_FOLDER = ''

RE_QUERY = re.compile(r"[(\?|\&)]([^=]+)\=([^&#]+)")

LHCB_MAP_NAME = 'LHCb_2D_map'

FIG_ASPECT_RATIO = (20, 9)  # appear as ~ (16, 9), but place for legend
FIG_WINDOW_COVERAGE = 10/12  # in layout, size 8 with 2 padding on each side

# num: (abs_err, rel_err)
PLOT_VALUES_TO_ERR_FLAGS = {'mean': (False, False), 'abs_err': (True, False),
                            'rel_err': (False, True)}


def init_dashboard(server, transforms=None):

    #if (not os.path.isdir(backend_dir)):
    #    error_message: str = ("The provided directory for backend was "
    #                          + "not found")
    #    raise Exception(error_message)

    if (transforms is None):
        backend_dir = os.getenv("CACHE_DIR", "temporary_backend/")
        backend = FileSystemBackend(backend_dir)
        transforms = [ServersideOutputTransform(backends=[backend])]

    app = DashProxy(__name__, server=server,
                    routes_pathname_prefix=BASE_URL,  # Backend routes
                    requests_pathname_prefix=REQUESTS_PATHNAME_PREFIX,  # Frontend requests
                    external_stylesheets=[dbc.themes.BOOTSTRAP, FONT_AWESOME,
                                          CODEPEN],
                    transforms=transforms,
                    suppress_callback_exceptions=True,
                    prevent_initial_callbacks=True,
                    )

    # Switch to display error
    info_text_disp_err_label = ('If selected, this option allows to display '
                                + 'the error bars on the plot.')
    info_text_disp_err_label = html.Div(info_text_disp_err_label,
                                         style={'font-size': '150%'})
    disp_err_label = html.Div([html.Strong('Display Error Bars'),
                               dbc.Button(className="far fa-question-circle",
                                          style=blue_roundbutton,
                                          id='info_disp_err_label'),
                               dbc.Popover(info_text_disp_err_label,
                                           target='info_disp_err_label',
                                           body=True, trigger="hover")],
                               style={'display': 'flex',
                                      'flex-direction': 'row'})
    disp_err = dbc.Checklist(options=[{'label': disp_err_label,
                                       'value': 1}],
                             value=[1], id='disp_err', switch=True)
    # Switch to enable x-log scale or y-log scale
    info_text_log_x_label = ('If selected, this option allows to display '
                             + 'logarithmic scale on the x-axis of the plot.')
    info_text_log_x_label = html.Div(info_text_log_x_label,
                                     style={'font-size': '150%'})
    log_x_label = html.Div([html.Strong('Logarithmic X-axis'),
                            dbc.Button(className="far fa-question-circle",
                                       style=blue_roundbutton,
                                       id='info_log_x_label'),
                            dbc.Popover(info_text_log_x_label,
                                        target='info_log_x_label',
                                        body=True, trigger="hover")],
                            style={'display': 'flex',
                                   'flex-direction': 'row'})
    log_x = dbc.Checklist(options=[{'label': log_x_label,
                                    'value': 1}],
                          value=[1], id='log_x', switch=True)
    info_text_log_y_label = ('If selected, this option allows to display '
                             + 'logarithmic scale on the x-axis of the plot.')
    info_text_log_y_label = html.Div(info_text_log_y_label,
                                      style={'font-size': '150%'})
    log_y_label = html.Div([html.Strong('Logarithmic Y-axis'),
                            dbc.Button(className="far fa-question-circle",
                                       style=blue_roundbutton,
                                       id='info_log_y_label'),
                            dbc.Popover(info_text_log_y_label,
                                        target='info_log_y_label',
                                        body=True, trigger="hover")],
                            style={'display': 'flex',
                                   'flex-direction': 'row'})
    log_y = dbc.Checklist(options=[{'label': log_y_label,
                                    'value': 1}],
                          value=[1], id='log_y', switch=True)
    # Switch to enable lethargy unit
    info_text_lethargy_label = ('If selected, this option allows to display '
                                + 'data with lethargic unit. This allows '
                                + 'proportional interpretation of the plot '
                                + 'with logarithmic x-axis.')
    info_text_lethargy_label = html.Div(info_text_lethargy_label,
                                      style={'font-size': '150%'})
    lethargy_label = html.Div([html.Strong('Lethargic Units'),
                               dbc.Button(className="far fa-question-circle",
                                          style=blue_roundbutton,
                                          id='info_lethargy_label'),
                               dbc.Popover(info_text_lethargy_label,
                                           target='info_lethargy_label',
                                           body=True, trigger="hover")],
                               style={'display': 'flex',
                                      'flex-direction': 'row'})
    lethargy = dbc.Checklist(options=[{'label': lethargy_label,
                                       'value': 1, 'disabled': False}],
                             value=[1], id='lethargy', switch=True)
    # Create a card to hold the toggle button
    lim_x_min = dbc.Input(id='lim_x_min', type='text', debounce=True,
                          placeholder='min. value')
    lim_x_max = dbc.Input(id='lim_x_max', type='text', debounce=True,
                          placeholder='max. value')
    lim_x_label = html.Label('X-axis Boundaries: ', style={'width': "100%"})
    lim_x = dbc.Stack([lim_x_label, lim_x_min, lim_x_max],
                      gap=3, direction='horizontal')
    lim_y_min = dbc.Input(id='lim_y_min', type='text', debounce=True,
                          placeholder='min. value')
    lim_y_max = dbc.Input(id='lim_y_max', type='text', debounce=True,
                          placeholder='max. value')
    lim_y_label = html.Label('Y-axis Boundaries: ', style={'width': "100%"})
    lim_y = dbc.Stack([lim_y_label, lim_y_min, lim_y_max],
                      gap=3, direction='horizontal')
    # Create dropdown menu with available colorscales
    color_dropdown_label = html.Label('Color Palette: ',
                                      style={'width': "50%"})
    dfl_color_value = ''
    color_options = list(PLOTLY_COLOR_PALETTES_DICT.keys())
    for color_opt in color_options:
        if (color_opt.startswith('Alphabet')):
            dfl_color_value = copy.copy(color_opt)
    if (not dfl_color_value):
        dfl_color_value = color_options[0]
    color_dropdown = dcc.Dropdown(options=color_options,
                                  value=dfl_color_value,
                                  id='trk_color_dropdown',
                                  clearable=False, style={'width': "100%"})
    color_dd = dbc.Stack([color_dropdown_label, color_dropdown],
                      gap=3, direction='horizontal')
    card_header = dbc.CardHeader(html.Strong('Plot Parameters: '))
    card_body = dbc.Stack([html.Br(), lim_x, lim_y, html.Br(), color_dd], gap=3)
    plot_limits_card = dbc.Card([card_header, card_body],
                                 style={"border": "none"})
    # Create a card to hold the plot boundaries
    card_header = dbc.CardHeader(html.Strong('Plot Boundaries: '))
    card_body = dbc.Stack([html.Br(),
                           dbc.Stack([log_x, html.Br(), disp_err],
                                     direction='horizontal', gap=3),
                           html.Br(),
                           dbc.Stack([log_y, html.Br(), lethargy],
                                     direction='horizontal', gap=3)],
                           gap=3)
    plot_control_card = dbc.Card([card_header, card_body],
                                 style={"border": "none"})
    # Create dropdown menu with available particles
    particle_dropdown = dcc.Dropdown(id='particle_dropdown', multi=True)
    # Create a card to hold the particle dropdown
    card_header = dbc.CardHeader(html.Strong('Particles on Plot: '))
    card_body = dbc.Stack([html.Br(), particle_dropdown], gap=3)
    particle_card = dbc.Card([card_header, card_body], style={"border": "none"})
    # Create data table to display track information
    track_table = dash_table.DataTable(id='track_table', page_size=10,
                                       #style_as_list_view=True,
                                       row_selectable='multi',
                                       )
    # Create button to download raw data
    info_text_download_header = ("Downloading data as multiple files, "
                                 + "where each file contains the fluence of a "
                                 + "given particle. If plot data, each file "
                                 + "contains 3 columns, representing each "
                                 + "point on the plot, and the corresponding "
                                 + "error. If raw data, each file contains the "
                                 + "entire data set in the ActiWiz Creator "
                                 + "Software format.")
    info_text_download_header = html.Div(info_text_download_header,
                                         style={'font-size': '150%'})
    download_header = dbc.Stack([html.Strong('Exporting Data: '),
                                 dbc.Button(className='far fa-question-circle',
                                            style=blue_roundbutton,
                                            id='info_trk_download'),
                                 dbc.Popover(info_text_download_header,
                                             target='info_trk_download',
                                             body=True, trigger="hover")],
                                direction='horizontal', gap=0)
    download_header = html.Div([download_header], style={'width': "35%"})
    download_delimiter_header = html.Div('Delimiter Character: ',
                                         style={'width': "25%"})
    download_delimiter_dropdown = dcc.Dropdown(
                                   options=DELIMITER_CHAR_OPTIONS,
                                   value=DELIMITER_CHAR_OPTIONS[0]['value'],
                                   id='trk_download_delimiter',
                                   clearable=False, style={'width': "45%"})
    download_end_line_header = html.Div('End-of-line Character: ',
                                        style={'width': "25%"})
    download_end_line_dropdown = dcc.Dropdown(
                                    options=END_LINE_CHAR_OPTIONS,
                                    value=END_LINE_CHAR_OPTIONS[0]['value'],
                                    id='trk_download_end_line',
                                    clearable=False, style={'width': "45%"})
    download_button = dbc.Button('Download Plot Data', color='primary',
                                 className='me-1', outline=True,
                                 id='trk_download_data_btn',
                                 style={'width': "40%"})
    download_dcc = dcc.Download(id='trk_download_data')
    raw_download_button = dbc.Button('Download Raw Data', color='primary',
                                     className='me-1', outline=True,
                                     id='trk_raw_download_data_btn',
                                     style={'width': "40%"})
    raw_download_dcc = dcc.Download(id='trk_raw_download_data')
    download_button_stack = dbc.Stack([download_header,
                                       html.Br(),
                                       html.Br(),
                                       download_delimiter_header,
                                       download_delimiter_dropdown,
                                       html.Br(),
                                       download_end_line_header,
                                       download_end_line_dropdown,
                                       html.Br(),
                                       download_button, download_dcc,
                                       raw_download_button, raw_download_dcc],
                                      direction='horizontal', gap=1)
    # Create plot
    usrtrack_fig = dcc.Loading(dcc.Graph(id='usrtrack_fig'), type='circle')
    # Create card for expert user
    expert_label_1 = html.Strong('Welcome LHCb Radiation Expert !')
    expert_label_2 = html.Div("As an expert, you have access to unique "
                              "features. Click on the button if you would like "
                              "to change the view to user or back to expert.")
    expert_button = dbc.Button(id='expert_trk_button')
    expert_card = dbc.Card(dbc.CardBody([dbc.Stack([expert_label_1,
                                                    expert_label_2,
                                                    expert_button],
                                                   direction='horizontal',
                                                   gap=3)],
                                        className='mt-3'))
    # Create the entire layout per rows
    row_0 = html.Div([
                dbc.Row([dbc.Col([dbc.Stack([html.Br(),
                                             expert_card,
                                             html.Br(),
                                             ],
                                            gap=3)
                               ], width={'size': 10, "offset": 1})
                            ], align='center', id='expert_trk_row_div')
                          ])
    row_1 = html.Div([
                dbc.Row([dbc.Col([dbc.Stack([html.Br(),
                                             track_table,
                                             html.Br(),
                                             ],
                                            gap=3)
                               ], width={'size': 10, "offset": 1})
                            ], align='center')
                          ], id='table_row_div')
    row_2 = html.Div([
             dbc.Row([dbc.Col([dbc.Stack([html.Br(), particle_card],
                                         gap=3)],
                               width={"size": 3, "offset": 1}, align='start'),
                      dbc.Col([dbc.Stack([html.Br(), plot_limits_card],
                                         gap=3)],#direction='horizontal')],
                               width={"size": 2, "offset": 1}, align='start'),
                      dbc.Col([dbc.Stack([html.Br(), plot_control_card],
                                         gap=3)],#, direction='horizontal')],
                               width={"size": 3, "offset": 1}, align='start'),
                      ], align='center')
                  ])

    row_3 = html.Div([
                dbc.Row([dbc.Col([dbc.Stack([html.Br(),
                                             usrtrack_fig],
                                            gap=3)
                               ], width={'size': 10, "offset": 1})
                            ], align='center')
                          ])

    row_4 = html.Div([
                dbc.Row([dbc.Col([dbc.Stack([html.Br(),
                                             download_button_stack,
                                             html.Br()],
                                            gap=3)
                               ], width={'size': 10, "offset": 1})
                            ], align='center')
                          ])
    # Adding backend components
    modal_timeout_header = dbc.ModalHeader(dbc.ModalTitle("This plot expired."),
                                           close_button=True)
    modal_timeout_body = dbc.ModalBody("Please reload your web page to "
                                       "access the content.")
    modal_timeout_footer = dbc.ModalFooter(dbc.Button("Close",
                                                       id="foot_timeout_track",
                                                       className="ms-auto",
                                                       n_clicks=0,))
    modal_timeout = dbc.Modal([modal_timeout_header, modal_timeout_body,
                               modal_timeout_footer],
                               id="timeout_track", centered=True, is_open=False)
    modal_load_error = dbc.Modal([], id="load_error_track", keyboard=False,
                                 backdrop="static",)

    breakpoints = WindowBreakpoints(
            id='breakpoints_track',
            # Define the breakpoint thresholds
            widthBreakpointThresholdsPx=[80, 10000],
            # And their name, note that there is one more name than breakpoint thresholds
            widthBreakpointNames=["sm", "md", "lg"],
        )

    backend_components = [dcc.Location(id='url_track', refresh=False),
                          dcc.Loading(dcc.Store(id='cache_store_track'),
                                      fullscreen=True, type='dot'),
                          modal_timeout,
                          dbc.Alert('', dismissable=True, id='warning_track',
                                    is_open=False),
                          breakpoints]

    app.layout = html.Div(backend_components
                          + [row_0, row_1, row_2, row_3, row_4])

    return app


################################################################################
# Error handling
################################################################################

@callback(
    Output("timeout_track", "is_open"),
    Input("foot_timeout_track", "n_clicks"),
    prevent_initial_call=True)
def toggle_modal_timeout_track(n):

    return False

def error_handler(err):

    set_props("timeout_track", dict(is_open=True))


def load_error_handler(err):
    children = [dbc.ModalHeader(dbc.ModalTitle("Data Loading Error."),
                                close_button=False),
                dbc.ModalBody(str(err)), dbc.ModalFooter('')]

    set_props("load_error_track", dict(children=children, is_open=True))


################################################################################
# Callack formatting
################################################################################

def check_lim_bound(val, other_val, extr, log):
    """Check if the provided value is a valid float and is bigger than the
    other value (min or max)."""
    invalid = False
    try:
        val_ = float(val)
    except:
        invalid = True
    if (not invalid):
        try:    # respect the ordering
            other_val_ = float(other_val)
            if (extr == 'min'):
                # the min value can not be greater than the crt max value
                if (val_ >= other_val_):
                    invalid = True
            else:   # extr == 'max'
                # the max value can not be smaller than the crt min value
                if (val_ <= other_val_):
                    invalid = True
        except:
            pass
    if (log):
        str_val = None if invalid else str("{:.2e}".format(val_))
    else:
        str_val = None if invalid else str("{:.2f}".format(val_))

    return str_val, False

@callback(
    Output('lim_x_min', 'value', allow_duplicate=True),
    Output('lim_x_min', 'invalid'),
    Input('lim_x_min', 'value'),
    State('lim_x_max', 'value'),
    State('log_x', 'value'),
    on_error=error_handler,
    prevent_initial_call=True)
def check_lim_x_min(val, other_val, log_x):

    return check_lim_bound(val, other_val, 'min', bool(log_x))

@callback(
    Output('lim_x_max', 'value', allow_duplicate=True),
    Output('lim_x_max', 'invalid'),
    Input('lim_x_max', 'value'),
    State('lim_x_min', 'value'),
    State('log_x', 'value'),
    on_error=error_handler,
    prevent_initial_call=True)
def check_lim_x_max(val, other_val, log_x):

    return check_lim_bound(val, other_val, 'max', bool(log_x))

@callback(
    Output('lim_y_min', 'value', allow_duplicate=True),
    Output('lim_y_min', 'invalid'),
    Input('lim_y_min', 'value'),
    State('lim_y_max', 'value'),
    State('log_y', 'value'),
    on_error=error_handler,
    prevent_initial_call=True)
def check_lim_y_min(val, other_val, log_y):

    return check_lim_bound(val, other_val, 'min', bool(log_y))

@callback(
    Output('lim_y_max', 'value', allow_duplicate=True),
    Output('lim_y_max', 'invalid'),
    Input('lim_y_max', 'value'),
    State('lim_y_min', 'value'),
    State('log_y', 'value'),
    on_error=error_handler,
    prevent_initial_call=True)
def check_lim_y_max(val, other_val, log_y):

    return check_lim_bound(val, other_val, 'max', bool(log_y))


################################################################################
# Initial creation of plot object
################################################################################

@callback(Output('cache_store_track', 'data'),
          Output('track_table', 'data'),
          Output('track_table', 'columns'),
          Output('track_table', 'selected_rows'),
          Output('expert_trk_button', 'n_clicks'),
          Output('expert_trk_row_div', 'style'),
          Input('url_track', 'href'),
          prevent_initial_call=True,
          on_error=load_error_handler,
          )
def query_data_track(href):

    args = RE_QUERY.findall(href)
    dirs = []
    suffixes = []
    dir_volumes = []
    dir_simu_names = []
    type = ''
    for arg in args:
        key, value = unquote(arg[0]), unquote(arg[1])
        if (key.startswith('dir')):
            dirs.append(copy.copy(value))
            suffixes.append(key[3:])
        if (key == 'type'):
            type = copy.copy(value)

    for suffix in suffixes:
        vol_matching_value = ''
        for arg in args:
            key, value = unquote(arg[0]), unquote(arg[1])
            if (key == 'vol'+suffix):
                vol_matching_value = value
        if (vol_matching_value):
            dir_volumes.append(float(vol_matching_value))
        else:
            dir_volumes.append(None)

    for suffix in suffixes:
        name_matching_value = ''
        for arg in args:
            key, value = unquote(arg[0]), unquote(arg[1])
            if (key == 'name'+suffix):
                name_matching_value = value
        if (name_matching_value):
            dir_simu_names.append(copy.copy(name_matching_value))
        else:
            dir_simu_names.append(None)

    if (not dirs):
        error_message: str = ("No directory containing the data was provided. "
                              + "Hint: check 'dir' parameter in query string.")

        raise Exception(error_message)

    usrtrack_plot = UsrtrackPlot()
    nbr_tracks = [] # keep the nbr tracks added in each file
    simu_names = []
    volumes = []
    value_file_paths = []
    abs_err_file_paths = []
    rel_err_file_paths = []
    inp_file_paths = []
    main_bin_exts = ['.txt', '.lis']
    for i, crt_dir in enumerate(dirs):
        if (not os.path.isdir(crt_dir)):
            error_message: str = ("The provided directory '{}'".format(crt_dir)
                                  + "containing the data was not found")

            raise Exception(error_message)
        list_files = os.listdir(crt_dir)
        # For now, assumed that there is only 1 .txt, 1 .rel.err, and 1 .abs.err
        value_file_path = ''
        for file in list_files:
            for main_bin_ext in main_bin_exts:
                if (file.lower().endswith(main_bin_ext)):
                    value_file_paths.append(os.path.join(crt_dir, file))
                    abs_err_file_paths.append('')
                    rel_err_file_paths.append('')
                    inp_file_paths.append('')
                    simu_names.append(dir_simu_names[i])
                    volumes.append(dir_volumes[i])
        abs_err_file_path, rel_err_file_path, inp_file_path = '', '', ''
        for file in list_files:
            if (file.lower().endswith('.abs.err')):
                abs_err_file_path = os.path.join(crt_dir, file)
                temp_file_name = abs_err_file_path[:-8]+main_bin_ext
                if (temp_file_name in value_file_paths):
                    ind = value_file_paths.index(temp_file_name)
                    abs_err_file_paths[ind] = abs_err_file_path
            if (file.lower().endswith('.rel.err')):
                rel_err_file_path = os.path.join(crt_dir, file)
                temp_file_name = rel_err_file_path[:-8]+main_bin_ext
                if (temp_file_name in value_file_paths):
                    ind = value_file_paths.index(temp_file_name)
                    rel_err_file_paths[ind] = rel_err_file_path
            if (file.lower().endswith('.inp')):
                inp_file_path = os.path.join(crt_dir, file)
                temp_file_name = inp_file_path[:-4]+main_bin_ext
                if (temp_file_name in value_file_paths):
                    ind = value_file_paths.index(temp_file_name)
                    inp_file_paths[ind] = inp_file_path

    for i in range(len(value_file_paths)):
        value_file_path = value_file_paths[i]
        abs_err_file_path = abs_err_file_paths[i]
        rel_err_file_path = rel_err_file_paths[i]
        inp_file_path = inp_file_paths[i]
        try:
            if (abs_err_file_path):
                new_tracks = UsrtrackParser.get_tracks(value_file_path,
                                                       abs_err_file_path,
                                                       rel_err=False)
            elif (rel_err_file_path):
                new_tracks = UsrtrackParser.get_tracks(value_file_path,
                                                       rel_err_file_path,
                                                       rel_err=True)
            else:
                new_tracks = UsrtrackParser.get_tracks(value_file_path)
            usrtrack_plot.add_track(*new_tracks)
            nbr_tracks.append(usrtrack_plot.nbr_tracks)

            #if (not abs_err_file_path):
            #    warning_message: str = ("No error file was found in the provided "
            #                            "directory. Error data might not be "
            #                            "displayed correctly.")
            #
            #    set_props("warning_track",
            #              dict(is_open=True, children=warning_message))


        except Exception as error:

            raise Exception(str(error))

    ############################################################################
    track_info_table = usrtrack_plot.track_info_table
    keys = track_info_table[0].keys()
    columns = []
    if ('particle_nbr' in keys):
        columns.append(dict(id='particle_nbr', name='FLUKA Particle Number',
                            type='numeric',
                            format=Format(precision=2,
                                          scheme=Scheme.decimal_integer)))
    if ('region_nbr' in keys):
        columns.append(dict(id='region_nbr', name='FLUKA Region Number',
                            type='numeric',
                            format=Format(precision=2,
                                          scheme=Scheme.decimal_integer)))
    if ('track_name' in keys):
        columns.append(dict(id='track_name', name='Track Identifier'))
    if ('bin_start' in keys):
        columns.append(dict(id='bin_start', name='Lower Energy Limit',
                            type='numeric',
                            format=Format(precision=2, scheme=Scheme.exponent)))
    if ('bin_end' in keys):
        columns.append(dict(id='bin_end', name='Upper Energy Limit',
                            type='numeric',
                            format=Format(precision=2, scheme=Scheme.exponent)))
    if ('nbr_bins' in keys):
        columns.append(dict(id='nbr_bins', name='Total Number of Energy bins',
                            type='numeric',
                            format=Format(precision=2,
                                          scheme=Scheme.decimal_integer)))
    if ('volume' in keys):
        columns.append(dict(id='volume', name='\u270E Detector Volume (cm3)',
                            type='numeric',
                            format=Format(precision=2, scheme=Scheme.exponent),
                                          editable=True))
    else:

        raise ValueError('Should contain the Volume.')

    if (volumes):
        nbr_tracks = np.array(nbr_tracks)
        for i in range(len(track_info_table)):
            ind = int(np.searchsorted(nbr_tracks, i, side='right'))
            if (volumes[ind] is not None):
                track_info_table[i]['volume'] = volumes[ind]
    if (simu_names):
        columns = [dict(id='simu_name', name='Simulation Name')] + columns
        nbr_tracks = np.array(nbr_tracks)
        for i in range(len(track_info_table)):
            ind = int(np.searchsorted(nbr_tracks, i, side='right'))
            if (simu_names[ind] is not None):
                track_info_table[i]['simu_name'] = simu_names[ind]

    if (type == 'expert'):
        expert_button_value = 0
        expert_row_type = {'display': 'block'}
    elif (type == 'user'):
        expert_button_value = 1
        expert_row_type = {'display': 'none'}
    else:
        error_message: str = ("Unknow type of users specified.")

        raise Exception(error_message)

    return (Serverside(usrtrack_plot), track_info_table, columns,
            [i for i in range(len(track_info_table))], expert_button_value,
            expert_row_type)


################################################################################
# Callbacks
# ------------------------------------------------------------------------------
# > Figure Initiation Callabck Logic:
#   'url_track' -> 'cache_store_track'
# > Figure Update Callback Logic:
#   'cache_store_track' -> 'particle_dropdown', 'volume_input'
#   'disp_err', 'log_x', 'log_y', 'particle_dropdown',\
#   'volume_input' -> 'usrtrack_fig'
################################################################################

@callback(
    Output('expert_trk_button', 'children'),
    Output('expert_trk_button', 'color'),
    Output('table_row_div', 'style'),
    Input('expert_trk_button', 'n_clicks'),
    on_error=error_handler,
    prevent_initial_call=True
)
def on_expert_button_click(n):
    if (n is not None):
        if (n%2):   # first click

            return 'Show Expert View', 'success', {'display': 'none'}
        else:   # initial no click (n=0)

            return 'Show User View', 'primary', {'display': 'block'}

@callback(
    Output('particle_dropdown', 'options'),
    Output('particle_dropdown', 'value'),
    Input('cache_store_track', 'data'),
    on_error=error_handler,
    prevent_initial_call=True
    )
def initiate_particle_dropdown(usrtrack_plot):
    particle_names = usrtrack_plot.particle_names

    return particle_names, particle_names


@callback(
    Output('usrtrack_fig', 'figure', allow_duplicate=True),
    Input('lim_x_min', 'invalid'),
    Input('lim_x_max', 'invalid'),
    Input('lim_y_min', 'invalid'),
    Input('lim_y_max', 'invalid'),
    State('lim_x_min', 'value'),
    State('lim_x_max', 'value'),
    State('lim_y_min', 'value'),
    State('lim_y_max', 'value'),
    State('log_x', 'value'),
    State('log_y', 'value'),
    State('usrtrack_fig', 'figure'),
    prevent_initial_call=True,
    )
def update_plot_limits(lim_x_min_invalid, lim_x_max_invalid, lim_y_min_invalid,
                       lim_y_max_invalid, lim_x_min, lim_x_max, lim_y_min,
                       lim_y_max, log_x, log_y, usrtrack_fig):
    patched_fig = Patch()
    if (usrtrack_fig is not None):  # can be at first call
        x_range = usrtrack_fig['layout']['xaxis']['range']
        if (lim_x_min is not None):
            x_range[0] = float(lim_x_min)
            if (bool(log_x)):
                x_range[0] = math.log10(x_range[0])
        if (lim_x_max is not None):
            x_range[1] = float(lim_x_max)
            if (bool(log_x)):
                x_range[1] = math.log10(x_range[1])
        patched_fig['layout']['xaxis']['autorange'] = False
        patched_fig['layout']['xaxis']['range'] = x_range

        y_range = usrtrack_fig['layout']['yaxis']['range']
        if (lim_y_min is not None):
            y_range[0] = float(lim_y_min)
            if (bool(log_y)):
                y_range[0] = math.log10(y_range[0])
        if (lim_y_max is not None):
            y_range[1] = float(lim_y_max)
            if (bool(log_y)):
                y_range[1] = math.log10(y_range[1])
        patched_fig['layout']['yaxis']['autorange'] = False
        patched_fig['layout']['yaxis']['range'] = y_range

    return patched_fig


@callback(
    Output('usrtrack_fig', 'style'),
    Input('breakpoints_track', 'width'),
    Input('breakpoints_track', 'height'),
)
def update_fig_dimensions(window_width, window_height):
    fig_ratio = FIG_ASPECT_RATIO[0]/FIG_ASPECT_RATIO[1]
    fig_width = window_width*FIG_WINDOW_COVERAGE
    fig_height = window_width*FIG_WINDOW_COVERAGE/fig_ratio

    return {'width': fig_width, 'height': fig_height}



@callback(
    Output('lethargy', 'options'),
    Input('log_x', 'value'),
    prevent_initial_call=True,
    on_error=error_handler
    )
def disable_lethargy(log_x):
    patched_options = Patch()
    if (log_x):

        patched_options[0]['disabled'] = False
    else:

        patched_options[0]['disabled'] = True

    return patched_options



@callback(
    Output('trk_download_data', 'data'),
    Input('trk_download_data_btn', 'n_clicks'),
    State('cache_store_track', 'data'),
    State('disp_err', 'value'),
    State('log_x', 'value'),
    State('lethargy', 'value'),
    State('particle_dropdown', 'value'),
    State('track_table', 'selected_rows'),
    State('track_table', 'data'),
    State('trk_download_delimiter', 'value'),
    State('trk_download_end_line', 'value'),
    prevent_initial_call=True,
    on_error=error_handler,
    )
def download_trk_data(n, usrtrack_plot, disp_err, log_x, lethargy_unit,
                      particle_names, selected_rows, track_table_data,
                      delimiter_char, end_line_char):
    """Extraction is done manually from the plot object and not from the figure
    in order to output a txt file in a given format."""
    # Can not have lethargy unit without x-axis logarithmic
    lethargy_unit_ = lethargy_unit if log_x else False
    all_track_names = usrtrack_plot.track_names
    track_names = [all_track_names[i] for i in selected_rows]
    volumes = [float(row['volume']) for row in track_table_data]

    list_data, list_particles = usrtrack_plot.get_data_as_string(
                                                 particle_names=particle_names,
                                                 track_names=track_names,
                                                 track_volumes=volumes,
                                                 err=bool(disp_err),
                                                 log_x=bool(log_x),
                                                 lethargy_unit=lethargy_unit_,
                                                 delimiter_char=delimiter_char,
                                                 end_line_char=end_line_char)
    def write_archive(bytes_io):
        base_file_name = 'simrad_usrtrack_plot_'
        with zipfile.ZipFile(bytes_io, mode='w') as zf:
            for i in range(len(list_data)):
                crt_file_name = base_file_name + str(track_names[i]) + '_'
                crt_file_name += str(list_particles[i]) + '_'
                crt_file_name += '{:.2f}'.format(volumes[i]) + '_cm3'
                crt_file_name = crt_file_name.replace('.', '_')
                crt_file_name = crt_file_name.replace(' ', '_')
                crt_file_name += '.txt'
                zf.writestr(crt_file_name, list_data[i])

    return dcc.send_bytes(write_archive, 'simrad_usrtrack_plot_data.zip')



@callback(
    Output('trk_raw_download_data', 'data'),
    Input('trk_raw_download_data_btn', 'n_clicks'),
    State('cache_store_track', 'data'),
    State('track_table', 'selected_rows'),
    State('track_table', 'data'),
    State('trk_download_end_line', 'value'),
    prevent_initial_call=True,
    on_error=error_handler,
    )
def download_trk_raw_data(n, usrtrack_plot, selected_rows, track_table_data,
                          end_line_char):
    """Extraction is done manually from the plot object and not from the figure
    in order to output a txt file in a given format."""
    # Can not have lethargy unit without x-axis logarithmic
    all_track_names = usrtrack_plot.track_names
    track_names = [all_track_names[i] for i in selected_rows]
    volumes = [float(row['volume']) for row in track_table_data]
    delimiter_char = '\t'   # ActiWiz format
    list_data, list_particles = usrtrack_plot.get_data_as_actiwiz_string(
                                                 track_names=track_names,
                                                 track_volumes=volumes,
                                                 delimiter_char=delimiter_char,
                                                 end_line_char=end_line_char)
    def write_archive(bytes_io):
        base_file_name = 'simrad_usrtrack_plot_'
        with zipfile.ZipFile(bytes_io, mode='w') as zf:
            for i in range(len(list_data)):
                crt_file_name = base_file_name + str(track_names[i]) + '_'
                crt_file_name += str(list_particles[i]) + '_'
                crt_file_name += '{:.2f}'.format(volumes[i]) + '_cm3'
                crt_file_name = crt_file_name.replace('.', '_')
                crt_file_name = crt_file_name.replace(' ', '_')
                crt_file_name += '.txt'
                zf.writestr(crt_file_name, list_data[i])

    return dcc.send_bytes(write_archive, 'simrad_usrtrack_raw_data.zip')


@callback(
    Output('usrtrack_fig', 'figure'),
    Output('lim_x_min', 'value'),
    Output('lim_x_max', 'value'),
    Output('lim_y_min', 'value'),
    Output('lim_y_max', 'value'),
    State('cache_store_track', 'data'),   # chained -> altering particle_dropdown
    Input('disp_err', 'value'),
    Input('log_x', 'value'),
    Input('log_y', 'value'),
    Input('lethargy', 'value'),
    Input('particle_dropdown', 'value'),
    Input('track_table', 'selected_rows'),
    Input('track_table', 'data'),
    State('lim_x_min', 'value'),
    State('lim_x_max', 'value'),
    State('lim_y_min', 'value'),
    State('lim_y_max', 'value'),
    Input('trk_color_dropdown', 'value'),
    prevent_initial_call=True,
    on_error=error_handler
    )
def update_graph_track(usrtrack_plot, disp_err, log_x, log_y, lethargy_unit,
                       particle_names, selected_rows, track_table_data,
                       lim_x_min, lim_x_max, lim_y_min, lim_y_max,
                       color_dropdown):
    # Can not have lethargy unit without x-axis logarithmic
    lethargy_unit_ = lethargy_unit if log_x else False
    all_track_names = usrtrack_plot.track_names
    track_names = [all_track_names[i] for i in selected_rows]
    volumes = [float(row['volume']) for row in track_table_data]
    line_names = []
    line_colors = []
    colors = PLOTLY_COLOR_PALETTES_DICT[color_dropdown]
    for i, row in enumerate(track_table_data):
        if (row.get('simu_name') is not None):
            line_names.append(row['simu_name'])
        else:
            line_names.append('')
        line_colors.append(colors[i%len(colors)])
    new_fig = usrtrack_plot.get_plot_light(particle_names=particle_names,
                                           track_names=track_names,
                                           track_volumes=volumes,
                                           err=bool(disp_err),
                                           log_x=bool(log_x),
                                           log_y=bool(log_y),
                                           lethargy_unit=lethargy_unit_,
                                           line_names=line_names,
                                           line_colors=line_colors,
                                           )

    return ({'data': new_fig['data'], 'layout': new_fig['layout']},
            lim_x_min, lim_x_max, lim_y_min, lim_y_max)



if __name__ == "__main__":

    from flask import Flask

    app = Flask(__name__)

    # inject Dash
    app = init_dashboard(app)

    app_port = 5009
    print(f'http://localhost:{app_port}{BASE_URL}')
    app.run(debug=True, port=app_port)
