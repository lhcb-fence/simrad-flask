""".. moduleauthor:: Carlos Brito"""

import pytest
import flask
from werkzeug.datastructures import FileStorage

#TODO: add tests for dash apps

def test_spectra_dict(client):
    file_path = 'simrad_server/resources/data/RUN2/detector_geometry_input_5_/fluka_version_4_4_0/magnetic_field_UP/PARTICLE_SPECTRA/RspMP31/RspMP31.TXT'
    file = FileStorage(
        stream=open(file_path, "rb"),
        filename="test_file.txt",
        content_type="text/plain",
    )
    response = client.post(
            "/usrtrack_info",
               data={
                "file": file,
            },
            content_type='multipart/form-data',
        )
    assert response.status_code == 200 and response.content_type == "application/json"

def test_regular_dict(client):
    file_path = 'simrad_server/resources/data/RUN2/detector_geometry_input_5_/fluka_version_4_4_0/magnetic_field_UP/dose_x/dose_x.TXT'
    file = FileStorage(
        stream=open(file_path, "rb"),
        filename="test_file.txt",
        content_type="text/plain",
    )
    response = client.post(
            "/usrbin_info",
               data={
                "file": file,
            },
            content_type='multipart/form-data',
        )

    assert response.status_code == 200 and response.content_type == "application/json"
